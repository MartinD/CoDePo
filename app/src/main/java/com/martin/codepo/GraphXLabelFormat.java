package com.martin.codepo;

import android.support.annotation.NonNull;

import java.text.FieldPosition;
import java.text.Format;
import java.text.ParsePosition;
import java.util.ArrayList;

/**
 * Created by user on 18/07/17.
 */

public class GraphXLabelFormat extends Format {

    private ArrayList<String> listTime;

    public GraphXLabelFormat(ArrayList<String> listTime) {
        this.listTime = listTime;
    }

    @Override
    public StringBuffer format(Object obj, @NonNull StringBuffer toAppendTo, @NonNull FieldPosition pos) {
        int i = Math.round(((Number) obj).floatValue());
        return toAppendTo.append(listTime.get(i).substring(0, 5));

    }

    @Override
    public Object parseObject(String source, ParsePosition pos) {
        // unused
        return null;
    }
}
