package com.martin.codepo;

import java.util.ArrayList;

/**
 * Created by user on 13/04/17.
 */

public class TupleData extends ArrayList{

    private ArrayList<Float> first;
    private ArrayList<Float> second;
    private ArrayList<Float> third;

    public TupleData(ArrayList<Float> first, ArrayList<Float> second, ArrayList<Float> third){
        this.first = first;
        this.second = second;
        this.third = third;
    }

    public void add(Float cpu, Float ram, Float disk){
        first.add(cpu);
        second.add(ram);
        third.add(disk);
    }

    public ArrayList<Float> getFirst() {
        return first;
    }

    public ArrayList<Float> getSecond() {
        return second;
    }

    public ArrayList<Float> getThird() {
        return third;
    }
}
