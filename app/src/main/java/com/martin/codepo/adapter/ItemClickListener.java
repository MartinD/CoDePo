package com.martin.codepo.adapter;

import android.view.View;

/**
 * Created by user on 19/02/17.
 */

public interface ItemClickListener {

    public void onItemClick(int pos, View view);

}
