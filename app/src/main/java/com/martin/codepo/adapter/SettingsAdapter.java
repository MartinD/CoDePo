package com.martin.codepo.adapter;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.martin.codepo.R;
import com.martin.codepo.activity.CenterData;
import com.martin.codepo.activity.Informations;
import com.martin.codepo.activity.ModifyCheckTime;
import com.martin.codepo.activity.ScanNetwork;
import com.martin.codepo.activity.Tutorial;
import com.martin.codepo.database.CenterDAO;

import java.util.ArrayList;

/**
 * Created by user on 13/04/17.
 */

public class SettingsAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private ArrayList<String> names;

    public SettingsAdapter(ArrayList<String> names){
        this.names = names;
    }

    @Override
    public HolderSettings onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.setting, parent, false);
        return new HolderSettings(view);
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        HolderSettings holderSettings = (HolderSettings) holder;
        String name = names.get(position);
        holderSettings.getName().setText(name);
        holderSettings.itemView.setTag(position);
        holderSettings.SetOnItemClickListener(itemClickListener);
    }


    private ItemClickListener itemClickListener = new ItemClickListener() {
        @Override
        public void onItemClick(int pos, View view) {
            Context context = view.getContext();
            Intent intent = null;
            switch (names.get(pos)){
                case ("Tutoriel"):
                    intent = new Intent(context, Tutorial.class);
                    break;
                case("Centre"):
                    intent = new Intent(context, CenterData.class);
                    break;
                case("Scan du réseau"):
                    intent = new Intent(context, ScanNetwork.class);
                    break;
                case("Informations"):
                    intent = new Intent(context, Informations.class);
                    break;
                case ("Notifications"):
                    intent = new Intent(context, ModifyCheckTime.class);
                    break;
            }
            context.startActivity(intent);
        }
    };

    @Override
    public int getItemCount() {
        return names == null ? 0 : names.size();
    }


    public class HolderSettings extends RecyclerView.ViewHolder implements View.OnClickListener {

        private TextView name;
        private ItemClickListener itemClickListener;

        public HolderSettings(View itemView) {
            super(itemView);
            name = (TextView) itemView.findViewById(R.id.name);
            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            itemClickListener.onItemClick(getAdapterPosition(), v);
        }

        public TextView getName(){
            return name;
        }

        public void SetOnItemClickListener(ItemClickListener itemClickListener){
            this.itemClickListener = itemClickListener;
        }
    }

}
