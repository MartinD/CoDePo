package com.martin.codepo.adapter;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.TextView;

import com.martin.codepo.R;
import com.martin.codepo.Types;
import com.martin.codepo.activity.ModifyDevice;
import com.martin.codepo.database.Device;

import java.util.ArrayList;
import java.util.Objects;

/**
 * Created by martin on 14/03/18.
 */

public class ChosableDevicesAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private ArrayList<Device> devices;
    private ArrayList<Device> devicesChosen;

    public ChosableDevicesAdapter(ArrayList<Device> devices, ArrayList<Device> devicesChosen) {
        this.devices = devices;
        this.devicesChosen = devicesChosen;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = null;
        if(viewType == Types.Ping.getNum()) {
            view = LayoutInflater.from(parent.getContext()).inflate(R.layout.device_ping_selectable, parent, false);
        }
        else  if(viewType == Types.SNMP.getNum()){
            view = LayoutInflater.from(parent.getContext()).inflate(R.layout.device_snmp_selectable, parent, false);
        }
        else if(viewType == Types.TCP.getNum()){
            view = LayoutInflater.from(parent.getContext()).inflate(R.layout.device_tcp_selectable, parent, false);
        }
        else if(viewType == Types.Android.getNum()){
            view = LayoutInflater.from(parent.getContext()).inflate(R.layout.device_android_selectable, parent, false);
        }
        else if(viewType == Types.Wifi.getNum()){
            view = LayoutInflater.from(parent.getContext()).inflate(R.layout.device_wifi_selectable, parent, false);
        }
        else if (viewType == Types.Electricity.getNum()){
            view = LayoutInflater.from(parent.getContext()).inflate(R.layout.device_electricity_selectable, parent, false);
        }
        else if (viewType == Types.Solar.getNum()){
            view = LayoutInflater.from(parent.getContext()).inflate(R.layout.device_solar_selectable, parent, false);
        }
        return new HolderChoosableDevices(view);
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        final Device device = devices.get(position);
        final HolderChoosableDevices viewHolder = (HolderChoosableDevices) holder;
        viewHolder.getServerName().setText(device.getName());
        viewHolder.getServerIp().setText(device.getIp());
        viewHolder.getDeviceChoosen().setChecked(isDeviceChoosen(device));
        viewHolder.itemView.setTag(position);
        viewHolder.getDeviceChoosen().setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(viewHolder.getDeviceChoosen().isChecked()){
                    devicesChosen.add(device);
                }
                else {
                    toRemove(device);
                }
            }
        });
        viewHolder.SetOnItemClickListener(itemClickListener);
    }

    private boolean isDeviceChoosen(Device deviceUser){
        for(Device device : devicesChosen){
            if (Objects.equals(device.getId(), deviceUser.getId())){
                return true;
            }
        }
        return false;
    }

    private void toRemove(Device deviceUser){
        for(Device device : devicesChosen){
            if(Objects.equals(device.getId(), deviceUser.getId())){
                devicesChosen.remove(device);
                break;
            }
        }
    }

    private ItemClickListener itemClickListener = new ItemClickListener() {
        @Override
        public void onItemClick(int pos, View view) {
            Context context = view.getContext();
            Device device = devices.get(pos);
            Intent intent = new Intent(context, ModifyDevice.class);
            intent.putExtra(DevicesAdapter.EXTRA_DEVICE, device);
            context.startActivity(intent);
        }
    };

    @Override
    public int getItemCount() {
        return devices == null ? 0 : devices.size();
    }

    public void setDevices(ArrayList<Device> devices){
        this.devices = devices;
    }

    public ArrayList<Device> getDevicesChosen(){
        return this.devicesChosen;
    }

    private class HolderChoosableDevices extends RecyclerView.ViewHolder implements View.OnClickListener{

        private TextView serverName;
        private TextView serverIp;
        private CheckBox deviceChoosen;
        private ItemClickListener itemClickListener;

        public HolderChoosableDevices(View itemView) {
            super(itemView);
            serverName = (TextView) itemView.findViewById(R.id.nameServer);
            serverIp = (TextView) itemView.findViewById(R.id.ipServer);
            deviceChoosen = (CheckBox) itemView.findViewById(R.id.deviceChosen);
            itemView.setOnClickListener(this);
        }

        public TextView getServerName() {
            return serverName;
        }

        public TextView getServerIp() {
            return serverIp;
        }

        public CheckBox getDeviceChoosen(){
            return deviceChoosen;
        }

        @Override
        public void onClick(View view) {
            itemClickListener.onItemClick(getAdapterPosition(), view);
        }

        public void SetOnItemClickListener(ItemClickListener itemClickListener){
            this.itemClickListener = itemClickListener;
        }
    }
}
