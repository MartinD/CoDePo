package com.martin.codepo.adapter;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.martin.codepo.R;
import com.martin.codepo.Types;
import com.martin.codepo.activity.DataDevice;
import com.martin.codepo.database.Device;

import java.util.ArrayList;

/**
 * Created by martin on 1/04/17.
 */

public class DevicesAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private ArrayList<Device> devices = new ArrayList<>();
    public static final String EXTRA_DEVICE = "DEVICE";

    public DevicesAdapter(ArrayList<Device> devices){
        this.devices = devices;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = null;
        if(viewType == Types.Ping.getNum()) {
            view = LayoutInflater.from(parent.getContext()).inflate(R.layout.device_ping, parent, false);
        }
        else  if(viewType == Types.SNMP.getNum()){
            view = LayoutInflater.from(parent.getContext()).inflate(R.layout.device_snmp, parent, false);
        }
        else if(viewType == Types.TCP.getNum()){
            view = LayoutInflater.from(parent.getContext()).inflate(R.layout.device_tcp, parent, false);
        }
        else if(viewType == Types.Android.getNum()){
            view = LayoutInflater.from(parent.getContext()).inflate(R.layout.device_android, parent, false);
        }
        else if(viewType == Types.Wifi.getNum()){
            view = LayoutInflater.from(parent.getContext()).inflate(R.layout.device_wifi, parent, false);
        }
        else if (viewType == Types.Electricity.getNum()){
            view = LayoutInflater.from(parent.getContext()).inflate(R.layout.device_electricity, parent, false);
        }
        else if (viewType == Types.Solar.getNum()){
            view = LayoutInflater.from(parent.getContext()).inflate(R.layout.device_solar, parent, false);
        }
        return new ViewHolder(view);

    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        Device device = devices.get(position);
        ViewHolder viewHolder = (ViewHolder) holder;
        viewHolder.getServerName().setText(device.getName());
        viewHolder.getServerIp().setText(device.getIp());
        viewHolder.itemView.setTag(position);
        viewHolder.SetOnItemClickListener(itemClickListener);
    }

    private ItemClickListener itemClickListener = new ItemClickListener() {
        @Override
        public void onItemClick(int pos, View view) {
            Context context = view.getContext();
            Device device = devices.get(pos);
            Intent intent = new Intent(context, DataDevice.class);
            intent.putExtra(DevicesAdapter.EXTRA_DEVICE, device);
            context.startActivity(intent);
        }
    };

    @Override
    public int getItemCount() {
        return devices.size();
    }

    public void setDevices(ArrayList<Device> devices){
        this.devices = devices;
    }

    public ArrayList<Device> getDevices(){
        return devices;
    }

    @Override
    public int getItemViewType(int position){
        Device device = devices.get(position);
        return device.getDeviceType();
    }

    private class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener{

        private TextView serverName;
        private TextView serverIp;
        private ItemClickListener itemClickListener;


        public ViewHolder(View itemView) {
            super(itemView);
            serverName = (TextView) itemView.findViewById(R.id.nameServer);
            serverIp = (TextView) itemView.findViewById(R.id.ipServer);
            itemView.setOnClickListener(this);
        }

        public TextView getServerName() {
            return serverName;
        }

        public TextView getServerIp() {
            return serverIp;
        }

        @Override
        public void onClick(View view) {
            itemClickListener.onItemClick(getAdapterPosition(), view);
        }

        public void SetOnItemClickListener(ItemClickListener itemClickListener){
            this.itemClickListener = itemClickListener;
        }
    }
}
