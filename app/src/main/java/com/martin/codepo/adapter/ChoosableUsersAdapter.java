package com.martin.codepo.adapter;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.TextView;

import com.martin.codepo.R;
import com.martin.codepo.activity.ModifyUser;
import com.martin.codepo.database.User;

import java.util.ArrayList;
import java.util.Objects;

/**
 * Created by user on 19/03/17.
 */

public class ChoosableUsersAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder>{

    private ArrayList<User> users;
    private ArrayList<User> usersChosen;

    public ChoosableUsersAdapter(ArrayList<User> users, ArrayList<User> usersChosen){
        this.users = users;
        this.usersChosen = usersChosen;
    }

    @Override
    public HolderChoosablePhones onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.user_selectable, parent, false);
        //Toast.makeText(parent.getContext(),"user: " + ( phoneChoosen.size() != 0 ? phoneChoosen.contains(phoneNumbers.get(0)) :  0) , Toast.LENGTH_SHORT).show(); //trouver pourquoi contain ne fonctionne pas
        return new HolderChoosablePhones(view);
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        final User user = users.get(position);
        final HolderChoosablePhones viewHolder = (HolderChoosablePhones) holder;
        viewHolder.getPhoneName().setText(user.getName());
        viewHolder.getPhoneNumber().setText(user.getPhone());
        //String online = phoneNumber.getIsOnline() == 1 ? "Serveur SMS" : "";
        //viewHolder.getPhoneOnline().setText(online);
        viewHolder.getPhoneChoosen().setChecked(isPhoneChoosen(user));
        viewHolder.SetOnItemClickListener(itemClickListener);
        viewHolder.itemView.setTag(position);
        viewHolder.getPhoneChoosen().setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(viewHolder.getPhoneChoosen().isChecked()){
                    usersChosen.add(user);
                }
                else {
                    toRemove(user);
                }
            }
        });
        viewHolder.SetOnItemClickListener(itemClickListener);
    }

    private boolean isPhoneChoosen(User phoneNumbers){
        for(User user : usersChosen){
            if (Objects.equals(user.getPhone(), phoneNumbers.getPhone())){
                return true;
            }
        }
        return false;
    }

    private void toRemove(User phoneNumbers){
        for(User phone : usersChosen){
            if(Objects.equals(phone.getPhone(), phoneNumbers.getPhone())){
                usersChosen.remove(phone);
                break;
            }
        }
    }

    private ItemClickListener itemClickListener = new ItemClickListener() {
        @Override
        public void onItemClick(int pos, View view) {
            Context context = view.getContext();
            User user = users.get(pos);
            Intent intent = new Intent(context, ModifyUser.class);
            intent.putExtra(UsersAdapter.EXTRA_USER, user);
            context.startActivity(intent);
        }
    };

    @Override
    public int getItemCount() {
        return users == null ? 0 : users.size();
    }

    public void setPhones(ArrayList<User> users){
        this.users = users;
    }

    public ArrayList<User> getPhonesChoosen(){
        return this.usersChosen;
    }

    private class HolderChoosablePhones extends RecyclerView.ViewHolder implements View.OnClickListener{

        private TextView phoneName;
        private TextView phoneNumber;
        private TextView phoneOnline;
        private CheckBox phoneChoosen;
        private ItemClickListener itemClickListener;

        public HolderChoosablePhones(View itemView) {
            super(itemView);
            phoneName = (TextView) itemView.findViewById(R.id.phoneName);
            phoneNumber = (TextView) itemView.findViewById(R.id.phoneNumber);
            phoneOnline = (TextView) itemView.findViewById(R.id.phoneOnline);
            phoneChoosen = (CheckBox) itemView.findViewById(R.id.phoneChoosen);
            itemView.setOnClickListener(this);
        }

        public TextView getPhoneName() {
            return phoneName;
        }

        public TextView getPhoneNumber() {
            return phoneNumber;
        }

        public TextView getPhoneOnline(){
            return phoneOnline;
        }

        public CheckBox getPhoneChoosen() {
            return phoneChoosen;
        }

        @Override
        public void onClick(View view) {
            itemClickListener.onItemClick(getAdapterPosition(), view);
        }

        public void SetOnItemClickListener(ItemClickListener itemClickListener){
            this.itemClickListener = itemClickListener;
        }

    }

}
