package com.martin.codepo.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.martin.codepo.NetworkDevice;
import com.martin.codepo.R;
import com.martin.codepo.activity.EncodeDevice;

import java.util.List;

/**
 * Created by martin on 19/02/17.
 */
public class NetworkDeviceAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder>{


    private List<NetworkDevice> addresses;
    private int rowLayout;
    private Context context;

    public NetworkDeviceAdapter(List<NetworkDevice> addresses, int rowLayout, Context context) {
        this.addresses = addresses;
        this.rowLayout = rowLayout;
        this.context = context;
    }


    @Override
    public HolderNetwork onCreateViewHolder(ViewGroup viewGroup, int i) {
        View v = LayoutInflater.from(viewGroup.getContext()).inflate(rowLayout, viewGroup, false);
        return new HolderNetwork(v);
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        HolderNetwork holderNetwork = (HolderNetwork) holder;
        final NetworkDevice address = addresses.get(position);
        holderNetwork.getDeviceName().setText(address.getDeviceName());
        holderNetwork.getDeviceIp().setText(address.getIpAddress());
        //holderNetwork.getMacAdd().setText(address.getMacAddress());
        holderNetwork.SetOnItemClickListener(itemClickListener);

    }


    private ItemClickListener itemClickListener = new ItemClickListener() {
        @Override
        public void onItemClick(int pos, View view) {
            EncodeDevice.networkDevice = addresses.get(pos);
        }
    };



    @Override
    public int getItemCount() {
        return addresses == null ? 0 : addresses.size();
    }

    public void setAddresses(List<NetworkDevice> addresses) {
        this.addresses = addresses;
    }

    public class HolderNetwork extends RecyclerView.ViewHolder implements View.OnClickListener{

        private TextView deviceName;
        private TextView deviceIp;
        //private TextView macAdd;
        private ItemClickListener itemClickListener;

        public HolderNetwork(final View itemView) {
            super(itemView);
            deviceName = (TextView) itemView.findViewById(R.id.deviceName);
            deviceIp = (TextView) itemView.findViewById(R.id.deviceIp);
            //macAdd = (TextView)itemView.findViewById(R.id.macAdd);
            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            itemClickListener.onItemClick(getAdapterPosition(), view);
        }

        public void SetOnItemClickListener(ItemClickListener itemClickListener){
            this.itemClickListener = itemClickListener;
        }

        public TextView getDeviceName() {
            return deviceName;
        }

        public TextView getDeviceIp() {
            return deviceIp;
        }

    /*public TextView getMacAdd() {
        return macAdd;
    }*/
    }


}
