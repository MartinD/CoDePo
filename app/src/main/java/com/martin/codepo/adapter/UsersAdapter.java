package com.martin.codepo.adapter;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.martin.codepo.R;
import com.martin.codepo.activity.ModifyUser;
import com.martin.codepo.database.User;

import java.util.ArrayList;

/**
 * Created by martin on 13/04/17.
 */

public class UsersAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {


    private ArrayList<User> users;
    public static final String EXTRA_USER = "USER";

    public UsersAdapter(ArrayList<User> users){
        this.users = users;
    }

    @Override
    public HolderUsers onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.user, parent, false);
        //Toast.makeText(parent.getContext(),"user: " + ( phoneChoosen.size() != 0 ? phoneChoosen.contains(phoneNumbers.get(0)) :  0) , Toast.LENGTH_SHORT).show(); //trouver pourquoi contain ne fonctionne pas
        return new HolderUsers(view);
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        User user = users.get(position);
        HolderUsers holderUsers = (HolderUsers) holder;
        holderUsers.getPhoneName().setText(user.getName());
        holderUsers.getPhoneNumber().setText(user.getPhone());
        holderUsers.itemView.setTag(position);
        holderUsers.SetOnItemClickListener(itemClickListener);
    }

    private ItemClickListener itemClickListener = new ItemClickListener() {
        @Override
        public void onItemClick(int pos, View view) {
            Context context = view.getContext();
            User user = users.get(pos);
            Intent intent = new Intent(context, ModifyUser.class);
            intent.putExtra(EXTRA_USER, user);
            context.startActivity(intent);
        }
    };

    @Override
    public int getItemCount() {
        return users == null ? 0 : users.size();
    }


    public ArrayList<User> getUsersChoosen() {
        return users;
    }

    public void setUsers(ArrayList<User> users) {
        this.users = users;
    }



    public class HolderUsers extends RecyclerView.ViewHolder implements View.OnClickListener {

        private TextView phoneName;
        private TextView phoneNumber;
        private TextView phoneOnline;
        private ItemClickListener itemClickListener;

        public HolderUsers(View itemView) {
            super(itemView);
            phoneName = (TextView) itemView.findViewById(R.id.phoneName);
            phoneNumber = (TextView) itemView.findViewById(R.id.phoneNumber);
            phoneOnline = (TextView) itemView.findViewById(R.id.isOnline);
            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            itemClickListener.onItemClick(getAdapterPosition(), view);
        }

        public void SetOnItemClickListener(ItemClickListener itemClickListener){
            this.itemClickListener = itemClickListener;
        }

        public TextView getPhoneName() {
            return phoneName;
        }

        public TextView getPhoneNumber() {
            return phoneNumber;
        }

        public TextView getPhoneOnline(){
            return phoneOnline;
        }

    }

}
