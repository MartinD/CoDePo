package com.martin.codepo.database;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by martin on 10/03/18.
 */

public class DeviceConfig implements Parcelable {

    private String id;
    private String first;
    private String second;
    private String third;
    private String deviceKey;
    private String type;

    public DeviceConfig(String id, String first, String second, String third, String deviceKey) {
        this.id = id;
        this.first = first;
        this.second = second;
        this.third = third;
        this.deviceKey = deviceKey;
        this.type = "config";
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getFirst() {
        return first;
    }

    public void setFirst(String first) {
        this.first = first;
    }

    public String getSecond() {
        return second;
    }

    public void setSecond(String second) {
        this.second = second;
    }

    public String getThird() {
        return third;
    }

    public void setThird(String third) {
        this.third = third;
    }

    public String getDeviceKey() {
        return deviceKey;
    }

    public void setDeviceKey(String deviceKey) {
        this.deviceKey = deviceKey;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.id);
        dest.writeString(this.first);
        dest.writeString(this.second);
        dest.writeString(this.third);
        dest.writeString(this.deviceKey);
    }

    protected DeviceConfig(Parcel in) {
        this.id = in.readString();
        this.first = in.readString();
        this.second = in.readString();
        this.third = in.readString();
        this.deviceKey = in.readString();
    }

    public static final Parcelable.Creator<DeviceConfig> CREATOR = new Parcelable.Creator<DeviceConfig>() {
        @Override
        public DeviceConfig createFromParcel(Parcel source) {
            return new DeviceConfig(source);
        }

        @Override
        public DeviceConfig[] newArray(int size) {
            return new DeviceConfig[size];
        }
    };

}
