package com.martin.codepo.database;

import java.util.ArrayList;

/**
 * Created by martin on 9/03/18.
 */

public class Center {

    private String id;
    private String name;
    private String responsible;
    private String address;
    private String phone;
    private int isOnline;
    private String type;
    private ArrayList<User> users;
    private ArrayList<Device> devices;

    public Center(String id, String name, String address, String responsible, String phone, int isOnline) {
        this.id = id;
        this.name = name;
        this.address = address;
        this.phone = phone;
        this.isOnline = isOnline;
        this.responsible = responsible;
        this.type = "monitoring";
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getResponsible() {
        return responsible;
    }

    public void setResponsible(String responsible) {
        this.responsible = responsible;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public int getIsOnline() {
        return isOnline;
    }

    public void setIsOnline(int isOnline) {
        this.isOnline = isOnline;
    }

    public ArrayList<User> getUsers() {
        return users;
    }

    public void setUsers(ArrayList<User> users) {
        this.users = users;
    }

    public ArrayList<Device> getDevices() {
        return devices;
    }

    public void setDevices(ArrayList<Device> devices) {
        this.devices = devices;
    }

    public String getName() {
        return name;
    }
}
