package com.martin.codepo.database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;

import java.util.ArrayList;

/**
 * Created by martin on 11/03/18.
 */

public class DeviceConfigDAO extends AbstractDAO {

    public static final String DeviceConfigTable = "DeviceConfig";
    public static final String DeviceConfigKey = "ConfigKey";
    public static final String DeviceConfigFirst = "First";
    public static final String DeviceConfigSecond = "Second";
    public static final String DeviceConfigThird = "Third";
    public static final String DeviceConfigCreateTable = "CREATE TABLE IF NOT EXISTS " + DeviceConfigTable + " (" +
            DeviceConfigKey + " TEXT PRIMARY KEY, " +
            DeviceConfigFirst + " TEXT, " +
            DeviceConfigSecond + " TEXT, " +
            DeviceConfigThird + " TEXT, " +
            DeviceDAO.DeviceKey + " TEXT, " +
            "FOREIGN KEY (" + DeviceDAO.DeviceKey + ") REFERENCES " + DeviceDAO.DeviceKey + "(" + DeviceDAO.DeviceKey + ") );";

    public DeviceConfigDAO(Context context) {
        super(context);
    }



    public DeviceConfig selectConfigDeviceKey(String deviceKey) {
        open();
        Cursor cursor = sqLiteDatabase.rawQuery("select * from " + DeviceConfigTable + " where " + DeviceDAO.DeviceKey + " = ?", new String[]{deviceKey});
        DeviceConfig deviceConfig = null;
        while (cursor.moveToNext()){
            deviceConfig = new DeviceConfig(cursor.getString(0), cursor.getString(1), cursor.getString(2), cursor.getString(3), cursor.getString(4));
        }
        close();
        return deviceConfig;
    }

    public void updateDeviceConfig(DeviceConfig deviceConfig) {
        open();
        ContentValues contentValues = new ContentValues();
        contentValues.put(DeviceConfigFirst, deviceConfig.getFirst());
        contentValues.put(DeviceConfigSecond, deviceConfig.getSecond());
        contentValues.put(DeviceConfigThird, deviceConfig.getThird());
        sqLiteDatabase.update(DeviceConfigTable, contentValues, DeviceConfigKey + " = ?", new String[]{deviceConfig.getId()});
        close();
    }

    public void addDeviceConfig(DeviceConfig deviceConfig) {
        open();
        ContentValues contentValues = new ContentValues();
        contentValues.put(DeviceConfigKey, deviceConfig.getId());
        contentValues.put(DeviceConfigFirst, deviceConfig.getFirst());
        contentValues.put(DeviceConfigSecond, deviceConfig.getSecond());
        contentValues.put(DeviceConfigThird, deviceConfig.getThird());
        contentValues.put(DeviceDAO.DeviceKey, deviceConfig.getDeviceKey());
        sqLiteDatabase.insert(DeviceConfigTable, null, contentValues);
        close();
    }

    public ArrayList<DeviceConfig> selectAllDeviceConfig() {
        open();
        Cursor cursor = sqLiteDatabase.rawQuery("select * from " + DeviceConfigTable, null);
        DeviceConfig deviceConfig;
        ArrayList<DeviceConfig> deviceConfigs = new ArrayList<>();
        while (cursor.moveToNext()){
            deviceConfig = new DeviceConfig(cursor.getString(0), cursor.getString(1), cursor.getString(2), cursor.getString(3), cursor.getString(4));
            deviceConfigs.add(deviceConfig);
        }
        close();
        return deviceConfigs;
    }
}
