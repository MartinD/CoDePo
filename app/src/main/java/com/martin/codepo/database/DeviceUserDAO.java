package com.martin.codepo.database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.util.Log;

import java.util.ArrayList;

/**
 * Created by martin on 13/03/18.
 */

public class DeviceUserDAO extends AbstractDAO {

    public static final String DeviceUserTable = "DeviceUser";
    public static final String DeviceUserKey = "DeviceUserKey";
    public static final String DeviceUserCreateTable = "CREATE TABLE IF NOT EXISTS " + DeviceUserTable + " (" +
            DeviceUserKey + " TEXT PRIMARY KEY, " +
            DeviceDAO.DeviceKey + " TEXT, " +
            UserDAO.UserKey + " TEXT, " +
            "FOREIGN KEY (" + DeviceDAO.DeviceKey + ") REFERENCES " + DeviceDAO.DeviceKey + "(" + DeviceDAO.DeviceKey + "), " +
            "FOREIGN KEY (" + UserDAO.UserKey + ") REFERENCES " + UserDAO.UserKey + "(" + UserDAO.UserKey + ") );";

    public DeviceUserDAO(Context context) {
        super(context);
    }

    public void addDeviceUser(DeviceUser deviceUser){
        open();
        ContentValues contentValues = new ContentValues();
        contentValues.put(DeviceUserKey, deviceUser.getId());
        contentValues.put(DeviceDAO.DeviceKey, deviceUser.getDeviceKey());
        contentValues.put(UserDAO.UserKey, deviceUser.getUserKey());
        sqLiteDatabase.insert(DeviceUserTable, null, contentValues);
        close();
    }

    public void delAllDeviceUserByUser(String id){
        open();
        sqLiteDatabase.delete(DeviceUserTable, UserDAO.UserKey + " = ?", new String[]{id});
        close();
    }

    public void delAllDeviceUser(String deviceKey){
        open();
        sqLiteDatabase.delete(DeviceUserTable, DeviceDAO.DeviceKey + " = ?", new String[]{deviceKey});
        close();
    }

    public void updateDeviceUser(DeviceUser deviceUser){
        open();
        ContentValues contentValues = new ContentValues();
        contentValues.put(DeviceDAO.DeviceKey, deviceUser.getDeviceKey());
        contentValues.put(UserDAO.UserKey, deviceUser.getUserKey());
        sqLiteDatabase.update(DeviceUserTable, contentValues, DeviceUserKey + " = ?", new String[]{deviceUser.getId()});
        close();
    }

    public void updateAllDeviceUser(String deviceKey, ArrayList<DeviceUser> deviceUsers) {
        delAllDeviceUser(deviceKey);
        for(DeviceUser deviceUser : deviceUsers){
            addDeviceUser(deviceUser);
        }
    }

    public void updateAllDeviceUserByUser(String userKey, ArrayList<DeviceUser> deviceUsers){
        delAllDeviceUserByUser(userKey);
        for(DeviceUser deviceUser : deviceUsers){
            addDeviceUser(deviceUser);
        }
    }

    public void addAllDeviceUser(ArrayList<DeviceUser> deviceUsers){
        for(DeviceUser deviceUser : deviceUsers){
            addDeviceUser(deviceUser);
        }
    }

    public ArrayList<DeviceUser> selectDeviceUserByDeviceKey(String deviceKey){
        open();
        Cursor cursor = sqLiteDatabase.rawQuery("select * from " + DeviceUserTable + " where " + DeviceDAO.DeviceKey + " = ?", new String[]{deviceKey});
        ArrayList<DeviceUser> deviceUsers = new ArrayList<>();
        DeviceUser deviceUser;
        while (cursor.moveToNext()){
            deviceUser = new DeviceUser(cursor.getString(0), cursor.getString(1), cursor.getString(2));
            deviceUsers.add(deviceUser);
        }
        close();
        return deviceUsers;
    }

    public ArrayList<DeviceUser> selectDeviceUserByUserKey(String userKey){
        open();Cursor cursor = sqLiteDatabase.rawQuery("select * from " + DeviceUserTable + " where " + UserDAO.UserKey+ " = ?", new String[]{userKey});
        ArrayList<DeviceUser> deviceUsers = new ArrayList<>();
        DeviceUser deviceUser;
        while (cursor.moveToNext()){
            deviceUser = new DeviceUser(cursor.getString(0), cursor.getString(1), cursor.getString(2));
            deviceUsers.add(deviceUser);
        }
        close();
        return deviceUsers;
    }
}
