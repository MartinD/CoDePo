package com.martin.codepo.database;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by martin on 13/03/18.
 */

public class DeviceUser implements Parcelable {

    private String id;
    private String deviceKey;
    private String userKey;
    private String type;

    public DeviceUser(String id, String deviceKey, String userKey) {
        this.id = id;
        this.deviceKey = deviceKey;
        this.userKey = userKey;
        this.type = "deviceUser";
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getDeviceKey() {
        return deviceKey;
    }

    public void setDeviceKey(String deviceKey) {
        this.deviceKey = deviceKey;
    }

    public String getUserKey() {
        return userKey;
    }

    public void setUserKey(String userKey) {
        this.userKey = userKey;
    }


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.id);
        dest.writeString(this.deviceKey);
        dest.writeString(this.userKey);
    }

    protected DeviceUser(Parcel in) {
        this.id = in.readString();
        this.deviceKey = in.readString();
        this.userKey = in.readString();
    }

    public static final Parcelable.Creator<DeviceUser> CREATOR = new Parcelable.Creator<DeviceUser>() {
        @Override
        public DeviceUser createFromParcel(Parcel source) {
            return new DeviceUser(source);
        }

        @Override
        public DeviceUser[] newArray(int size) {
            return new DeviceUser[size];
        }
    };
}
