package com.martin.codepo.database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;

import java.util.ArrayList;

/**
 * Created by martin on 16/03/17.
 */

public class DeviceDAO extends AbstractDAO{

    public static final String DeviceTable = "Device";
    public static final String DeviceKey = "DeviceKey";
    public static final String DeviceName = "Name";
    public static final String DeviceIP = "IP";
    public static final String DeviceType = "Type";
    public static final String DeviceCreateTable = "CREATE TABLE IF NOT EXISTS " + DeviceTable + " ("+
            DeviceKey + " TEXT PRIMARY KEY, " +
            DeviceIP + " TEXT," +
            DeviceName + " TEXT," +
            DeviceType + " INTEGER, " +
            CenterDAO.CenterKey + " TEXT, " +
            CheckTimeDAO.CheckTimeKey + " TEXT, " +
            "FOREIGN KEY (" + CheckTimeDAO.CheckTimeKey + ") REFERENCES " + CheckTimeDAO.CheckTimeKey + "(" + CheckTimeDAO.CheckTimeKey + "), " +
            "FOREIGN KEY (" + CenterDAO.CenterKey + ") REFERENCES " + CenterDAO.CenterKey + "(" + CenterDAO.CenterKey + ") );";

    //private CheckTimeDAO checkTimeDAO;
    private ThresholdDAO thresholdDAO;
    private DeviceConfigDAO deviceConfigDAO;
    private DeviceUserDAO deviceUserDAO;

    public DeviceDAO(Context context) {
        super(context);
        //checkTimeDAO = new CheckTimeDAO(context);
        thresholdDAO = new ThresholdDAO(context);
        deviceConfigDAO = new DeviceConfigDAO(context);
        deviceUserDAO = new DeviceUserDAO(context);
    }


    public void addDevice(Device device){
        open();
        ContentValues contentValues = new ContentValues();
        contentValues.put(DeviceKey, device.getId());
        contentValues.put(DeviceIP, device.getIp());
        contentValues.put(DeviceName, device.getName());
        contentValues.put(DeviceType, device.getDeviceType());
        contentValues.put(CenterDAO.CenterKey, device.getMonitoringKey());
        sqLiteDatabase.insert(DeviceTable, null, contentValues);
        close();
        //checkTimeDAO.add(device.getCheckTime());
        thresholdDAO.addThreshold(device.getThreshold());
        deviceConfigDAO.addDeviceConfig(device.getDeviceConfig());
        deviceUserDAO.addAllDeviceUser(device.getDeviceUsers());
    }

    public void delDevice(String id){
        open();
        sqLiteDatabase.delete(DeviceTable, DeviceKey + " = ?", new String[]{id});
        //sqLiteDatabase.delete(CheckTimeDAO.CheckTimeTable, DeviceKey + " = ?", new String[]{id});
        sqLiteDatabase.delete(ThresholdDAO.ThresholdTable, DeviceKey + " = ?", new String[]{id});
        sqLiteDatabase.delete(DeviceDataDAO.DeviceDataTable, DeviceKey + " = ?", new String[]{id});
        sqLiteDatabase.delete(DeviceConfigDAO.DeviceConfigTable, DeviceKey + " = ?", new String[]{id});
        sqLiteDatabase.delete(DeviceUserDAO.DeviceUserTable, DeviceKey + " = ?", new String[]{id});
        close();
    }


    public void updateDevice(Device device){
        open();
        ContentValues contentValues = new ContentValues();
        contentValues.put(DeviceIP, device.getIp());
        contentValues.put(DeviceName, device.getName());
        contentValues.put(DeviceType, device.getDeviceType());
        sqLiteDatabase.update(DeviceTable, contentValues, DeviceKey + " = ?", new String[]{device.getId()});
        close();
        //checkTimeDAO.updateCheckTime(device.getCheckTime());
        thresholdDAO.updateThreshold(device.getThreshold());
        deviceConfigDAO.updateDeviceConfig(device.getDeviceConfig());
        deviceUserDAO.updateAllDeviceUser(device.getId(), device.getDeviceUsers());
    }

    public Device selectDevice(String id, boolean link){
        open();
        Device device = null;
        Cursor cursor = sqLiteDatabase.rawQuery("select * from " + DeviceTable + " where " + DeviceKey + " = ?", new String[]{id});
        while (cursor.moveToNext()) {
            device = new Device(cursor.getString(0), cursor.getString(1), cursor.getString(2), cursor.getInt(3), cursor.getString(4));
        }
        close();
        if(link){
            assert device != null;
            device.setThreshold(thresholdDAO.selectThresholdDeviceKey(device.getId()));
            device.setDeviceConfig(deviceConfigDAO.selectConfigDeviceKey(device.getId()));
            device.setDeviceUsers(deviceUserDAO.selectDeviceUserByDeviceKey(device.getId()));
        }
        return device;
    }

    public ArrayList<Device> selectAllDevices(boolean link){
        open();
        Cursor cursor =  sqLiteDatabase.rawQuery("select * from " + DeviceTable, null);
        ArrayList<Device> devices = new ArrayList<>();
        Device device;
        while(cursor.moveToNext()){
            device = new Device(cursor.getString(0), cursor.getString(1), cursor.getString(2), cursor.getInt(3), cursor.getString(4));
            //device.setCheckTime(checkTimeDAO.selectCheckTimeByDeviceType(device.getDeviceType()));
            if(link){
                device.setThreshold(thresholdDAO.selectThresholdDeviceKey(device.getId()));
                device.setDeviceConfig(deviceConfigDAO.selectConfigDeviceKey(device.getId()));
                device.setDeviceUsers(deviceUserDAO.selectDeviceUserByDeviceKey(device.getId()));
            }
            devices.add(device);
        }
        close();
        return  devices;
    }

    public ArrayList<Device> selectAllDevicesType(int deviceType){
        open();
        Cursor cursor = sqLiteDatabase.rawQuery("select * from " + DeviceTable + " where " + DeviceType + " = ?", new String[]{String.valueOf(deviceType)});
        ArrayList<Device> devices = new ArrayList<>();
        Device device;
        while (cursor.moveToNext()){
            device =  new Device(cursor.getString(0), cursor.getString(1), cursor.getString(2), cursor.getInt(3), cursor.getString(4));
            //device.setCheckTime(checkTimeDAO.selectCheckTimeByDeviceType(device.getDeviceType()));

            device.setThreshold(thresholdDAO.selectThresholdDeviceKey(device.getId()));
            device.setDeviceConfig(deviceConfigDAO.selectConfigDeviceKey(device.getId()));
            device.setDeviceUsers(deviceUserDAO.selectDeviceUserByDeviceKey(device.getId()));
            devices.add(device);
        }
        close();
        return  devices;
    }

    public ArrayList<Device> selectAllDevicesByUser(ArrayList<DeviceUser> deviceUsers, boolean link){
        ArrayList<Device> devices = new ArrayList<>();
        for(DeviceUser deviceUser : deviceUsers){
            devices.add(selectDevice(deviceUser.getDeviceKey(), link));
        }
        return devices;
    }
}
