package com.martin.codepo.database;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by martin on 9/03/18.
 */

public class Threshold implements Parcelable {

    private String id;
    private float first;
    private float second;
    private float third;
    private String deviceKey;
    private String type;

    public Threshold(String id, float first, float second, float third, String deviceKey){
        this.id = id;
        this.first = first;
        this.second = second;
        this.third = third;
        this.deviceKey = deviceKey;
        this.type = "threshold";
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public float getFirst() {
        return first;
    }

    public void setFirst(float first) {
        this.first = first;
    }

    public float getSecond() {
        return second;
    }

    public void setSecond(float second) {
        this.second = second;
    }

    public float getThird() {
        return third;
    }

    public void setThird(float third) {
        this.third = third;
    }

    public String getDeviceKey() {
        return deviceKey;
    }

    public void setDeviceKey(String deviceKey) {
        this.deviceKey = deviceKey;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.id);
        dest.writeFloat(this.first);
        dest.writeFloat(this.second);
        dest.writeFloat(this.third);
        dest.writeString(this.deviceKey);
    }

    protected Threshold(Parcel in) {
        this.id = in.readString();
        this.first = in.readFloat();
        this.second = in.readFloat();
        this.third = in.readFloat();
        this.deviceKey = in.readString();
    }

    public static final Parcelable.Creator<Threshold> CREATOR = new Parcelable.Creator<Threshold>() {
        @Override
        public Threshold createFromParcel(Parcel source) {
            return new Threshold(source);
        }

        @Override
        public Threshold[] newArray(int size) {
            return new Threshold[size];
        }
    };
}
