package com.martin.codepo.database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.DatabaseUtils;

import java.util.ArrayList;

/**
 * Created by martin on 9/03/18.
 */

public class DeviceDataDAO extends AbstractDAO{

    public static final String DeviceDataTable = "DeviceData";
    public static final String DeviceDataKey = "DataKey";
    public static final String DeviceDataTime = "Time";
    public static final String DeviceDataFirst = "First";
    public static final String DeviceDataSecond = "Second";
    public static final String DeviceDataThird = "Third";
    public static final String DeviceDataCreateTable = "CREATE TABLE IF NOT EXISTS " + DeviceDataTable + " ("+
            DeviceDataKey + " TEXT PRIMARY KEY, " +
            DeviceDataTime + " TEXT," +
            DeviceDataFirst + " REAL," +
            DeviceDataSecond + " REAL, " +
            DeviceDataThird + " REAL, " +
            DeviceDAO.DeviceKey + " TEXT, " +
            "FOREIGN KEY (" + DeviceDAO.DeviceKey+ ") REFERENCES " + DeviceDAO.DeviceKey + "(" + DeviceDAO.DeviceKey + ") );";

    public DeviceDataDAO(Context context) {
        super(context);
    }

    public void addDeviceData(DeviceData deviceData){
        open();
        ContentValues deviceDataValues = new ContentValues();
        deviceDataValues.put(DeviceDataKey, deviceData.getId());
        deviceDataValues.put(DeviceDataTime, deviceData.getTimeStamp());
        deviceDataValues.put(DeviceDataFirst, deviceData.getFirst());
        deviceDataValues.put(DeviceDataSecond, deviceData.getSecond());
        deviceDataValues.put(DeviceDataThird, deviceData.getThird());
        deviceDataValues.put(DeviceDAO.DeviceKey, deviceData.getDeviceKey());
        sqLiteDatabase.insert(DeviceDataTable, null, deviceDataValues);
        close();
        deleteLastDataByDeviceKey(deviceData);
    }

    public void delDeviceData(String id){
        open();
        close();
    }

    public float getAverageValue(String deviceKey) {
        open();
        Cursor cursor = sqLiteDatabase.rawQuery("select " + DeviceDataFirst + " from " + DeviceDataTable + " where " + DeviceDAO.DeviceKey + " = ?", new String[]{deviceKey});
        int size = 0;
        float sum = 0;
        while (cursor.moveToNext()){
            sum += cursor.getFloat(0);
            size += 1;
        }
        close();
        return sum/size;
    }

    public ArrayList<DeviceData> selectAllDataDeviceKey(String id) { //TODO verify if data sorted by datetime
        open();
        Cursor cursor = sqLiteDatabase.rawQuery("select * from " + DeviceDataTable + " where " + DeviceDAO.DeviceKey + " = ?", new String[]{id});
        DeviceData deviceData;
        ArrayList<DeviceData> deviceDatas = new ArrayList<>();
        while (cursor.moveToNext()){
            deviceData = new DeviceData(cursor.getString(0), cursor.getString(1), cursor.getFloat(2), cursor.getFloat(3), cursor.getFloat(4), id);
            deviceDatas.add(deviceData);
        }
        close();
        return deviceDatas;
    }

    public DeviceData selectLastDeviceDataByDeviceKey(String deviceKey){ // TODO
        open();
        Cursor cursor = sqLiteDatabase.rawQuery("select * from " + DeviceDataTable + " where " + DeviceDAO.DeviceKey + " = ? order by datetime(" + DeviceDataTime + ") desc limit 1", new String[]{deviceKey});
        DeviceData deviceData = null;
        while(cursor.moveToNext()){
            deviceData = new DeviceData(cursor.getString(0), cursor.getString(1), cursor.getFloat(2), cursor.getFloat(3), cursor.getFloat(3), deviceKey);
        }
        close();
        return deviceData;
    }

    public ArrayList<DeviceData> selectFrameDeviceDataByDeviceKey(String deviceKey, int frame){
        open();
        Cursor cursor = sqLiteDatabase.rawQuery("select * from " + DeviceDataTable + " where " + DeviceDAO.DeviceKey + " = ? order by " + DeviceDataTime + " desc limit ?", new String[]{deviceKey, String.valueOf(frame)});
        ArrayList<DeviceData> deviceDatas = new ArrayList<>();
        DeviceData deviceData;
        while(cursor.moveToNext()){
            deviceData = new DeviceData(cursor.getString(0), cursor.getString(1), cursor.getFloat(2), cursor.getFloat(3), cursor.getFloat(3), deviceKey);
            deviceDatas.add(deviceData);
        }
        close();
        return deviceDatas;
    }

    public void deleteLastDataByDeviceKey(DeviceData deviceData){
        open();
        if(DatabaseUtils.longForQuery(sqLiteDatabase, "select count(*) from " + DeviceDataTable + " where " + DeviceDAO.DeviceKey + " = ?", new String[]{deviceData.getDeviceKey()}) > 3500){
            sqLiteDatabase.delete(DeviceDataTable, DeviceDataTime + " = ( select min(" + DeviceDataTime + ") from " + DeviceDataTable + " where " + DeviceDAO.DeviceKey + " = ?)", new String[]{deviceData.getDeviceKey()});
        }
        close();
    }

    public ArrayList<DeviceData> selectAllDeviceData(){
        open();
        Cursor cursor = sqLiteDatabase.rawQuery("select * from " + DeviceDataTable, null);
        ArrayList<DeviceData> deviceDatas = new ArrayList<>();
        DeviceData deviceData;
        while (cursor.moveToNext()){
            deviceData = new DeviceData(cursor.getString(0), cursor.getString(1), cursor.getFloat(2), cursor.getFloat(3), cursor.getFloat(3), cursor.getString(4));
            deviceDatas.add(deviceData);
        }
        close();
        return deviceDatas;
    }

}
