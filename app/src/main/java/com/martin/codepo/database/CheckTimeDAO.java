package com.martin.codepo.database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.util.Log;

import java.util.ArrayList;

/**
 * Created by user on 16/03/17.
 */

public class CheckTimeDAO extends AbstractDAO {

    public static final String CheckTimeTable = "ModifyCheckTime";
    public static final String CheckTimeKey = "CheckTimeKey";
    public static final String CheckTimeControl = "Control";
    public static final String CheckTimeFirstNotif = "FirstNotif";
    public static final String CheckTimeIntervalNotif = "IntervalNotif";
    public static final String CheckTimeCreateTable = "CREATE TABLE IF NOT EXISTS " + CheckTimeTable + " (" +
            CheckTimeKey + " INTEGER PRIMARY KEY, " +
            CheckTimeControl + " INTEGER, " +
            CheckTimeFirstNotif + " INTEGER, " +
            CheckTimeIntervalNotif + " INTEGER );";

    public CheckTimeDAO(Context context) {
        super(context);
    }

    public void add(CheckTime checkTime){
        open();
        ContentValues contentValues = new ContentValues();
        contentValues.put(CheckTimeKey, checkTime.getId());
        contentValues.put(CheckTimeControl, checkTime.getControl());
        contentValues.put(CheckTimeFirstNotif, checkTime.getFirstNotif());
        contentValues.put(CheckTimeIntervalNotif, checkTime.getIntervalNotif());
        sqLiteDatabase.insert(CheckTimeTable, null, contentValues);
        close();
    }

    public void del(int id){
        //TODO
    }

    public void updateCheckTime(CheckTime checkTime){
        open();
        ContentValues contentValues = new ContentValues();
        contentValues.put(CheckTimeControl, checkTime.getControl());
        contentValues.put(CheckTimeFirstNotif, checkTime.getFirstNotif());
        contentValues.put(CheckTimeIntervalNotif, checkTime.getIntervalNotif());
        sqLiteDatabase.update(CheckTimeTable, contentValues, CheckTimeKey + " = ? ", new String[]{String.valueOf(checkTime.getId())});
        close();
    }

    public CheckTime selectCheckTimeByDeviceType(int deviceType){
        open();
        Cursor cursor = sqLiteDatabase.rawQuery("select * from " + CheckTimeTable + " where " + CheckTimeKey + " = ?", new String[]{String.valueOf(deviceType)});
        CheckTime checkTime = null;
        while (cursor.moveToNext()){
            checkTime = new CheckTime(cursor.getInt(0), cursor.getInt(1), cursor.getInt(2), cursor.getInt(3));
        }
        close();
        return checkTime;
    }

    public ArrayList<CheckTime> selectAllCheckTime(){
        open();
        Cursor cursor = sqLiteDatabase.rawQuery("select * from " + CheckTimeTable, null);
        ArrayList<CheckTime> checkTimes = new ArrayList<>();
        CheckTime checkTime;
        while (cursor.moveToNext()){
            checkTime = new CheckTime(cursor.getInt(0), cursor.getInt(1), cursor.getInt(2), cursor.getInt(3));
            checkTimes.add(checkTime);
        }
        close();
        return checkTimes;
    }

    public void initCheckTime(){
        for(int i = 0; i < 7; i ++){
            ContentValues contentValues = new ContentValues();
            contentValues.put(CheckTimeKey, i);
            contentValues.put(CheckTimeControl, 3);
            contentValues.put(CheckTimeFirstNotif, 2);
            contentValues.put(CheckTimeIntervalNotif, 2);
            sqLiteDatabase.insert(CheckTimeTable, null, contentValues);
        }
    }

}
