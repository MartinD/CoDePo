package com.martin.codepo.database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;

/**
 * Created by martin on 9/03/18.
 */

public class CenterDAO extends AbstractDAO {

    public static final String CenterTable = "Center";
    public static final String CenterKey = "CenterKey";
    public static final String CenterName = "CenterName";
    public static final String CenterAddress = "Address";
    public static final String CenterResponsible = "Responsible";
    public static final String CenterTelResponsible = "TelResponsible";
    public static final String CenterOnline = "Online";
    public static final String CenterCreateTable = "CREATE TABLE IF NOT EXISTS " + CenterTable + " (" +
            CenterKey + " TEXT PRIMARY KEY, " +
            CenterName + " TEXT, " +
            CenterAddress + " TEXT, " +
            CenterResponsible + " TEXT, " +
            CenterTelResponsible + " TEXT, " +
            CenterOnline + " INTEGER);";

    public CenterDAO(Context context) {
        super(context);
    }

    public void addCenter(Center center){
        open();
        ContentValues contentValues = new ContentValues();
        contentValues.put(CenterKey, center.getId());
        contentValues.put(CenterName, center.getName());
        contentValues.put(CenterAddress, center.getAddress());
        contentValues.put(CenterResponsible, center.getResponsible());
        contentValues.put(CenterTelResponsible, center.getPhone());
        contentValues.put(CenterOnline, center.getIsOnline());
        sqLiteDatabase.insert(CenterTable, null, contentValues);
        close();
    }

    public void delCenter(){
        open();
        sqLiteDatabase.delete(CenterTable, null, null);
        sqLiteDatabase.delete(UserDAO.UserTable, null, null);
        sqLiteDatabase.delete(DeviceDAO.DeviceTable, null, null);
        close();
    }

    public void updateCenter(Center center){
        open();
        ContentValues contentValues = new ContentValues();
        contentValues.put(CenterKey, center.getId());
        contentValues.put(CenterName, center.getName());
        contentValues.put(CenterAddress, center.getAddress());
        contentValues.put(CenterResponsible, center.getResponsible());
        contentValues.put(CenterTelResponsible, center.getPhone());
        contentValues.put(CenterOnline, center.getIsOnline());
        sqLiteDatabase.update(CenterTable, contentValues, CenterKey + " = ?", new String[]{center.getId()});
        ContentValues userValues = new ContentValues();
        userValues.put(CenterKey, center.getId());
        sqLiteDatabase.update(UserDAO.UserTable, userValues, null, null);
        ContentValues deviceValues = new ContentValues();
        deviceValues.put(CenterKey, center.getId());
        sqLiteDatabase.update(DeviceDAO.DeviceTable, deviceValues, null, null);
        close();

    }

    public Boolean selectCenterIsOnline(){
        open();
        Cursor cursor = sqLiteDatabase.rawQuery("select " + CenterOnline + " from " + CenterTable, null, null);
        int isOnline = 0;
        while (cursor.moveToNext()){
            isOnline = cursor.getInt(0);
        }
        close();
        return isOnline == 1;
    }

    public Center selectCenter(){
        open();
        Cursor cursor = sqLiteDatabase.rawQuery("select * from " + CenterTable, null, null );
        Center center = null;
        while (cursor.moveToNext()){
            center = new Center(cursor.getString(0), cursor.getString(1), cursor.getString(2), cursor.getString(3), cursor.getString(4), cursor.getInt(5));
        }
        close();
        return center;
    }
}
