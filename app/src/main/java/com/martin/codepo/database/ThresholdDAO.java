package com.martin.codepo.database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;

import java.util.ArrayList;

/**
 * Created by martin on 9/03/18.
 */

public class ThresholdDAO extends AbstractDAO{

    public static final String ThresholdTable = "Threshold";
    public static final String ThresholdKey = "ThresholdKey";
    public static final String ThresholdFirst = "First";
    public static final String ThresholdSecond = "Second";
    public static final String ThresholdThird = "Third";
    public static final String ThresholdCreateTable = "CREATE TABLE IF NOT EXISTS " + ThresholdTable + " ("+
            ThresholdKey + " TEXT PRIMARY KEY, " +
            ThresholdFirst + " REAL," +
            ThresholdSecond + " REAL," +
            ThresholdThird + " REAL," +
            DeviceDAO.DeviceKey + " TEXT," +
            "FOREIGN KEY (" + DeviceDAO.DeviceKey + ") REFERENCES " + DeviceDAO.DeviceKey + "(" + DeviceDAO.DeviceKey + ") );";


    public ThresholdDAO(Context context) {
        super(context);
    }

    public void addThreshold(Threshold threshold){
        open();
        ContentValues thresholdValues = new ContentValues();
        thresholdValues.put(ThresholdKey, threshold.getId());
        thresholdValues.put(ThresholdFirst, threshold.getFirst());
        thresholdValues.put(ThresholdSecond, threshold.getSecond());
        thresholdValues.put(ThresholdThird, threshold.getThird());
        thresholdValues.put(DeviceDAO.DeviceKey, threshold.getDeviceKey());
        sqLiteDatabase.insert(ThresholdTable, null, thresholdValues);
        close();
    }

    public void delThreshold(String id){
        open();
        close();
    }

    public void updateThreshold(Threshold threshold){
        open();
        ContentValues thresholdValues = new ContentValues();
        thresholdValues.put(ThresholdFirst, threshold.getFirst());
        thresholdValues.put(ThresholdSecond, threshold.getSecond());
        thresholdValues.put(ThresholdThird, threshold.getThird());
        sqLiteDatabase.update(ThresholdTable, thresholdValues, ThresholdKey + " = ?", new String[]{threshold.getId()});
        close();
    }

    public Threshold selectThresholdDeviceKey(String deviceKey){
        open();
        Cursor cursor = sqLiteDatabase.rawQuery("select * from " + ThresholdTable + " where " + DeviceDAO.DeviceKey + " = ?", new String[]{deviceKey});
        Threshold threshold = null;
        while (cursor.moveToNext()) {
            threshold = new Threshold(cursor.getString(0), cursor.getFloat(1), cursor.getFloat(2), cursor.getFloat(3), cursor.getString(4));
        }
        close();
        return threshold;
    }

    public ArrayList<Threshold> selectAllThreshold() {
        open();
        Cursor cursor = sqLiteDatabase.rawQuery("select * from " + ThresholdTable, null);
        Threshold threshold;
        ArrayList<Threshold> thresholds = new ArrayList<>();
        while (cursor.moveToNext()){
            threshold = new Threshold(cursor.getString(0), cursor.getFloat(1), cursor.getFloat(2), cursor.getFloat(3), cursor.getString(4));
            thresholds.add(threshold);
        }
        close();
        return thresholds;
    }
}
