package com.martin.codepo.database;

import android.content.Context;
import android.database.DatabaseUtils;
import android.database.sqlite.SQLiteDatabase;

/**
 * Created by user on 16/03/17.
 */

public abstract class AbstractDAO {

    protected final static int version = 1;
    protected final static String name = "codepo.db";
    protected SQLiteDatabase sqLiteDatabase = null;
    protected Database database = null;

    public AbstractDAO(Context context){
        this.database = new Database(context);

    }

    public SQLiteDatabase open() {
        this.sqLiteDatabase = database.getWritableDatabase();
        return sqLiteDatabase;
    }

    public void close() {
        sqLiteDatabase.close();
    }

    public SQLiteDatabase getSqLiteDatabase() {
        return sqLiteDatabase;
    }



}
