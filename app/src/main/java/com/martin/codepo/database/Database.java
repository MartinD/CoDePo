package com.martin.codepo.database;

import android.content.ContentValues;
import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

/**
 * Created by user on 16/03/17.
 */

public class Database extends SQLiteOpenHelper {

    private static final String databaseName = "codepo.db";
    private static final int databaseVersion = 1;
    private Context context;

    public Database(Context context) {
        super(context, databaseName, null, databaseVersion);
        this.context = context;
    }

    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase) {

        sqLiteDatabase.execSQL(CenterDAO.CenterCreateTable);
        sqLiteDatabase.execSQL(UserDAO.UserCreateTable);
        sqLiteDatabase.execSQL(DeviceDAO.DeviceCreateTable);
        sqLiteDatabase.execSQL(DeviceDataDAO.DeviceDataCreateTable);
        sqLiteDatabase.execSQL(DeviceConfigDAO.DeviceConfigCreateTable);
        sqLiteDatabase.execSQL(ThresholdDAO.ThresholdCreateTable);
        sqLiteDatabase.execSQL(CheckTimeDAO.CheckTimeCreateTable);
        sqLiteDatabase.execSQL(DeviceUserDAO.DeviceUserCreateTable);

        for(int i = 0; i < 7; i ++){
            ContentValues contentValues = new ContentValues();
            contentValues.put(CheckTimeDAO.CheckTimeKey, i);
            contentValues.put(CheckTimeDAO.CheckTimeControl, 3);
            contentValues.put(CheckTimeDAO.CheckTimeFirstNotif, 2);
            contentValues.put(CheckTimeDAO.CheckTimeIntervalNotif, 2);
            sqLiteDatabase.insert(CheckTimeDAO.CheckTimeTable, null, contentValues);
        }

    }

    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int i, int i1) {

        sqLiteDatabase.execSQL("DROP TABLE IF EXISTS " + CenterDAO.CenterTable);
        sqLiteDatabase.execSQL("DROP TABLE IF EXISTS " + DeviceDAO.DeviceTable);
        sqLiteDatabase.execSQL("DROP TABLE IF EXISTS " + DeviceDataDAO.DeviceDataTable);
        sqLiteDatabase.execSQL("DROP TABLE IF EXISTS " + DeviceConfigDAO.DeviceConfigTable);
        sqLiteDatabase.execSQL("DROP TABLE IF EXISTS " + CheckTimeDAO.CheckTimeTable);
        sqLiteDatabase.execSQL("DROP TABLE IF EXISTS " + UserDAO.UserTable);
        sqLiteDatabase.execSQL("DROP TABLE IF EXISTS " + ThresholdDAO.ThresholdTable);
        sqLiteDatabase.execSQL("DROP TABLE IF EXISTS " + DeviceUserDAO.DeviceUserTable);

        onCreate(sqLiteDatabase);
    }

    public int getDatabaseVersion(){
        return databaseVersion;
    }

    private void initDatabase(){
        CheckTimeDAO checkTimeDAO = new CheckTimeDAO(context);
        checkTimeDAO.initCheckTime();
    }

}
