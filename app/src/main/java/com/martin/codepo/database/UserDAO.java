package com.martin.codepo.database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;

import com.martin.codepo.activity.Monitoring;

import java.util.ArrayList;

/**
 * Created by martin on 9/03/18.
 */

public class UserDAO extends AbstractDAO{

    public static final String UserTable = "User";
    public static final String UserKey = "UserKey";
    public static final String UserName = "Name";
    public static final String UserFirstname = "Firstname";
    public static final String UserEmail = "Email";
    public static final String UserPassword = "Password";
    public static final String UserCountry = "Country";
    public static final String UserPrefix = "Prefix";
    public static final String UserPhone = "Phone";
    public static final String UserCreateTable = "CREATE TABLE IF NOT EXISTS " + UserTable + " (" +
            UserKey + " TEXT PRIMARY KEY, " +
            UserName + " TEXT, " +
            UserFirstname + " TEXT, " +
            UserEmail + " TEXT, " +
            UserPassword + " INTEGER, " +
            UserCountry + " TEXT, " +
            UserPrefix + " TEXT, " +
            UserPhone + " TEXT, " +
            CenterDAO.CenterKey + " TEXT, " +
            "FOREIGN KEY (" + CenterDAO.CenterKey + ") REFERENCES " + CenterDAO.CenterKey + "(" + CenterDAO.CenterKey + ") );";

    private DeviceUserDAO deviceUserDAO;

    public UserDAO(Context context) {
        super(context);
        deviceUserDAO = new DeviceUserDAO(context);
    }

    public void addUser(User user){
        open();
        ContentValues contentValues = new ContentValues();
        contentValues.put(UserKey, user.getId());
        contentValues.put(UserName, user.getName());
        contentValues.put(UserFirstname, user.getFirstname());
        contentValues.put(UserEmail, user.getEmail());
        contentValues.put(UserPassword, user.getPassword());
        contentValues.put(UserCountry, user.getCountry());
        contentValues.put(UserPrefix, user.getPrefix());
        contentValues.put(UserPhone, user.getPhone());
        contentValues.put(CenterDAO.CenterKey, user.getCenterKey());
        sqLiteDatabase.insert(UserTable, null, contentValues);
        close();
        deviceUserDAO.addAllDeviceUser(user.getDeviceUsers());
    }

    public void delUser(String id){
        open();
        sqLiteDatabase.delete(UserTable, UserKey + " = ?", new String[]{id});
        sqLiteDatabase.delete(DeviceUserDAO.DeviceUserTable, UserKey + " = ?", new String[]{id});
        close();
    }

    public void updateUser(User user){
        open();
        ContentValues userValues = new ContentValues();
        userValues.put(UserName, user.getName());
        userValues.put(UserFirstname, user.getFirstname());
        userValues.put(UserEmail, user.getEmail());
        userValues.put(UserPassword, user.getPassword());
        userValues.put(UserCountry, user.getCountry());
        userValues.put(UserPrefix, user.getPrefix());
        userValues.put(UserPhone, user.getPhone());
        sqLiteDatabase.update(UserTable, userValues, UserKey + " = ?", new String[]{user.getId()});
        close();
        deviceUserDAO.updateAllDeviceUserByUser(user.getId(), user.getDeviceUsers());
    }

    public User selectUser(String id){
        open();
        Cursor cursor = sqLiteDatabase.rawQuery("select * from " + UserTable + " where " + UserKey + " = ?", new String[]{id});
        User user = null;
        while (cursor.moveToNext()) {
            user = new User(cursor.getString(0), cursor.getString(1), cursor.getString(2), cursor.getString(3),
                    cursor.getString(4), cursor.getString(5), cursor.getString(6), cursor.getString(7), cursor.getString(8));
        }
        close();
        user.setDeviceUsers(deviceUserDAO.selectDeviceUserByUserKey(id));
        return user;
    }

    public ArrayList<User> selectAllUser(){
        open();
        Cursor cursor = sqLiteDatabase.rawQuery("select * from " + UserTable, null, null);
        ArrayList<User> users = new ArrayList<>();
        User user;
        while (cursor.moveToNext()){
            user = new User(cursor.getString(0), cursor.getString(1), cursor.getString(2), cursor.getString(3),
                    cursor.getString(4), cursor.getString(5), cursor.getString(6), cursor.getString(7), cursor.getString(8));
            user.setDeviceUsers(deviceUserDAO.selectDeviceUserByUserKey(user.getId()));
            users.add(user);
        }
        close();
        return users;
    }

    public ArrayList<User> selectAllUserByDevice(ArrayList<DeviceUser> deviceUsers) {
        ArrayList<User> users = new ArrayList<>();
        for(DeviceUser deviceUser : deviceUsers){
            users.add(selectUser(deviceUser.getUserKey()));
        }
        return users;
    }

    public ArrayList<User> selectAllUsersDistinct(ArrayList<User> users){
        ArrayList<User> allUsers = selectAllUser();
        ArrayList<User> distinctUsers = new ArrayList<>();
        for(User user : allUsers){
            if(!users.contains(user)){
                distinctUsers.add(user);
            }
        }
        return distinctUsers;
    }
}
