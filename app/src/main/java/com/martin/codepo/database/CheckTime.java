package com.martin.codepo.database;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by user on 16/03/17.
 */

public class CheckTime implements Parcelable {

    private Integer id;
    private int control;
    private int firstNotif;
    private int intervalNotif;
    private String type;

    public CheckTime(Integer id, int control, int firstNotif, int intervalNotif){
        this.id = id;
        this.control = control;
        this.firstNotif = firstNotif;
        this.intervalNotif = intervalNotif;
        this.type = "checkTime";
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public int getControl() {
        return control;
    }

    public void setControl(int control) {
        this.control = control;
    }

    public int getFirstNotif() {
        return firstNotif;
    }

    public void setFirstNotif(int firstNotif) {
        this.firstNotif = firstNotif;
    }

    public int getIntervalNotif() {
        return intervalNotif;
    }

    public void setIntervalNotif(int intervalNotif) {
        this.intervalNotif = intervalNotif;
    }


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeValue(this.id);
        dest.writeInt(this.control);
        dest.writeInt(this.firstNotif);
        dest.writeInt(this.intervalNotif);
    }

    protected CheckTime(Parcel in) {
        this.id = (Integer) in.readValue(Integer.class.getClassLoader());
        this.control = in.readInt();
        this.firstNotif = in.readInt();
        this.intervalNotif = in.readInt();
    }

    public static final Parcelable.Creator<CheckTime> CREATOR = new Parcelable.Creator<CheckTime>() {
        @Override
        public CheckTime createFromParcel(Parcel source) {
            return new CheckTime(source);
        }

        @Override
        public CheckTime[] newArray(int size) {
            return new CheckTime[size];
        }
    };
}
