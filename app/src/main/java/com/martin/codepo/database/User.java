package com.martin.codepo.database;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.ArrayList;

/**
 * Created by martin on 9/03/18.
 */

public class User implements Parcelable {

    private String id;
    private String name;
    private String firstname;
    private String email;
    private String password;
    private String country;
    private String prefix;
    private String phone;
    private String centerKey;
    private String type;

    private ArrayList<DeviceUser> deviceUsers;

    public User(String id, String name, String firstname, String email, String password, String country, String prefix, String phone, String centerKey) {
        this.id = id;
        this.name = name;
        this.firstname = firstname;
        this.email = email;
        this.password = password;
        this.country = country;
        this.prefix = prefix;
        this.phone = phone;
        this.centerKey = centerKey;
        this.type = "user";
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getFirstname() {
        return firstname;
    }

    public void setFirstname(String firstname) {
        this.firstname = firstname;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getPrefix() {
        return prefix;
    }

    public void setPrefix(String prefix) {
        this.prefix = prefix;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getCenterKey() {
        return centerKey;
    }

    public void setCenterKey(String centerKey) {
        this.centerKey = centerKey;
    }

    public ArrayList<DeviceUser> getDeviceUsers() {
        return deviceUsers;
    }

    public void setDeviceUsers(ArrayList<DeviceUser> deviceUsers) {
        this.deviceUsers = deviceUsers;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.id);
        dest.writeString(this.name);
        dest.writeString(this.firstname);
        dest.writeString(this.email);
        dest.writeString(this.password);
        dest.writeString(this.country);
        dest.writeString(this.prefix);
        dest.writeString(this.phone);
        dest.writeString(this.centerKey);
        dest.writeTypedList(this.deviceUsers);
    }

    protected User(Parcel in) {
        this.id = in.readString();
        this.name = in.readString();
        this.firstname = in.readString();
        this.email = in.readString();
        this.password = in.readString();
        this.country = in.readString();
        this.prefix = in.readString();
        this.phone = in.readString();
        this.centerKey = in.readString();
        this.deviceUsers = in.createTypedArrayList(DeviceUser.CREATOR);
    }

    public static final Parcelable.Creator<User> CREATOR = new Parcelable.Creator<User>() {
        @Override
        public User createFromParcel(Parcel source) {
            return new User(source);
        }

        @Override
        public User[] newArray(int size) {
            return new User[size];
        }
    };
}
