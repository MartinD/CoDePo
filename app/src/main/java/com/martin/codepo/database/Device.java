package com.martin.codepo.database;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.ArrayList;

/**
 * Created by user on 16/03/17.
 */

public class Device implements Parcelable {

    //TODO rajouter checkTimeKey comme attribut pour ne pas avoir de problème sur la plateforme web
    //TODO changer le deviceType en string pour faciliter le passage à la plateforme web
    private String ip;
    private String name;
    private String id;
    private int deviceType;
    private String monitoringKey;
    private String type;

    private ArrayList<DeviceUser> deviceUsers;
    private Threshold threshold;
    private DeviceConfig deviceConfig;

    public Device(String id, String ip, String name, int deviceType, String monitoringKey){
        this.id = id;
        this.ip = ip;
        this.name = name;
        this.deviceType = deviceType;
        this.monitoringKey = monitoringKey;
        this.type = "device";
    }

    public Device(int deviceType){
        this.deviceType = deviceType;
    }

    public String getIp() {
        return ip;
    }

    public void setIp(String ip) {
        this.ip = ip;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public int getDeviceType() {
        return deviceType;
    }

    public void setDeviceType(int deviceType) {
        this.deviceType = deviceType;
    }

    public String getMonitoringKey() {
        return monitoringKey;
    }

    public void setMonitoringKey(String monitoringKey) {
        this.monitoringKey = monitoringKey;
    }

    public ArrayList<DeviceUser> getDeviceUsers() {
        return deviceUsers;
    }

    public void setDeviceUsers(ArrayList<DeviceUser> users) {
        this.deviceUsers = users;
    }

    public Threshold getThreshold() {
        return threshold;
    }

    public void setThreshold(Threshold threshold) {
        this.threshold = threshold;
    }

    public DeviceConfig getDeviceConfig() {
        return deviceConfig;
    }

    public void setDeviceConfig(DeviceConfig deviceConfig) {
        this.deviceConfig = deviceConfig;
    }


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.ip);
        dest.writeString(this.name);
        dest.writeString(this.id);
        dest.writeInt(this.deviceType);
        dest.writeString(this.monitoringKey);
        dest.writeTypedList(this.deviceUsers);
        //dest.writeParcelable(this.checkTime, flags);
        dest.writeParcelable(this.threshold, flags);
        dest.writeParcelable(this.deviceConfig, flags);
    }

    protected Device(Parcel in) {
        this.ip = in.readString();
        this.name = in.readString();
        this.id = in.readString();
        this.deviceType = in.readInt();
        this.monitoringKey = in.readString();
        this.deviceUsers = in.createTypedArrayList(DeviceUser.CREATOR);
        this.threshold = in.readParcelable(Threshold.class.getClassLoader());
        this.deviceConfig = in.readParcelable(DeviceConfig.class.getClassLoader());
    }

    public static final Parcelable.Creator<Device> CREATOR = new Parcelable.Creator<Device>() {
        @Override
        public Device createFromParcel(Parcel source) {
            return new Device(source);
        }

        @Override
        public Device[] newArray(int size) {
            return new Device[size];
        }
    };
}
