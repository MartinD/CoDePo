package com.martin.codepo.database;

/**
 * Created by martin on 9/03/18.
 */

public class DeviceData {

    private String id;
    private String timeStamp;
    private float first;
    private float second;
    private float third;
    private String deviceKey;
    private String type;
    private String smsType;

    public DeviceData(String id, String timeStamp, float first, float second, float third, String deviceKey){
        this.id = id;
        this.timeStamp = timeStamp;
        this.first = first;
        this.second = second;
        this.third = third;
        this.deviceKey = deviceKey;
        this.type = "data";
        this.smsType = "";
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getTimeStamp() {
        return timeStamp;
    }

    public void setTimeStamp(String timeStamp) {
        this.timeStamp = timeStamp;
    }

    public float getFirst() {
        return first;
    }

    public void setFirst(float first) {
        this.first = first;
    }

    public float getSecond() {
        return second;
    }

    public void setSecond(float second) {
        this.second = second;
    }

    public float getThird() {
        return third;
    }

    public void setThird(float third) {
        this.third = third;
    }

    public String getDeviceKey() {
        return deviceKey;
    }

    public void setDeviceKey(String deviceKey) {
        this.deviceKey = deviceKey;
    }

    public void setSmsType(String smsType) {
        this.smsType = smsType;
    }
}
