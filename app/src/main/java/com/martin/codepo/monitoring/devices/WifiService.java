package com.martin.codepo.monitoring.devices;

import android.app.AlarmManager;
import android.app.IntentService;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.IBinder;
import android.os.SystemClock;
import android.support.annotation.Nullable;
import android.support.annotation.RequiresApi;

import com.martin.codepo.Types;
import com.martin.codepo.adapter.DevicesAdapter;
import com.martin.codepo.database.CheckTime;
import com.martin.codepo.database.CheckTimeDAO;
import com.martin.codepo.database.Device;
import com.martin.codepo.database.DeviceDAO;
import com.martin.codepo.database.DeviceData;
import com.martin.codepo.database.DeviceDataDAO;
import com.martin.codepo.monitoring.SharedMonitoringTools;

import java.util.ArrayList;
import java.util.UUID;

/**
 * Created by martin on 26/03/18.
 */

public class WifiService extends IntentService{

    private boolean running = true;

    /**
     * Creates an IntentService.  Invoked by your subclass's constructor.
     *
     */
    public WifiService() {
        super("WifiService");
    }

    @Override
    protected void onHandleIntent(@Nullable Intent intent) {
        DeviceDAO deviceDAO = new DeviceDAO(getApplicationContext());
        DeviceDataDAO deviceDataDAO = new DeviceDataDAO(getApplicationContext());
        ArrayList<Device> wifis;
        wifis = deviceDAO.selectAllDevicesType(Types.Wifi.getNum());
        for(Device wifi : wifis){
            int up = SharedMonitoringTools.isConnected(getApplicationContext()) ? 1 : 0;
            deviceDataDAO.addDeviceData(new DeviceData(String.valueOf(UUID.randomUUID()), SharedMonitoringTools.getCurrentTime(), up, up, up, wifi.getId()));
        }

    }

    @RequiresApi(api = Build.VERSION_CODES.M)
    @Override
    public void onDestroy() {
        super.onDestroy();
        CheckTimeDAO checkTimeDAO = new CheckTimeDAO(getApplicationContext());
        AlarmManager alarmManager = (AlarmManager) getApplicationContext().getSystemService(ALARM_SERVICE);
        Intent intent = new Intent(getApplicationContext(), WifiService.class);
        int control = checkTimeDAO.selectCheckTimeByDeviceType(Types.Wifi.getNum()).getControl();
        PendingIntent pendingIntent = PendingIntent.getService(getApplicationContext(), Types.Wifi.getNum(), intent, 0);
        alarmManager.setExactAndAllowWhileIdle(AlarmManager.ELAPSED_REALTIME_WAKEUP, SystemClock.elapsedRealtime() + control * SharedMonitoringTools.MINUTE, pendingIntent);

    }

}
