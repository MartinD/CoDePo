package com.martin.codepo.monitoring.devices;

import android.app.Service;
import android.content.Intent;
import android.os.IBinder;
import android.support.annotation.Nullable;

import com.martin.codepo.monitoring.SharedMonitoringTools;
import com.martin.codepo.monitoring.sms.SmsFormat;

/**
 * Created by user on 26/07/17.
 */

public class ApplicationService extends Service {

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        return START_NOT_STICKY;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }

    @Override
    public void onTaskRemoved(Intent rootIntent) {
        String offlineMessage = "L'application a été interrompue. Le monitoring n'est plus effectif !";
        SharedMonitoringTools.sendSmsToEveryOne(offlineMessage, SmsFormat.stopMonitoring(this), getApplicationContext());
        stopSelf();
    }

}
