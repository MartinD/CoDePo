package com.martin.codepo.monitoring.sms;

import android.app.AlarmManager;
import android.app.IntentService;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Build;
import android.os.IBinder;
import android.preference.PreferenceManager;
import android.support.annotation.Nullable;
import android.support.annotation.RequiresApi;
import android.util.Log;

import com.martin.codepo.Types;
import com.martin.codepo.activity.ModifyCheckTime;
import com.martin.codepo.database.Center;
import com.martin.codepo.database.CenterDAO;
import com.martin.codepo.database.Device;
import com.martin.codepo.database.DeviceDAO;
import com.martin.codepo.database.DeviceData;
import com.martin.codepo.database.DeviceDataDAO;
import com.martin.codepo.monitoring.SharedMonitoringTools;

import java.util.ArrayList;
import java.util.Calendar;

/**
 * Created by martin on 21/03/18.
 */

public class DayliSmsService extends IntentService{

    private boolean running = true;

    /**
     * Creates an IntentService.  Invoked by your subclass's constructor.
     *
     */
    public DayliSmsService() {
        super("DayliSms");
    }

    @RequiresApi(api = Build.VERSION_CODES.M)
    @Override
    public void onDestroy() {
        super.onDestroy();
        AlarmManager alarmManager = (AlarmManager) getApplication().getSystemService(ALARM_SERVICE);
        Calendar calendar = Calendar.getInstance();
        calendar.setTimeInMillis(System.currentTimeMillis());
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
        Integer hour = Integer.valueOf(sharedPreferences.getString(ModifyCheckTime.SMS_HOUR, "16"));
        calendar.set(Calendar.HOUR_OF_DAY, hour);
        Intent intent = new Intent(getApplicationContext(), DayliSmsService.class);
        PendingIntent pendingIntent = PendingIntent.getService(getApplicationContext(), 10, intent, 0);
        alarmManager.setExactAndAllowWhileIdle(AlarmManager.RTC_WAKEUP, calendar.getTimeInMillis(), pendingIntent);

    }

    @Override
    protected void onHandleIntent(@Nullable Intent intent) {
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(this);
        Integer ENDDAY = Integer.valueOf(sharedPreferences.getString(ModifyCheckTime.SMS_HOUR, "16"));
        Calendar calendar = Calendar.getInstance();
        int currentHour = calendar.get(Calendar.HOUR_OF_DAY);
        if(currentHour == ENDDAY){
            sendReport();
        }
    }

    @Override
    public void onTaskRemoved(Intent rootIntent) {
        this.running = false;
        stopSelf();
    }

    private void sendReport(){

        CenterDAO centerDAO = new CenterDAO(this);
        DeviceDAO deviceDAO = new DeviceDAO(this);
        DeviceDataDAO deviceDataDAO = new DeviceDataDAO(this);
        Center center = centerDAO.selectCenter();
        ArrayList<Device> devices = deviceDAO.selectAllDevices(true);

        StringBuilder offlineMessage = new StringBuilder();
        StringBuilder onlineMessage = new StringBuilder();
        offlineMessage.append("Rapport journalier du centre hospitalier ").append(center.getName()).append(" :\n\n");
        for(Device device : devices){
            DeviceData deviceData = deviceDataDAO.selectLastDeviceDataByDeviceKey(device.getId());
            if(deviceData != null){
                deviceData.setSmsType("report");
                onlineMessage.append(SmsFormat.DataFormat(deviceData));
                if(device.getDeviceType() == Types.Wifi.getNum()){
                    offlineMessage.append(deviceData.getFirst() == 1 ? ("Connecté au wifi\n\n") : ("Déconnecté du wifi\n\n"));
                } else if(device.getDeviceType() == Types.Ping.getNum()){
                    offlineMessage.append((deviceData.getFirst() == 1) ? (device.getIp() + " (" + device.getName() + ") est connecté au réseau\n\n") : (device.getIp() + " (" + device.getName() + ") est absent du réseau\n\n"));
                } else if(device.getDeviceType() == Types.SNMP.getNum()){
                    offlineMessage.append("Valeurs du serveur ").append(device.getIp()).append("(").append(device.getName()).append(") :\n CPU => ").append(deviceData.getFirst()).append("%\n RAM => ").append(deviceData.getSecond()).append("%\n DISK => ").append(deviceData.getThird()).append("%\n\n");
                } else if(device.getDeviceType() == Types.TCP.getNum()){
                    offlineMessage.append("Charge de ").append(device.getIp()).append(" (").append(device.getName()).append(") : ").append(deviceData.getFirst()).append("%\n\n");
                } else if(device.getDeviceType() == Types.Android.getNum()){
                    offlineMessage.append("Batterie du smartphone : ").append(deviceData.getFirst()).append("%\n\n");
                } else if(device.getDeviceType() == Types.Electricity.getNum()){
                    offlineMessage.append((deviceData.getFirst() == 1) ? ("Réseau électrique fonctionnel\n\n") : ("Réseau électrique down\n\n"));
                } else if(device.getDeviceType() == Types.Solar.getNum()){
                    offlineMessage.append("Voltage des panneaux solaires : ").append(deviceDataDAO.getAverageValue(device.getId())).append("%\n\n");
                }
            }
        }

        offlineMessage.append("Fin du rapport");

        SharedMonitoringTools.sendSmsToEveryOne(offlineMessage.toString(), onlineMessage.toString(), this);
    }


}
