package com.martin.codepo.monitoring.sms;

import android.app.AlarmManager;
import android.app.IntentService;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Intent;
import android.os.Build;
import android.os.IBinder;
import android.os.SystemClock;
import android.support.annotation.Nullable;
import android.support.annotation.RequiresApi;

import com.martin.codepo.Types;
import com.martin.codepo.database.Center;
import com.martin.codepo.database.CenterDAO;
import com.martin.codepo.database.CheckTime;
import com.martin.codepo.database.CheckTimeDAO;
import com.martin.codepo.database.Device;
import com.martin.codepo.database.DeviceDAO;
import com.martin.codepo.database.DeviceData;
import com.martin.codepo.database.DeviceDataDAO;
import com.martin.codepo.monitoring.SharedMonitoringTools;

import java.util.ArrayList;

/**
 * Created by martin on 27/03/18.
 */

public class AlertSmsService extends IntentService {

    private int deviceType;

    /**
     * Creates an IntentService.  Invoked by your subclass's constructor.
     *
     */
    public AlertSmsService() {
        super("AlertSmsService");
    }

    @RequiresApi(api = Build.VERSION_CODES.M)
    @Override
    public void onDestroy() {
        super.onDestroy();
        for(int i=0; i<7; i++){
            AlarmManager alarmManager = (AlarmManager) getApplicationContext().getSystemService(ALARM_SERVICE);
            Intent intent;
            int control;
            PendingIntent pendingIntent;
            CheckTimeDAO checkTimeDAO = new CheckTimeDAO(getApplicationContext());
            intent = new Intent(getApplicationContext(), AlertSmsService.class);
            control = checkTimeDAO.selectCheckTimeByDeviceType(i).getControl();
            pendingIntent = PendingIntent.getService(getApplicationContext(), i + 11, intent, 0);
            alarmManager.setExactAndAllowWhileIdle(AlarmManager.ELAPSED_REALTIME_WAKEUP, SystemClock.elapsedRealtime() + control * SharedMonitoringTools.MINUTE, pendingIntent);
        }
    }

    @Override
    protected void onHandleIntent(@Nullable Intent intent) {
        deviceType = intent.getIntExtra("DEVICE_TYPE", 0);
        CheckTimeDAO checkTimeDAO = new CheckTimeDAO(getApplicationContext());
        DeviceDAO deviceDAO = new DeviceDAO(getApplicationContext());
        ArrayList<Device> devices;
        //while (running){
        //System.out.println("Task running for Alert SMS monitoring");
        devices = deviceDAO.selectAllDevicesType(deviceType);
        //  try {
        for(Device device : devices){
            ArrayList<DeviceData> wrongDeviceData = selectAllWrongDeviceData(device);
            CheckTime checkTime = checkTimeDAO.selectCheckTimeByDeviceType(device.getDeviceType());
            if((wrongDeviceData.size() * checkTime.getControl()) % ( checkTime.getIntervalNotif() * 60 ) == checkTime.getControl() * checkTime.getFirstNotif()){
                StringBuilder[] messages = alertMessageByType(device, wrongDeviceData.get(wrongDeviceData.size() - 1));   // envoie la dernière donnée enregistrée
                SharedMonitoringTools.sendSms(messages[0].toString(), messages[1].toString(), device, getApplicationContext());
            }
        }
    }

    /**
     * Détermine le type de message d'erreur à envoyer en fonction
     * du type de device.
     * @param device
     * @param deviceData
     * @return
     */
    private StringBuilder[] alertMessageByType(Device device, DeviceData deviceData){
        CenterDAO centerDAO = new CenterDAO(getApplicationContext());
        Center center = centerDAO.selectCenter();
        StringBuilder offlineMessage =new StringBuilder();
        StringBuilder onlineMessage = new StringBuilder();
        offlineMessage.append("Alerte du centre hospitalier ").append(center.getName()).append(" :\n\n");
        deviceData.setSmsType("alert");
        onlineMessage.append(SmsFormat.DataFormat(deviceData));
        if(device.getDeviceType() == Types.Wifi.getNum()){
            offlineMessage.append("Déconnecté du wifi");
        } else if(deviceType == Types.Ping.getNum()){
            offlineMessage.append(device.getIp()).append(" (").append(device.getName()).append(") est absent du réseau\n\n");
        } else if(deviceType == Types.SNMP.getNum()){
            offlineMessage.append("Valeurs du serveur ").append(device.getIp()).append("(").append(device.getName()).append(") :\n CPU => ").append(deviceData.getFirst()).append("%\n RAM => ").append(deviceData.getSecond()).append("%\n DISK => ").append(deviceData.getThird()).append("%\n\n");
        } else if(deviceType == Types.TCP.getNum()){
            offlineMessage.append("Charge de ").append(device.getIp()).append(" (").append(device.getName()).append(") : ").append(deviceData.getFirst()).append("%\n\n");
        } else if(deviceType == Types.Android.getNum()){
            offlineMessage.append("Batterie du smartphone : ").append(deviceData.getFirst()).append("%\n\n");
        } else if(deviceType == Types.Electricity.getNum()){
            offlineMessage.append("Réseau électrique down\n\n");
        } else if(deviceType == Types.Solar.getNum()){
            offlineMessage.append("Voltage des panneaux solaires : ").append(deviceData.getFirst()).append("%\n\n");
        }
        return new StringBuilder[]{offlineMessage, onlineMessage};
    }

    /**
     * Renvoie la liste des valeurs en dessous des seuils prescrits
     * @param device
     * @return
     */
    private ArrayList<DeviceData> selectAllWrongDeviceData(Device device) {
        DeviceDataDAO deviceDataDAO = new DeviceDataDAO(getApplicationContext());
        ArrayList<DeviceData> deviceDatas = deviceDataDAO.selectAllDataDeviceKey(device.getId());
        ArrayList<DeviceData> wrongDeviceDatas = new ArrayList<>();
        int i = deviceDatas.size();
        boolean splitted = false;
        int type = device.getDeviceType();
        while(!splitted && i > 0){
            DeviceData deviceData = deviceDatas.get(i-1);
            if(type != Types.SNMP.getNum()){
                if((deviceData.getFirst() >= device.getThreshold().getFirst() && deviceData.getSecond() >= device.getThreshold().getSecond() && deviceData.getThird() >= device.getThreshold().getThird()) || i==1){
                    wrongDeviceDatas = new ArrayList<>(deviceDatas.subList(i, deviceDatas.size()));
                    splitted = true;
                }
            } else {
                if((100 - deviceData.getFirst() >= device.getThreshold().getFirst() && 100 - deviceData.getSecond() >= device.getThreshold().getSecond() && 100 - deviceData.getThird() >= device.getThreshold().getThird()) || i==1){
                    wrongDeviceDatas = new ArrayList<>(deviceDatas.subList(i, deviceDatas.size()));
                    splitted = true;
                }
            }
            i --;
        }
        return wrongDeviceDatas;
    }

}
