package com.martin.codepo.monitoring;

import android.content.Context;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.preference.PreferenceManager;
import android.telephony.SmsManager;

import com.martin.codepo.activity.CenterData;
import com.martin.codepo.database.CenterDAO;
import com.martin.codepo.database.Device;
import com.martin.codepo.database.DeviceUser;
import com.martin.codepo.database.User;
import com.martin.codepo.database.UserDAO;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Locale;
import java.util.Objects;

/**
 * Created by martin on 20/03/18.
 */

public class SharedMonitoringTools {

    public static int SECOND = 1000;
    public static int MINUTE = 60 * SECOND;
    public static int HOUR = 60 * MINUTE;

    public static boolean isConnected(Context context){
        Boolean isConnected;
        ConnectivityManager connectivityManager = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = connectivityManager.getActiveNetworkInfo();
        isConnected = networkInfo != null && networkInfo.isAvailable() && networkInfo.isConnected();
        return isConnected;
    }

    /**
     * Cette méthode permet l'envoi d'un device_ping vers l'adresse IP du device pris en paramètres
     * @param device
     * @return si le device_ping a découvert l'adresse IP ou non
     */
    public static boolean isPinged(Device device){
        Boolean isPinged = false;
        Runtime runtime = Runtime.getRuntime();
        try {
            Process mIpAddrProcess = runtime.exec("/system/bin/ping -c 1 -W 3 " + device.getIp()); // -W spécifie le temps en secondes à attendre au maximum. Par défaut, les différences entre smartphone peuvent mener à des temps trop longs et dons l'application freeze si elle est réalisée dans le threadUI.
            int mExitValue = mIpAddrProcess.waitFor();
            isPinged = mExitValue == 0;
        }
        catch (InterruptedException | IOException ignore) {
            ignore.printStackTrace();
        }
        return isPinged;
    }

    /**
     * Renvoie la date et l'heure actuelle. Le format est important pour l'utilisation avec SQLite
     * @return un objet Date au format voulu
     */
    public static String getCurrentTime() {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.getDefault());
        return sdf.format(new Date());
    }

    /**
     * Si le centre est "offline", envoie un sms à tous les utilisateurs liés au service
     * Si le centre est "online", envoie un sms au serveur SMS
     * @param offlineMessage
     * @param onlineMessage
     * @param device
     * @param context
     */
    public static void sendSms(String offlineMessage, String onlineMessage, Device device, Context context){
        CenterDAO centerDAO = new CenterDAO(context);
        UserDAO userDAO = new UserDAO(context);
        SmsManager smsManager = SmsManager.getDefault();
        if(!centerDAO.selectCenterIsOnline() && device!= null){
            ArrayList<DeviceUser> userDevices = device.getDeviceUsers();
            ArrayList<User> users = userDAO.selectAllUserByDevice(userDevices);
            for(User user: users){
                ArrayList<String> offlineMessageDivided = smsManager.divideMessage(offlineMessage);
                smsManager.sendMultipartTextMessage(user.getPhone(), null, offlineMessageDivided, null, null);
            }
        } else if(centerDAO.selectCenterIsOnline()) {
            sendSmsServer(onlineMessage, context);
        }
    }

    public static void sendSmsServer(String message, Context context){
        SmsManager smsManager = SmsManager.getDefault();
        CenterDAO centerDAO = new CenterDAO(context);
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);
        String phoneServer = sharedPreferences.getString(CenterData.PHONE_SERVER, "");
        ArrayList<String> onlineMessageDivided = smsManager.divideMessage(message);
        if (centerDAO.selectCenterIsOnline()) {
            System.out.println(onlineMessageDivided);
            smsManager.sendMultipartTextMessage(phoneServer, null, onlineMessageDivided, null, null);
        }
    }

    public static void sendSmsToEveryOne(String offlineMessage, String onlineMessage, Context context){
        CenterDAO centerDAO = new CenterDAO(context);
        UserDAO userDAO = new UserDAO(context);
        SmsManager smsManager = SmsManager.getDefault();
        if(!centerDAO.selectCenterIsOnline()){
            ArrayList<User> users = userDAO.selectAllUser();
            for(User user: users){
                ArrayList<String> offlineMessageDivided = smsManager.divideMessage(offlineMessage);
                smsManager.sendMultipartTextMessage(user.getPhone(), null, offlineMessageDivided, null, null);
            }
        } else {
            sendSmsServer(onlineMessage, context);
        }
    }

}
