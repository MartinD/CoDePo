package com.martin.codepo.monitoring.devices;

import android.app.AlarmManager;
import android.app.IntentService;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.IBinder;
import android.os.SystemClock;
import android.support.annotation.Nullable;

import com.martin.codepo.Types;
import com.martin.codepo.database.CheckTimeDAO;
import com.martin.codepo.database.Device;
import com.martin.codepo.database.DeviceDAO;
import com.martin.codepo.database.DeviceData;
import com.martin.codepo.database.DeviceDataDAO;
import com.martin.codepo.monitoring.SharedMonitoringTools;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.InetSocketAddress;
import java.net.Socket;
import java.util.ArrayList;
import java.util.UUID;

/**
 * Created by martin on 26/03/18.
 */

public class ElectricityService extends IntentService{

    private boolean running = true;

    /**
     * Creates an IntentService.  Invoked by your subclass's constructor.
     *
     */
    public ElectricityService() {
        super("ElectricityService");
    }

    @Override
    protected void onHandleIntent(@Nullable Intent intent) {
        CheckTimeDAO checkTimeDAO = new CheckTimeDAO(getApplicationContext());
        DeviceDAO deviceDAO = new DeviceDAO(this);
        ArrayList<Device> electricities;
        TcpConnection tcpConnection;
        // while (running){
        electricities = deviceDAO.selectAllDevicesType(Types.Electricity.getNum());
        //     try{
        for(Device electricity : electricities){
            tcpConnection = new TcpConnection(electricity.getIp(), 8888, electricity);
            tcpConnection.execute();
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        CheckTimeDAO checkTimeDAO = new CheckTimeDAO(getApplicationContext());
        AlarmManager alarmManager = (AlarmManager) getApplicationContext().getSystemService(ALARM_SERVICE);
        Intent intent = new Intent(getApplicationContext(), ElectricityService.class);
        int control = checkTimeDAO.selectCheckTimeByDeviceType(Types.Electricity.getNum()).getControl();
        PendingIntent pendingIntent = PendingIntent.getService(getApplicationContext(), Types.Electricity.getNum(), intent, 0);
        alarmManager.setExactAndAllowWhileIdle(AlarmManager.ELAPSED_REALTIME_WAKEUP, SystemClock.elapsedRealtime() + control * SharedMonitoringTools.MINUTE, pendingIntent);

    }


    private class TcpConnection extends AsyncTask<Void, String, String> {


        private String address;
        private int port;
        private DeviceDataDAO electricityDataDAO;
        private Device device;

        private Socket socket = null;
        private OutputStream outStream = null;
        private BufferedReader inStream = null;

        TcpConnection(String address, int port, Device device) {
            this.address = address;
            this.port = port;
            this.device = device;
            electricityDataDAO = new DeviceDataDAO(getApplicationContext());
        }

        @Override
        protected String doInBackground(Void... params) {
            String message = null;
            try{
                socket = new Socket();
                int timeout = 5000;
                socket.connect(new InetSocketAddress(address, port), timeout);
                outStream = socket.getOutputStream();
                sendMessage();
                inStream = new BufferedReader(new InputStreamReader(socket.getInputStream()));
                message = inStream.readLine();
            }
            catch (Exception e){
                e.printStackTrace();
            }

            return message;
        }

        @Override
        protected void onPostExecute(String message) {
            super.onPostExecute(message);
            if(message != null) {
                float voltage = Float.valueOf(message) / 15 * 100;
                int up = (voltage > 10) ? 1 : 0;
                //int previousUp = electricityDataDAO.getLastUp(device_electricity.getId());
                //int step = (previousUp == up) ? 0 : 1;
                electricityDataDAO.addDeviceData(new DeviceData(String.valueOf(UUID.randomUUID()), SharedMonitoringTools.getCurrentTime(), up, up, up, device.getId()));
            }
            try {
                socket.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        private void sendMessage() {
            try {
                outStream.write(3);
                outStream.write('\n');
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }
}
