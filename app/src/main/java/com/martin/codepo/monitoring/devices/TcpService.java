package com.martin.codepo.monitoring.devices;

import android.app.AlarmManager;
import android.app.IntentService;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Build;
import android.os.IBinder;
import android.os.SystemClock;
import android.support.annotation.Nullable;
import android.support.annotation.RequiresApi;
import android.util.Log;

import com.martin.codepo.Types;
import com.martin.codepo.database.CheckTimeDAO;
import com.martin.codepo.database.Device;
import com.martin.codepo.database.DeviceDAO;
import com.martin.codepo.database.DeviceData;
import com.martin.codepo.database.DeviceDataDAO;
import com.martin.codepo.monitoring.SharedMonitoringTools;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.InetSocketAddress;
import java.net.Socket;
import java.util.ArrayList;
import java.util.UUID;

/**
 * Created by martin on 26/03/18.
 */

public class TcpService extends IntentService  {

    /**
     * Creates an IntentService.  Invoked by your subclass's constructor.
     *
     *
     */
    public TcpService() {
        super("TcpService");
    }

    @Override
    protected void onHandleIntent(@Nullable Intent intent) {
        DeviceDAO deviceDAO = new DeviceDAO(getApplicationContext());
        ArrayList<Device> tcps;
        tcps = deviceDAO.selectAllDevicesType(Types.TCP.getNum());
        for (Device tcp : tcps){
            new TcpConnection(tcp.getIp(), 8888, tcp).execute();
        }
    }

    @RequiresApi(api = Build.VERSION_CODES.M)
    @Override
    public void onDestroy() {
        super.onDestroy();
        CheckTimeDAO checkTimeDAO = new CheckTimeDAO(getApplicationContext());
        AlarmManager alarmManager = (AlarmManager) getApplicationContext().getSystemService(ALARM_SERVICE);
        Intent intent = new Intent(getApplicationContext(), TcpService.class);
        int control = checkTimeDAO.selectCheckTimeByDeviceType(Types.TCP.getNum()).getControl();
        PendingIntent pendingIntent = PendingIntent.getService(getApplicationContext(), Types.TCP.getNum(), intent, 0);
        alarmManager.setExactAndAllowWhileIdle(AlarmManager.ELAPSED_REALTIME_WAKEUP, SystemClock.elapsedRealtime() + control * SharedMonitoringTools.MINUTE, pendingIntent);

    }

    private class TcpConnection extends AsyncTask<Void, String, String> {


        private String address;
        private int port;
        private DeviceDataDAO tcpDataDAO;
        private Socket socket = null;
        private OutputStream outStream = null;
        private BufferedReader inStream = null;
        private Device device;

        public TcpConnection(String address, int port, Device device) {
            this.address = address;
            this.device = device;
            this.port = port;
            tcpDataDAO = new DeviceDataDAO(getApplicationContext());
        }

        @Override
        protected String doInBackground(Void... params) {
            String message = null;
            Log.d("Codepo", "Task in execution for " + device.getName());
            try{
                socket = new Socket();
                int timeout = 5000;
                socket.connect(new InetSocketAddress(address, port), timeout);
                outStream = socket.getOutputStream();
                sendMessage();
                inStream = new BufferedReader(new InputStreamReader(socket.getInputStream()));
                message = inStream.readLine();
            }
            catch (Exception e){
                e.printStackTrace();
            }

            return message;
        }

        @Override
        protected void onPostExecute(String message) {
            super.onPostExecute(message);
            if(message != null) {
                float voltage = Float.valueOf(message) / 15 * 100;
                //float previousVoltage = tcpDataDAO.getLastVoltage(device_tcp.getId());
                //int step = ((previousVoltage < limit && voltage > limit) || (previousVoltage > limit && voltage < limit)) ? 1 : 0;
                tcpDataDAO.addDeviceData(new DeviceData(String.valueOf(UUID.randomUUID()), SharedMonitoringTools.getCurrentTime(), voltage, voltage, voltage, device.getId()));
            }
            try {
                socket.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        private void sendMessage() {
            try {
                outStream.write(2);
                outStream.write('\n');
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }
}
