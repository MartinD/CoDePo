package com.martin.codepo.monitoring.sms;

import android.content.Context;
import android.support.annotation.NonNull;

import com.google.gson.Gson;
import com.martin.codepo.database.Center;
import com.martin.codepo.database.CenterDAO;
import com.martin.codepo.database.CheckTime;
import com.martin.codepo.database.CheckTimeDAO;
import com.martin.codepo.database.Device;
import com.martin.codepo.database.DeviceConfig;
import com.martin.codepo.database.DeviceConfigDAO;
import com.martin.codepo.database.DeviceDAO;
import com.martin.codepo.database.DeviceData;
import com.martin.codepo.database.Threshold;
import com.martin.codepo.database.ThresholdDAO;

import java.util.ArrayList;

/**
 * Created by martin on 4/03/18.
 */

public class SmsFormat {

    @NonNull
    public static String DataFormat(DeviceData deviceData){
        Gson gson = new Gson();
        return "create&&" + gson.toJson(deviceData);
    }

    public static String DatabaseFormat(Context context, String action){
        CenterDAO centerDAO = new CenterDAO(context);
        Center center = centerDAO.selectCenter();
        DeviceDAO deviceDAO = new DeviceDAO(context);
        ArrayList<Device> devices = deviceDAO.selectAllDevices(false);
        //UserDAO userDAO = new UserDAO(context);                       // Ne pas envoyer les utilisateurs car il faudrait les synchroniser avec toutes les applications de monitoring => Pour plus tard
        //DeviceDataDAO deviceDataDAO = new DeviceDataDAO(context);     // Ne pas envoyer les données précédentes. Seules les données d'alerte et de rapport sont envoyées => Trop de SMS
        //ArrayList<DeviceData> deviceData = deviceDataDAO.selectAllDeviceData();
        ThresholdDAO thresholdDAO = new ThresholdDAO(context);
        ArrayList<Threshold> thresholds = thresholdDAO.selectAllThreshold();
        DeviceConfigDAO deviceConfigDAO = new DeviceConfigDAO(context);
        ArrayList<DeviceConfig> deviceConfigs = deviceConfigDAO.selectAllDeviceConfig();
        CheckTimeDAO checkTimeDAO = new CheckTimeDAO(context);
        ArrayList<CheckTime> checkTimes = checkTimeDAO.selectAllCheckTime();
        Gson gson = new Gson();
        StringBuilder msg = new StringBuilder();
        msg.append(action).append("&&");
        msg.append(gson.toJson(center));
        for(Device device : devices){
            msg.append("&&").append(gson.toJson(device));
        }
        for(Threshold threshold : thresholds){
            msg.append("&&").append(gson.toJson(threshold));
        }
        for (DeviceConfig deviceConfig : deviceConfigs){
            msg.append("&&").append(gson.toJson(deviceConfig));
        }
        for (CheckTime checkTime : checkTimes){
            msg.append("&&").append(gson.toJson(checkTime));
        }
        //msg.delete(msg.length()-2, msg.length());
        return msg.toString();

    }

    public static String updateCheckTimeFormat(Context context){
        CheckTimeDAO checkTimeDAO = new CheckTimeDAO(context);
        ArrayList<CheckTime> checkTimes = checkTimeDAO.selectAllCheckTime();
        Gson gson = new Gson();
        StringBuilder msg = new StringBuilder();
        msg.append("update");
        for(CheckTime checkTime : checkTimes){
            msg.append("&&").append(gson.toJson(checkTime));
        }
        return msg.toString();
    }

    public static String DeviceFormat(Context context, String deviceKey, String action){
        Gson gson = new Gson();
        DeviceDAO deviceDAO = new DeviceDAO(context);
        Device device = deviceDAO.selectDevice(deviceKey, false);
        DeviceConfigDAO deviceConfigDAO = new DeviceConfigDAO(context);
        DeviceConfig deviceConfig = deviceConfigDAO.selectConfigDeviceKey(device.getId());
        ThresholdDAO thresholdDAO = new ThresholdDAO(context);
        Threshold threshold = thresholdDAO.selectThresholdDeviceKey(device.getId());
        String msg = action + "&&";
        msg += gson.toJson(device) + "&&";
        msg += gson.toJson(deviceConfig) + "&&";
        msg += gson.toJson(threshold);
        return msg;
    }

    public static String deleteDeviceFormat(String deviceKey){
        return "delete&&{'id':'" + deviceKey + "'}";
    }

    public static String stopMonitoring(Context context) {
        Gson gson = new Gson();
        CenterDAO centerDAO = new CenterDAO(context);
        Center center = centerDAO.selectCenter();
        if(center != null){
            center.setIsOnline(2);
            String msg = "update&&";
            msg += gson.toJson(center);
            return msg;
        } else {
            return "";
        }
    }
}
