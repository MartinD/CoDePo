package com.martin.codepo.monitoring.devices;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Build;
import android.os.IBinder;
import android.os.SystemClock;
import android.support.annotation.Nullable;
import android.support.annotation.RequiresApi;

import com.martin.codepo.Types;
import com.martin.codepo.database.CheckTimeDAO;
import com.martin.codepo.database.Device;
import com.martin.codepo.database.DeviceDAO;
import com.martin.codepo.database.DeviceData;
import com.martin.codepo.database.DeviceDataDAO;
import com.martin.codepo.monitoring.SharedMonitoringTools;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.InetSocketAddress;
import java.net.Socket;
import java.util.ArrayList;
import java.util.UUID;

/**
 * Created by martin on 26/03/18.
 */

public class SolarService extends Service implements Runnable {

    private boolean running = true;

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        Thread thread = new Thread(this);
        thread.start();
        return START_NOT_STICKY;
    }

    @RequiresApi(api = Build.VERSION_CODES.M)
    @Override
    public void onDestroy() {
        super.onDestroy();
        CheckTimeDAO checkTimeDAO = new CheckTimeDAO(getApplicationContext());
        AlarmManager alarmManager = (AlarmManager) getApplicationContext().getSystemService(ALARM_SERVICE);
        Intent intent = new Intent(getApplicationContext(), SolarService.class);
        int control = checkTimeDAO.selectCheckTimeByDeviceType(Types.Solar.getNum()).getControl();
        PendingIntent pendingIntent = PendingIntent.getService(getApplicationContext(), Types.Solar.getNum(), intent, 0);
        alarmManager.setExactAndAllowWhileIdle(AlarmManager.ELAPSED_REALTIME_WAKEUP, SystemClock.elapsedRealtime() + control * SharedMonitoringTools.MINUTE, pendingIntent);

    }

    @Override
    public void run() {
        CheckTimeDAO checkTimeDAO = new CheckTimeDAO(getApplicationContext());
        DeviceDAO deviceDAO = new DeviceDAO(getApplicationContext());
        ArrayList<Device> solars;
        //while (running){
            solars = deviceDAO.selectAllDevicesType(Types.Solar.getNum());
          //  try {
                for(Device solar : solars){
                    new TcpConnection(solar.getIp(), 8888, solar).execute();
                }
            //    Thread.sleep(checkTimeDAO.selectCheckTimeByDeviceType(Types.Solar.getNum()).getControl() * SharedMonitoringTools.MINUTE);
            //} catch (InterruptedException e) {
            //    e.printStackTrace();
            //}
        //}
        stopSelf();
    }

    private class TcpConnection extends AsyncTask<Void, String, String> {


        private String address;
        private int port;
        private DeviceDataDAO solarDataDAO;
        private float voltage = 15;
        private Device device;

        private Socket socket = null;
        private OutputStream outStream = null;
        private BufferedReader inStream = null;

        TcpConnection(String address, int port, Device device) {
            this.device = device;
            this.address = address;
            this.port = port;
            solarDataDAO = new DeviceDataDAO(getApplicationContext());
        }

        @Override
        protected String doInBackground(Void... params) {
            String message = null;
            try{
                socket = new Socket();
                int timeout = 5000;
                socket.connect(new InetSocketAddress(address, port), timeout);
                outStream = socket.getOutputStream();
                sendMessage();
                inStream = new BufferedReader(new InputStreamReader(socket.getInputStream()));
                message = inStream.readLine();
            }
            catch (Exception e){
                e.printStackTrace();
            }

            return message;
        }

        @Override
        protected void onPostExecute(String message) {
            super.onPostExecute(message);
            if(message != null) {
                voltage = Float.valueOf(message) / 15 * 100;
                //float previousVoltage = solarDataDAO.getLastVoltage(device_solar.getId());
                //int step = ((previousVoltage < limit && voltage > limit) || (previousVoltage > limit && voltage < limit)) ? 1 : 0;
                solarDataDAO.addDeviceData(new DeviceData(String.valueOf(UUID.randomUUID()), SharedMonitoringTools.getCurrentTime(), voltage, voltage, voltage, device.getId()));
            }
            try {
                socket.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        private void sendMessage() {
            try {
                outStream.write(4);
                outStream.write('\n');
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }
}
