package com.martin.codepo.monitoring.sms;

import android.app.AlarmManager;
import android.app.IntentService;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Intent;
import android.os.IBinder;
import android.os.SystemClock;
import android.support.annotation.Nullable;

import com.martin.codepo.Types;
import com.martin.codepo.database.Center;
import com.martin.codepo.database.CenterDAO;
import com.martin.codepo.database.CheckTimeDAO;
import com.martin.codepo.database.Device;
import com.martin.codepo.database.DeviceDAO;
import com.martin.codepo.database.DeviceData;
import com.martin.codepo.database.DeviceDataDAO;
import com.martin.codepo.database.Threshold;
import com.martin.codepo.monitoring.SharedMonitoringTools;

import java.util.ArrayList;

/**
 * Created by martin on 27/03/18.
 */

public class NoAlertSmsService extends IntentService {

    private int deviceType;

    /**
     * Creates an IntentService.  Invoked by your subclass's constructor.
     *
     */
    public NoAlertSmsService() {
        super("NoAlertSms");
    }

    @Override
    protected void onHandleIntent(@Nullable Intent intent) {
        deviceType = intent.getIntExtra("DEVICE_TYPE", 0);
        CheckTimeDAO checkTimeDAO = new CheckTimeDAO(getApplicationContext());
        DeviceDAO deviceDAO = new DeviceDAO(getApplicationContext());
        DeviceDataDAO deviceDataDAO = new DeviceDataDAO(getApplicationContext());
        ArrayList<Device> devices;
        //while (running){
        //System.out.println("Task running for no alert SMS monitoring");
        devices = deviceDAO.selectAllDevicesType(deviceType);
        //  try {
        for(Device device : devices){
            ArrayList<DeviceData> deviceData = deviceDataDAO.selectFrameDeviceDataByDeviceKey(device.getId(), 2);
            if(deviceData.size() > 1){
                DeviceData firstData = deviceData.get(0);
                DeviceData secondData = deviceData.get(1);
                Threshold threshold = device.getThreshold();
                if((firstData.getFirst() >= threshold.getFirst() && secondData.getFirst() < threshold.getFirst())
                        && (firstData.getSecond() >= threshold.getSecond() && secondData.getSecond() < threshold.getSecond())
                        && (firstData.getThird() >= threshold.getThird() && secondData.getThird() < threshold.getThird())) {
                    StringBuilder[] messages = noAlertMessageByType(device, firstData);   // envoie la dernière donnée enregistrée
                    SharedMonitoringTools.sendSms(messages[0].toString(), messages[1].toString(), device, getApplicationContext());
                }
            }
        }

    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        for(int i=0; i<7; i++){
            AlarmManager alarmManager = (AlarmManager) getApplicationContext().getSystemService(ALARM_SERVICE);
            Intent intent;
            int control;
            PendingIntent pendingIntent;
            CheckTimeDAO checkTimeDAO = new CheckTimeDAO(getApplicationContext());
            intent = new Intent(getApplicationContext(), NoAlertSmsService.class);
            control = checkTimeDAO.selectCheckTimeByDeviceType(i).getControl();
            pendingIntent = PendingIntent.getService(getApplicationContext(), i + 21, intent, 0);
            alarmManager.setExactAndAllowWhileIdle(AlarmManager.ELAPSED_REALTIME_WAKEUP, SystemClock.elapsedRealtime() + control * SharedMonitoringTools.MINUTE, pendingIntent);
        }
    }

    private StringBuilder[] noAlertMessageByType(Device device, DeviceData deviceData){
        CenterDAO centerDAO = new CenterDAO(getApplicationContext());
        Center center = centerDAO.selectCenter();
        StringBuilder offlineMessage =new StringBuilder();
        StringBuilder onlineMessage = new StringBuilder();
        offlineMessage.append("Retour à la normale pour le centre hospitalier ").append(center.getName()).append(" :\n\n");
        deviceData.setSmsType("noAlert");
        onlineMessage.append(SmsFormat.DataFormat(deviceData));
        if(deviceType == Types.Wifi.getNum()){
            offlineMessage.append("Connecté au wifi");
        } else if(deviceType == Types.Ping.getNum()){
            offlineMessage.append(device.getIp()).append(" (").append(device.getName()).append(") est connecté au réseau\n\n");
        } else if(deviceType == Types.SNMP.getNum()){
            offlineMessage.append("Valeurs du serveur ").append(device.getIp()).append("(").append(device.getName()).append(") :\n CPU => ").append(deviceData.getFirst()).append("%\n RAM => ").append(deviceData.getSecond()).append("%\n DISK => ").append(deviceData.getThird()).append("%\n\n");
        } else if(deviceType == Types.TCP.getNum()){
            offlineMessage.append("Charge de ").append(device.getIp()).append(" (").append(device.getName()).append(") : ").append(deviceData.getFirst()).append("%\n\n");
        } else if(deviceType == Types.Android.getNum()){
            offlineMessage.append("Batterie du smartphone : ").append(deviceData.getFirst()).append("%\n\n");
        } else if(deviceType == Types.Electricity.getNum()){
            offlineMessage.append("Réseau électrique opérationnel\n\n");
        } else if(deviceType == Types.Solar.getNum()){
            offlineMessage.append("Voltage des panneaux solaires : ").append(deviceData.getFirst()).append("%\n\n");
        }
        return new StringBuilder[]{offlineMessage, onlineMessage};
    }
}
