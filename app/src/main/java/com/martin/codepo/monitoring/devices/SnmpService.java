package com.martin.codepo.monitoring.devices;

import android.app.AlarmManager;
import android.app.IntentService;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Build;
import android.os.IBinder;
import android.os.SystemClock;
import android.support.annotation.Nullable;
import android.support.annotation.RequiresApi;

import com.martin.codepo.Types;
import com.martin.codepo.database.CheckTimeDAO;
import com.martin.codepo.database.Device;
import com.martin.codepo.database.DeviceDAO;
import com.martin.codepo.database.DeviceData;
import com.martin.codepo.database.DeviceDataDAO;
import com.martin.codepo.monitoring.SharedMonitoringTools;

import org.snmp4j.CommunityTarget;
import org.snmp4j.PDU;
import org.snmp4j.Target;
import org.snmp4j.TransportMapping;
import org.snmp4j.event.ResponseEvent;
import org.snmp4j.mp.SnmpConstants;
import org.snmp4j.smi.Address;
import org.snmp4j.smi.GenericAddress;
import org.snmp4j.smi.OID;
import org.snmp4j.smi.OctetString;
import org.snmp4j.smi.VariableBinding;
import org.snmp4j.transport.DefaultUdpTransportMapping;

import java.io.IOException;
import java.util.ArrayList;
import java.util.UUID;

/**
 * Created by martin on 26/03/18.
 */

public class SnmpService extends IntentService{
    
    private ArrayList<OID> oids = new ArrayList<>();

    /**
     * Creates an IntentService.  Invoked by your subclass's constructor.
     *
     */
    public SnmpService() {
        super("SnmpService");
    }

    @Override
    protected void onHandleIntent(@Nullable Intent intent) {
        oids.add(new OID(".1.3.6.1.4.1.2021.11.9.0"));
        oids.add(new OID(".1.3.6.1.4.1.2021.11.10.0"));
        oids.add(new OID(".1.3.6.1.4.1.2021.4.5.0"));
        oids.add(new OID(".1.3.6.1.4.1.2021.4.6.0"));
        oids.add(new OID(".1.3.6.1.4.1.2021.9.1.9.1"));
        CheckTimeDAO checkTimeDAO = new CheckTimeDAO(getApplicationContext());
        DeviceDAO deviceDAO = new DeviceDAO(getApplicationContext());
        ArrayList<Device> snmps;
        // while (running){
        snmps = deviceDAO.selectAllDevicesType(Types.SNMP.getNum());
        //     try {
        for(Device snmp : snmps){
            if(SharedMonitoringTools.isConnected(getApplicationContext())){
                try {
                    new SnmpClient("udp:" + snmp.getIp() + "/" + snmp.getDeviceConfig().getSecond(), oids, snmp.getId(), snmp).execute();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    @RequiresApi(api = Build.VERSION_CODES.M)
    @Override
    public void onDestroy() {
        super.onDestroy();
        CheckTimeDAO checkTimeDAO = new CheckTimeDAO(getApplicationContext());
        AlarmManager alarmManager = (AlarmManager) getApplicationContext().getSystemService(ALARM_SERVICE);
        Intent intent = new Intent(getApplicationContext(), SnmpService.class);
        int control = checkTimeDAO.selectCheckTimeByDeviceType(Types.SNMP.getNum()).getControl();
        PendingIntent pendingIntent = PendingIntent.getService(getApplicationContext(), Types.SNMP.getNum(), intent, 0);
        alarmManager.setExactAndAllowWhileIdle(AlarmManager.ELAPSED_REALTIME_WAKEUP, SystemClock.elapsedRealtime() + control * SharedMonitoringTools.MINUTE, pendingIntent);

    }

    private class SnmpClient extends AsyncTask<String, String, ArrayList<String>> {

        private org.snmp4j.Snmp snmp4J;
        private String id;
        private ArrayList<OID> oids = new ArrayList<>();
        private String address;
        private DeviceDataDAO snmpDataDAO;
        private Device device;

        public SnmpClient(String address, ArrayList<OID> oids, String id, Device device) throws IOException {
            super();
            this.address = address;
            this.oids = oids;
            this.id = id;
            this.device = device;
            snmpDataDAO = new DeviceDataDAO(getApplicationContext());
        }

        @Override
        protected ArrayList<String> doInBackground(String... strings) {
            start();
            try {
                return getAsString(oids);
            } catch (IOException e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(ArrayList<String> responses) {
            if(responses != null){
                if(responses.size() > 0) {
                    float cpu = Float.valueOf(responses.get(0)) + Float.valueOf(responses.get(1));
                    float ram = Float.valueOf(responses.get(3)) / Float.valueOf(responses.get(2));
                    float disk = Float.valueOf(responses.get(4));
                /*DeviceData previousSnmpData = snmpDataDAO.getLastSnmpData(snmp.getId());
                int step = 1;
                if(previousSnmpData != null) {
                    step = (((previousSnmpData.getFirst() < limitCpu && cpu > limitCpu) || (previousSnmpData.getFirst() > limitCpu && cpu < limitCpu)) ||
                            ((previousSnmpData.getSecond() < limitRam && ram > limitRam) || (previousSnmpData.getSecond() > limitRam && ram < limitRam)) ||
                            ((previousSnmpData.getThird() < limitDisk && disk > limitDisk) || (previousSnmpData.getThird() > limitDisk && disk < limitDisk))) ? 1 : 0;
                }*/
                    snmpDataDAO.addDeviceData(new DeviceData(String.valueOf(UUID.randomUUID()), SharedMonitoringTools.getCurrentTime(), cpu, ram, disk, id));
                }
            }
        }

        @Override
        protected void onProgressUpdate(String... values) {

        }


        public void stop() throws IOException {
            snmp4J.close();
        }

        private void start()  {
            try {
                TransportMapping transport = new DefaultUdpTransportMapping();
                snmp4J = new org.snmp4j.Snmp(transport);
                transport.listen();
            } catch (Exception e){
                e.printStackTrace();
            }
        }

        public ArrayList<String> getAsString(ArrayList<OID> oids) throws IOException {
            ArrayList<String> responses = new ArrayList<>();
            for(OID oid : oids){
                ResponseEvent event = get(new OID[]{oid});
                if(event != null) {
                    if (event.getResponse() != null) {
                        if (event.getResponse().get(0) != null) {
                            responses.add(event.getResponse().get(0).getVariable().toString());
                        }
                    }
                }
            }
            return responses;
        }


    /*public void getAsString(OID oids, ResponseListener listener) {
        try {
            snmp4J.send(getPDU(new OID[]{oids}), getTarget(),null, listener);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }*/


        private PDU getPDU(OID oids[]) {
            PDU pdu = new PDU();
            for (OID oid : oids) {
                pdu.add(new VariableBinding(oid));
            }
            pdu.setType(PDU.GET);
            return pdu;
        }

        public ResponseEvent get(OID oids[]) throws IOException {
            ResponseEvent event = snmp4J.send(getPDU(oids), getTarget(), null);
            if(event != null) {
                return event;
            }
            throw new RuntimeException("GET timed out");
        }

        private Target getTarget() {
            Address targetAddress = GenericAddress.parse(address);
            CommunityTarget target = new CommunityTarget();
            target.setCommunity(new OctetString(device.getDeviceConfig().getFirst()));
            target.setAddress(targetAddress);
            target.setRetries(2);
            target.setTimeout(1500);
            target.setVersion(SnmpConstants.version2c);
            return target;
        }

        /**
         * Normally this would return domain objects or something else than this...
         */
    /*public List<List<String>> getTableAsStrings(OID[] oids) {
        TableUtils tUtils = new TableUtils(snmp4J, new DefaultPDUFactory());

        @SuppressWarnings("unchecked")
        List<TableEvent> events = tUtils.getTable(getTarget(), oids, null, null);

        List<List<String>> list = new ArrayList<List<String>>();
        for (TableEvent event : events) {
            if(event.isError()) {
                throw new RuntimeException(event.getErrorMessage());
            }
            List<String> strList = new ArrayList<String>();
            list.add(strList);
            for(VariableBinding vb: event.getColumns()) {
                strList.add(vb.getVariable().toString());
            }
        }
        return list;
    }*/


    }
}
