package com.martin.codepo.monitoring.devices;

import android.annotation.SuppressLint;
import android.app.AlarmManager;
import android.app.IntentService;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.AsyncTask;
import android.os.BatteryManager;
import android.os.Build;
import android.os.IBinder;
import android.os.SystemClock;
import android.support.annotation.Nullable;
import android.support.annotation.RequiresApi;

import com.martin.codepo.Types;
import com.martin.codepo.database.CheckTimeDAO;
import com.martin.codepo.database.Device;
import com.martin.codepo.database.DeviceDAO;
import com.martin.codepo.database.DeviceData;
import com.martin.codepo.database.DeviceDataDAO;
import com.martin.codepo.monitoring.SharedMonitoringTools;

import java.io.IOException;
import java.io.OutputStream;
import java.net.InetSocketAddress;
import java.net.Socket;
import java.util.ArrayList;
import java.util.UUID;

/**
 * Created by martin on 26/03/18.
 */

public class AndroidService extends IntentService{

    private boolean running = true;

    /**
     * Creates an IntentService.  Invoked by your subclass's constructor.
     *
     */
    public AndroidService() {
        super("AndroidService");
    }

    @Override
    protected void onHandleIntent(@Nullable Intent intent) {
        CheckTimeDAO checkTimeDAO = new CheckTimeDAO(this);
        DeviceDAO deviceDAO = new DeviceDAO(this);
        ArrayList<Device> androids;
        //while (running){
        androids = deviceDAO.selectAllDevicesType(Types.Android.getNum());
        //try {
        for(Device android : androids){
            new TcpConnection(android.getIp(), 8888, android).execute();
        }
    }

    @RequiresApi(api = Build.VERSION_CODES.M)
    @Override
    public void onDestroy() {
        super.onDestroy();
        CheckTimeDAO checkTimeDAO = new CheckTimeDAO(this);
        AlarmManager alarmManager = (AlarmManager) getApplicationContext().getSystemService(ALARM_SERVICE);
        Intent intent = new Intent(getApplicationContext(), AndroidService.class);
        int control = checkTimeDAO.selectCheckTimeByDeviceType(Types.Android.getNum()).getControl();
        PendingIntent pendingIntent = PendingIntent.getService(getApplicationContext(), Types.Android.getNum(), intent, 0);
        alarmManager.setExactAndAllowWhileIdle(AlarmManager.ELAPSED_REALTIME_WAKEUP, SystemClock.elapsedRealtime() + control * SharedMonitoringTools.MINUTE, pendingIntent);
    }

    @SuppressLint("StaticFieldLeak")
    private class TcpConnection extends AsyncTask<Void, String, Void> {

        private String address;
        private int port;
        private Socket socket = null;
        private OutputStream outStream = null;
        private Device device;

        public TcpConnection(String address, int port, Device device) {
            this.address = address;
            this.device = device;
            this.port = port;
        }

        @Override
        protected Void doInBackground(Void... params) {
            DeviceDataDAO deviceDataDAO = new DeviceDataDAO(getApplicationContext());
            IntentFilter intentFilter = new IntentFilter(Intent.ACTION_BATTERY_CHANGED);
            Intent batteryStatus = getApplicationContext().registerReceiver(null, intentFilter);
            float level = batteryStatus.getIntExtra(BatteryManager.EXTRA_LEVEL, -1);
            float scale = batteryStatus.getIntExtra(BatteryManager.EXTRA_SCALE, -1);
            float levelBat = (level/scale) * 100;
            deviceDataDAO.addDeviceData(new DeviceData(String.valueOf(UUID.randomUUID()), SharedMonitoringTools.getCurrentTime(), levelBat, levelBat, levelBat, device.getId()));
            int status = batteryStatus.getIntExtra(BatteryManager.EXTRA_STATUS, -1);
            boolean isCharging = status == BatteryManager.BATTERY_STATUS_CHARGING || status == BatteryManager.BATTERY_STATUS_FULL;

            if(levelBat < 15 && !isCharging) {
                try{
                    socket = new Socket();
                    int timeout = 5000;
                    socket.connect(new InetSocketAddress(address, port), timeout);
                    outStream = socket.getOutputStream();
                    sendMessage(1);
                } catch (IOException e) {
                    e.printStackTrace();
                }
            } else if (levelBat > 90 && isCharging){
                try{
                    socket = new Socket();
                    int timeout = 5000;
                    socket.connect(new InetSocketAddress(address, port), timeout);
                    outStream = socket.getOutputStream();
                    sendMessage(0);
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }

            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            try {
                if(socket != null)
                    socket.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
            stopSelf();
        }

        /**
         * Write a message to the connection. Runs in UI thread.
         */
        private void sendMessage(int data) {
            try {
                outStream.write(data);
                outStream.write('\n');
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

    }
}
