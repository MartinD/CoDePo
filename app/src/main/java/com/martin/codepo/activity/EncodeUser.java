package com.martin.codepo.activity;

import android.view.MenuItem;

import com.martin.codepo.R;
import com.martin.codepo.database.Center;
import com.martin.codepo.database.CenterDAO;
import com.martin.codepo.database.Device;
import com.martin.codepo.database.DeviceUser;
import com.martin.codepo.database.User;
import com.martin.codepo.database.UserDAO;

import java.util.ArrayList;
import java.util.UUID;

/**
 * Created by martin on 13/03/18.
 */

public class EncodeUser extends UserData {

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()){
            case R.id.save:
                UserDAO userDAO = new UserDAO(this);
                userDAO.addUser(addUser());
                finish();
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    public User addUser(){
        CenterDAO centerDAO = new CenterDAO(this);
        Center center = centerDAO.selectCenter();
        User user = new User(String.valueOf(UUID.randomUUID()), name.getText().toString(), firstname.getText().toString(), email.getText().toString(),
                password.getText().toString(), (String) country.getSelectedItem(), (String) prefix.getSelectedItem(), phone.getText().toString(),
                center.getId());
        ArrayList<DeviceUser> devices = new ArrayList<>();
        deviceChoosen = chosableDevicesAdapter.getDevicesChosen();
        for(Device device : deviceChoosen){
            DeviceUser deviceUser = new DeviceUser(String.valueOf(UUID.randomUUID()), device.getId(), user.getId());
            devices.add(deviceUser);
        }
        user.setDeviceUsers(devices);
        return user;
    }
}
