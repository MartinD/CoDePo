package com.martin.codepo.activity;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.TextView;

import com.androidplot.xy.BoundaryMode;
import com.androidplot.xy.LineAndPointFormatter;
import com.androidplot.xy.PanZoom;
import com.androidplot.xy.SimpleXYSeries;
import com.androidplot.xy.XYGraphWidget;
import com.androidplot.xy.XYPlot;
import com.androidplot.xy.XYSeries;
import com.martin.codepo.GraphXLabelFormat;
import com.martin.codepo.R;
import com.martin.codepo.Types;
import com.martin.codepo.adapter.DevicesAdapter;
import com.martin.codepo.database.Device;
import com.martin.codepo.database.DeviceData;
import com.martin.codepo.database.DeviceDataDAO;
import com.martin.codepo.TupleData;

import java.util.ArrayList;

public class DataDevice extends AppCompatActivity {

    public static final String EXTRA_DEVICE = "DEVICE";
    private Device device;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_data);

        device = getIntent().getParcelableExtra(DevicesAdapter.EXTRA_DEVICE);

        TextView deviceName = (TextView) findViewById(R.id.deviceName);
        deviceName.setText(device.getName());
        TextView deviceIp = (TextView) findViewById(R.id.deviceIp);
        deviceIp.setText(device.getIp());

        XYPlot plot = (XYPlot) findViewById(R.id.plot);
        plot.setRangeBoundaries(0, 100, BoundaryMode.FIXED);
        DeviceDataDAO deviceDataDAO = new DeviceDataDAO(this);
        ArrayList<DeviceData> deviceDatas = deviceDataDAO.selectAllDataDeviceKey(device.getId());
        TupleData datas = listData(deviceDatas);
        ArrayList<String> listTime = listTime(deviceDatas);
        PanZoom.attach(plot);
        if(listTime.size() > 0) {
            String title = title(device.getDeviceType());
            int maxValue = (device.getDeviceType() == Types.Wifi.getNum() || device.getDeviceType() == Types.Ping.getNum() || device.getDeviceType() == Types.Electricity.getNum()) ? 1 : 100;
            plot.getGraph().getLineLabelStyle(XYGraphWidget.Edge.BOTTOM).setFormat(new GraphXLabelFormat(listTime));
            plot.getOuterLimits().set(0, listTime.size() - 1, 0, maxValue);
            XYSeries s1 = new SimpleXYSeries(datas.getFirst(), SimpleXYSeries.ArrayFormat.Y_VALS_ONLY, title);
            plot.addSeries(s1, new LineAndPointFormatter(Color.RED, Color.RED, null, null));
            if(device.getDeviceType() == Types.SNMP.getNum()){
                XYSeries s2 = new SimpleXYSeries(datas.getSecond(), SimpleXYSeries.ArrayFormat.Y_VALS_ONLY, "ram");
                plot.addSeries(s2, new LineAndPointFormatter(Color.GREEN, Color.GREEN, null, null));
                XYSeries s3 = new SimpleXYSeries(datas.getThird(), SimpleXYSeries.ArrayFormat.Y_VALS_ONLY, "disque");
                plot.addSeries(s3, new LineAndPointFormatter(Color.BLUE, Color.BLUE, null, null));
            }

        }

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_modify, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch(item.getItemId()){
            case (R.id.modify):
                Intent intent = new Intent(this, ModifyDevice.class);
                intent.putExtra(EXTRA_DEVICE, device);
                startActivity(intent);
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    private TupleData listData(ArrayList<DeviceData> datas){
        ArrayList<Float> firsts = new ArrayList<>();
        ArrayList<Float> seconds = new ArrayList<>();
        ArrayList<Float> thirds = new ArrayList<>();
        float first, second, third;
        for(DeviceData data : datas){
            first = data.getFirst(); firsts.add(first);
            second = data.getSecond(); seconds.add(second);
            third = data.getThird(); thirds.add(third);
        }
        return new TupleData(firsts, seconds, thirds);
    }

    private ArrayList<String> listTime(ArrayList<DeviceData> deviceDatas){
        ArrayList<String> listTime = new ArrayList<>();
        for(DeviceData deviceData : deviceDatas){
            String time = deviceData.getTimeStamp();
            listTime.add(time.split(" ")[1]);
        }
        return listTime;
    }

    private String title(int deviceType){
        String title;
        if(device.getDeviceType() == Types.Android.getNum()){
            title = "Charge";
        }
        else if(device.getDeviceType() == Types.SNMP.getNum()){
            title = "cpu";
        } else {
            title= "Présence";
        }
        return title;
    }
}
