package com.martin.codepo.activity;

import android.content.Intent;
import android.os.Build;
import android.support.annotation.RequiresApi;
import android.view.MenuItem;

import com.martin.codepo.R;
import com.martin.codepo.Types;
import com.martin.codepo.database.CheckTime;
import com.martin.codepo.database.Device;
import com.martin.codepo.database.DeviceConfig;
import com.martin.codepo.database.DeviceDAO;
import com.martin.codepo.database.DeviceUser;
import com.martin.codepo.database.Threshold;
import com.martin.codepo.database.User;
import com.martin.codepo.monitoring.SharedMonitoringTools;
import com.martin.codepo.monitoring.sms.SmsFormat;

import java.util.ArrayList;
import java.util.UUID;

public class ModifyDevice extends Data {

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()){
            case R.id.save:
                DeviceDAO deviceDAO = new DeviceDAO(this);
                Device device = modifyDevice();
                deviceDAO.updateDevice(device);
                SharedMonitoringTools.sendSmsServer(SmsFormat.DeviceFormat(this, device.getId(), "update"), this);
                Data.networkDevice = null;
                Intent intent = new Intent(this, Monitoring.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intent);
                finish();
                return true;
            default:
                return super.onContextItemSelected(item);
        }
    }

    public Device modifyDevice(){
        device.setIp(deviceIp.getText().toString());
        device.setName(deviceName.getText().toString());
        Threshold threshold = device.getThreshold();
        if(type == Types.Android.getNum() || type == Types.TCP.getNum() || type == Types.SNMP.getNum() || type == Types.Solar.getNum()){
            threshold.setFirst(Float.valueOf(thresholdFirst.getText().toString()));
            if(type == Types.SNMP.getNum()){
                threshold.setSecond(Float.valueOf(thresholdSecond.getText().toString()));
                threshold.setThird(Float.valueOf(thresholdThird.getText().toString()));
            }
        }
        device.setThreshold(threshold);
        DeviceConfig deviceConfig = device.getDeviceConfig();
        if(type == Types.SNMP.getNum()){
            deviceConfig.setFirst(configFirst.getText().toString());
            deviceConfig.setSecond(configSecond.getText().toString());
            deviceConfig.setThird(configThird.getSelectedItem().toString());
        }
        device.setDeviceConfig(deviceConfig);
        ArrayList<DeviceUser> users = new ArrayList<>();
        usersChoosen = choosableUsersAdapter.getPhonesChoosen();
        for(User userChoosen : usersChoosen){
            DeviceUser user = new DeviceUser(String.valueOf(UUID.randomUUID()), device.getId(), userChoosen.getId());
            users.add(user);
        }
        device.setDeviceUsers(users);
        return device;

    }

}
