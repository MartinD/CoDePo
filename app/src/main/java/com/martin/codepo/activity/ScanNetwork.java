package com.martin.codepo.activity;

import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.app.AppCompatDialog;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.StaggeredGridLayoutManager;
import android.util.DisplayMetrics;
import android.view.Display;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.martin.codepo.NetworkDevice;
import com.martin.codepo.adapter.NetworkDeviceAdapter;
import com.martin.codepo.Pinger;
import com.martin.codepo.R;

import java.net.InetAddress;
import java.net.NetworkInterface;
import java.net.SocketException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import static org.apache.http.conn.util.InetAddressUtils.isIPv4Address;

public class ScanNetwork extends AppCompatActivity {

    private NetworkDeviceAdapter networkDeviceAdapter = new NetworkDeviceAdapter(new ArrayList<NetworkDevice>(), R.layout.device_fragment, this);

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_scan_network);

        Button confirm = (Button) findViewById(R.id.confirm);
        confirm.setOnClickListener(confirmListener);


        Display display = getWindowManager().getDefaultDisplay();
        DisplayMetrics outMetrics = new DisplayMetrics ();
        display.getMetrics(outMetrics);

        float density  = getResources().getDisplayMetrics().density;
        float dpHeight = outMetrics.heightPixels / density;
        float dpWidth  = outMetrics.widthPixels / density;

        int numrows = (int) Math.floor(dpWidth / 300);

        RecyclerView recyclerView = (RecyclerView) findViewById(R.id.list);

        StaggeredGridLayoutManager manager = new StaggeredGridLayoutManager(numrows, StaggeredGridLayoutManager.VERTICAL);
        manager.setGapStrategy(StaggeredGridLayoutManager.GAP_HANDLING_MOVE_ITEMS_BETWEEN_SPANS);
        recyclerView.setLayoutManager(manager);
        recyclerView.setHasFixedSize(false);
        recyclerView.setAdapter(networkDeviceAdapter);

        recyclerView.setItemAnimator(new DefaultItemAnimator());

        rescan();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_redo, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        rescan();
        return super.onOptionsItemSelected(item);
    }

    private void rescan(){

        ConnectivityManager connManager = (ConnectivityManager) getSystemService(CONNECTIVITY_SERVICE);
        NetworkInfo mWifi = connManager.getNetworkInfo(ConnectivityManager.TYPE_WIFI);

        if (mWifi.isConnected() && mWifi.isAvailable()){
            final AlertDialog.Builder builder = new AlertDialog.Builder(this);
            View serverInfo = getLayoutInflater().inflate(R.layout.dialog_charging, null);
            builder.setView(serverInfo);
            AppCompatDialog dialog = builder.create();
            dialog.setCancelable(false);
            AsyncScan scan = new AsyncScan(dialog);
            scan.execute(networkDeviceAdapter);
        }else {
            Toast.makeText(this, "vous n'êtes pas connecté au device_wifi", Toast.LENGTH_LONG).show();
        }
    }

    private View.OnClickListener confirmListener = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            finish();
        }
    };

    private class AsyncScan extends AsyncTask<NetworkDeviceAdapter, Void, List<NetworkDevice>> {

        private NetworkDeviceAdapter adapter;
        private AppCompatDialog mDialog;

        public AsyncScan(AppCompatDialog dialog) {
            super();
            this.mDialog = dialog;
            mDialog.show();
        }

        protected List<NetworkDevice> doInBackground(NetworkDeviceAdapter... voids) {

            String ipString = getLocalIpv4Address();

            if (ipString == null){
                return new ArrayList<NetworkDevice>(1);
            }
            int lastdot = ipString.lastIndexOf(".");
            ipString = ipString.substring(0, lastdot);

            List<NetworkDevice> addresses = Pinger.getDevicesOnNetwork(ipString);
            adapter = voids[0];
            return addresses;
        }

        @Override
        protected void onPostExecute(List<NetworkDevice> inetAddresses) {
            super.onPostExecute(inetAddresses);
            adapter.setAddresses(inetAddresses);
            adapter.notifyDataSetChanged();
            mDialog.cancel();
        }

        @Override
        protected void onProgressUpdate(Void... values) {
            super.onProgressUpdate(values);
        }

        private String getLocalIpv4Address(){
            try {
                String ipv4;
                List<NetworkInterface>  nilist = Collections.list(NetworkInterface.getNetworkInterfaces());
                if(nilist.size() > 0){
                    for (NetworkInterface ni: nilist){
                        List<InetAddress>  ialist = Collections.list(ni.getInetAddresses());
                        if(ialist.size()>0){
                            for (InetAddress address: ialist){
                                if (!address.isLoopbackAddress()  && isIPv4Address(ipv4=address.getHostAddress())){
                                    return ipv4;
                                }
                            }
                        }
                    }
                }
            } catch (SocketException ex) {

            }
            return "";
        }
    }


}
