package com.martin.codepo.activity;


import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.RequiresApi;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.helper.ItemTouchHelper;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Toast;

import com.martin.codepo.R;
import com.martin.codepo.Types;
import com.martin.codepo.adapter.DevicesAdapter;
import com.martin.codepo.database.Device;
import com.martin.codepo.database.DeviceDAO;
import com.martin.codepo.monitoring.SharedMonitoringTools;
import com.martin.codepo.monitoring.sms.SmsFormat;

import java.util.ArrayList;


/**
 * A simple {@link Fragment} subclass.
 */
public class ServerMonitoring extends Fragment {

    private DevicesAdapter devicesAdapter;
    private AlertDialog monitoringDialog;
    private DeviceDAO deviceDAO;
    private ArrayList<Device> devices;

    public ServerMonitoring() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(final LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        final View view = inflater.inflate(R.layout.battery_server_container, container, false);

        deviceDAO = new DeviceDAO(getContext());
        devices = deviceDAO.selectAllDevices(true);
        devicesAdapter = new DevicesAdapter(devices);
        RecyclerView recyclerViewServers = (RecyclerView) view.findViewById(R.id.serversMonitored);
        recyclerViewServers.setLayoutManager(new LinearLayoutManager(getContext()));
        recyclerViewServers.setAdapter(devicesAdapter);

        new ItemTouchHelper(new ItemTouchHelper.SimpleCallback(0, ItemTouchHelper.RIGHT){

            @Override
            public boolean onMove(RecyclerView recyclerView, RecyclerView.ViewHolder viewHolder, RecyclerView.ViewHolder target) {
                return false;
            }

            @Override
            public void onSwiped(RecyclerView.ViewHolder viewHolder, int direction) {
                final int pos = (int) viewHolder.itemView.getTag();
                AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
                builder.setTitle(R.string.attention);
                builder.setMessage(R.string.delete_service);
                builder.setCancelable(false);
                builder.setPositiveButton("Oui", new DialogInterface.OnClickListener() {
                    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        Device device = devicesAdapter.getDevices().get(pos);
                        devicesAdapter.getDevices().remove(pos);
                        deviceDAO.delDevice(device.getId());
                        SharedMonitoringTools.sendSmsServer(SmsFormat.deleteDeviceFormat(device.getId()), getContext());
                        devicesAdapter.setDevices(devices);
                        devicesAdapter.notifyDataSetChanged();
                        dialog.cancel();
                    }
                });
                builder.setNegativeButton("Non", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        devicesAdapter.setDevices(devices);
                        devicesAdapter.notifyDataSetChanged();
                        dialog.cancel();
                    }
                });
                AlertDialog alertDialog = builder.create();
                alertDialog.show();
            }

        }).attachToRecyclerView(recyclerViewServers);

        FloatingActionButton add = (FloatingActionButton) view.findViewById(R.id.add);
        add.setOnClickListener(addListener);

        return view;
    }


    private View.OnClickListener addListener = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            showDialog();

        }
    };

    private void showDialog(){
        AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
        builder.setTitle(R.string.what_to_monitor);
        builder.setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                dialogInterface.cancel();
            }
        });


        String[] monitoringTypes = {Types.Wifi.getName(), Types.Ping.getName(), Types.SNMP.getName(),
                Types.TCP.getName(), Types.Android.getName(), Types.Electricity.getName(), Types.Solar.getName()};

        ArrayAdapter<String> listAdapter = new ArrayAdapter<>(getContext(), android.R.layout.simple_list_item_1, monitoringTypes);
        ListView listView = new ListView(getContext());
        listView.setOnItemClickListener(listViewListener);
        listView.setAdapter(listAdapter);
        builder.setView(listView);
        monitoringDialog = builder.create();
        monitoringDialog.show();
    }

    private AdapterView.OnItemClickListener listViewListener = new AdapterView.OnItemClickListener() {
        @Override
        public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
            String type = String.valueOf(adapterView.getItemAtPosition(i));
            int deviceType = Types.numFromString(type);

            if(deviceType == Types.Electricity.getNum() || deviceType == Types.Android.getNum() || deviceType == Types.Wifi.getNum()){
                if(isDeviceTypeMonitored(deviceType)){
                    Toast.makeText(getContext(), R.string.already_monitored, Toast.LENGTH_LONG).show();
                } else {
                    Intent intent = new Intent(getContext(), EncodeDevice.class);
                    intent.putExtra(DataDevice.EXTRA_DEVICE, new Device(Types.numFromString(type)));
                    startActivity(intent);
                }
            } else {
                Intent intent = new Intent(getContext(), EncodeDevice.class);
                intent.putExtra(DataDevice.EXTRA_DEVICE, new Device(Types.numFromString(type)));
                startActivity(intent);
            }
        }
    };


    @Override
    public void onResume() {
        super.onResume();

        if(monitoringDialog != null)
            monitoringDialog.dismiss();
        Context context = getContext();

        DeviceDAO deviceDAO = new DeviceDAO(context);
        devices = deviceDAO.selectAllDevices(true);

        devicesAdapter.setDevices(devices);
        devicesAdapter.notifyDataSetChanged();
        if(monitoringDialog!=null)
            monitoringDialog.hide();
    }

    private boolean isDeviceTypeMonitored(int type){
        for(Device device : devices){
            if(device.getDeviceType() == type)
                return true;
        }
        return false;
    }


}