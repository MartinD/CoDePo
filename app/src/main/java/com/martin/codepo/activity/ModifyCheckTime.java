package com.martin.codepo.activity;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Build;
import android.preference.PreferenceManager;
import android.support.annotation.RequiresApi;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ArrayAdapter;
import android.widget.Spinner;

import com.martin.codepo.R;
import com.martin.codepo.Types;
import com.martin.codepo.database.CheckTime;
import com.martin.codepo.database.CheckTimeDAO;
import com.martin.codepo.monitoring.SharedMonitoringTools;
import com.martin.codepo.monitoring.sms.DayliSmsService;
import com.martin.codepo.monitoring.sms.SmsFormat;

import java.text.Format;
import java.util.ArrayList;
import java.util.Calendar;

public class ModifyCheckTime extends AppCompatActivity {

    public static final String SMS_HOUR = "SMS_HOUR";

    private Spinner dayliSmsHour;
    private Spinner checkTimeControlWifi, checkTimeNotificationWifi, checkTimeIntervalWifi;
    private Spinner checkTimeControlPing, checkTimeNotificationPing, checkTimeIntervalPing;
    private Spinner checkTimeControlSnmp, checkTimeNotificationSnmp, checkTimeIntervalSnmp;
    private Spinner checkTimeControlTcp, checkTimeNotificationTcp, checkTimeIntervalTcp;
    private Spinner checkTimeControlAndroid, checkTimeNotificationAndroid, checkTimeIntervalAndroid;
    private Spinner checkTimeControlElectricity, checkTimeNotificationElectricity, checkTimeIntervalElectricity;
    private Spinner checkTimeControlSolar, checkTimeNotificationSolar, checkTimeIntervalSolar;
    private String[] spinnerDayliSms = {"1", "2", "3", "4", "5", "6", "7", "8", "9", "10", "11", "12",
            "13", "14", "15", "16", "17", "18", "19", "20", "21", "22", "23", "00"};
    private String[] spinnerControlText = {"3", "5", "10"};
    private String[] spinnerNotificationText = {"2", "4", "6"};
    private String[] spinnerIntervalText = {"2", "4", "6"};
    private CheckTimeDAO checkTimeDAO;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_check_time);

        checkTimeDAO = new CheckTimeDAO(this);

        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(this);
        String smsHour = sharedPreferences.getString(SMS_HOUR, "16");
        CheckTime checkTimeWifi = checkTimeDAO.selectCheckTimeByDeviceType(Types.Wifi.getNum());
        CheckTime checkTimePing = checkTimeDAO.selectCheckTimeByDeviceType(Types.Ping.getNum());
        CheckTime checkTimeSnmp = checkTimeDAO.selectCheckTimeByDeviceType(Types.SNMP.getNum());
        CheckTime checkTimeTcp = checkTimeDAO.selectCheckTimeByDeviceType(Types.TCP.getNum());
        CheckTime checkTimeAndroid = checkTimeDAO.selectCheckTimeByDeviceType(Types.Android.getNum());
        CheckTime checkTimeElectricity = checkTimeDAO.selectCheckTimeByDeviceType(Types.Electricity.getNum());
        CheckTime checkTimeSolar = checkTimeDAO.selectCheckTimeByDeviceType(Types.Solar.getNum());

        ArrayAdapter<String> dayliSmsHourAdapter = new ArrayAdapter<>(this, android.R.layout.simple_spinner_dropdown_item, spinnerDayliSms);
        ArrayAdapter<String> checkTimeControlAdapter = new ArrayAdapter<>(this, android.R.layout.simple_spinner_dropdown_item, spinnerControlText);
        ArrayAdapter<String> checkTimeNotificationAdapter = new ArrayAdapter<>(this, android.R.layout.simple_spinner_dropdown_item, spinnerNotificationText);
        ArrayAdapter<String> checkTimeIntervalAdapter = new ArrayAdapter<>(this, android.R.layout.simple_spinner_dropdown_item, spinnerIntervalText);

        dayliSmsHour = (Spinner) findViewById(R.id.dayliSmsHour);
        dayliSmsHour.setAdapter(dayliSmsHourAdapter);
        dayliSmsHour.setSelection(dayliSmsHourAdapter.getPosition(smsHour));

        checkTimeControlWifi = (Spinner) findViewById(R.id.checkTimeControlWifi);
        checkTimeControlWifi.setAdapter(checkTimeControlAdapter);
        checkTimeControlWifi.setSelection(checkTimeControlAdapter.getPosition(String.valueOf(checkTimeWifi.getControl())));
        checkTimeNotificationWifi = (Spinner) findViewById(R.id.checkTimeNotificationWifi);
        checkTimeNotificationWifi.setAdapter(checkTimeNotificationAdapter);
        checkTimeNotificationWifi.setSelection(checkTimeNotificationAdapter.getPosition(String.valueOf(checkTimeWifi.getFirstNotif())));
        checkTimeIntervalWifi = (Spinner) findViewById(R.id.checkTimeIntervalWifi);
        checkTimeIntervalWifi.setAdapter(checkTimeIntervalAdapter);
        checkTimeIntervalWifi.setSelection(checkTimeIntervalAdapter.getPosition(String.valueOf(checkTimeWifi.getIntervalNotif())));

        checkTimeControlPing = (Spinner) findViewById(R.id.checkTimeControlPing);
        checkTimeControlPing.setAdapter(checkTimeControlAdapter);
        checkTimeControlPing.setSelection(checkTimeControlAdapter.getPosition(String.valueOf(checkTimePing.getControl())));
        checkTimeNotificationPing = (Spinner) findViewById(R.id.checkTimeNotificationPing);
        checkTimeNotificationPing.setAdapter(checkTimeNotificationAdapter);
        checkTimeNotificationPing.setSelection(checkTimeNotificationAdapter.getPosition(String.valueOf(checkTimePing.getFirstNotif())));
        checkTimeIntervalPing = (Spinner) findViewById(R.id.checkTimeIntervalPing);
        checkTimeIntervalPing.setAdapter(checkTimeIntervalAdapter);
        checkTimeIntervalPing.setSelection(checkTimeIntervalAdapter.getPosition(String.valueOf(checkTimePing.getIntervalNotif())));

        checkTimeControlSnmp = (Spinner) findViewById(R.id.checkTimeControlSnmp);
        checkTimeControlSnmp.setAdapter(checkTimeControlAdapter);
        checkTimeControlSnmp.setSelection(checkTimeControlAdapter.getPosition(String.valueOf(checkTimeSnmp.getControl())));
        checkTimeNotificationSnmp = (Spinner) findViewById(R.id.checkTimeNotificationSnmp);
        checkTimeNotificationSnmp.setAdapter(checkTimeNotificationAdapter);
        checkTimeNotificationSnmp.setSelection(checkTimeNotificationAdapter.getPosition(String.valueOf(checkTimeSnmp.getFirstNotif())));
        checkTimeIntervalSnmp = (Spinner) findViewById(R.id.checkTimeIntervalSnmp);
        checkTimeIntervalSnmp.setAdapter(checkTimeIntervalAdapter);
        checkTimeIntervalSnmp.setSelection(checkTimeIntervalAdapter.getPosition(String.valueOf(checkTimeSnmp.getIntervalNotif())));

        checkTimeControlTcp = (Spinner) findViewById(R.id.checkTimeControlTcp);
        checkTimeControlTcp.setAdapter(checkTimeControlAdapter);
        checkTimeControlTcp.setSelection(checkTimeControlAdapter.getPosition(String.valueOf(checkTimeTcp.getControl())));
        checkTimeNotificationTcp = (Spinner) findViewById(R.id.checkTimeNotificationTcp);
        checkTimeNotificationTcp.setAdapter(checkTimeNotificationAdapter);
        checkTimeNotificationTcp.setSelection(checkTimeNotificationAdapter.getPosition(String.valueOf(checkTimeTcp.getFirstNotif())));
        checkTimeIntervalTcp = (Spinner) findViewById(R.id.checkTimeIntervalTcp);
        checkTimeIntervalTcp.setAdapter(checkTimeIntervalAdapter);
        checkTimeIntervalTcp.setSelection(checkTimeIntervalAdapter.getPosition(String.valueOf(checkTimeTcp.getIntervalNotif())));

        checkTimeControlAndroid = (Spinner) findViewById(R.id.checkTimeControlAndroid);
        checkTimeControlAndroid.setAdapter(checkTimeControlAdapter);
        checkTimeControlAndroid.setSelection(checkTimeControlAdapter.getPosition(String.valueOf(checkTimeAndroid.getControl())));
        checkTimeNotificationAndroid = (Spinner) findViewById(R.id.checkTimeNotificationAndroid);
        checkTimeNotificationAndroid.setAdapter(checkTimeNotificationAdapter);
        checkTimeNotificationAndroid.setSelection(checkTimeNotificationAdapter.getPosition(String.valueOf(checkTimeAndroid.getFirstNotif())));
        checkTimeIntervalAndroid = (Spinner) findViewById(R.id.checkTimeIntervalAndroid);
        checkTimeIntervalAndroid.setAdapter(checkTimeIntervalAdapter);
        checkTimeIntervalAndroid.setSelection(checkTimeIntervalAdapter.getPosition(String.valueOf(checkTimeAndroid.getIntervalNotif())));

        checkTimeControlElectricity = (Spinner) findViewById(R.id.checkTimeControlElectricity);
        checkTimeControlElectricity.setAdapter(checkTimeControlAdapter);
        checkTimeControlElectricity.setSelection(checkTimeControlAdapter.getPosition(String.valueOf(checkTimeElectricity.getControl())));
        checkTimeNotificationElectricity = (Spinner) findViewById(R.id.checkTimeNotificationElectricity);
        checkTimeNotificationElectricity.setAdapter(checkTimeNotificationAdapter);
        checkTimeNotificationElectricity.setSelection(checkTimeNotificationAdapter.getPosition(String.valueOf(checkTimeElectricity.getFirstNotif())));
        checkTimeIntervalElectricity = (Spinner) findViewById(R.id.checkTimeIntervalElectricity);
        checkTimeIntervalElectricity.setAdapter(checkTimeNotificationAdapter);
        checkTimeIntervalElectricity.setSelection(checkTimeNotificationAdapter.getPosition(String.valueOf(checkTimeElectricity.getIntervalNotif())));

        checkTimeControlSolar = (Spinner) findViewById(R.id.checkTimeControlSolar);
        checkTimeControlSolar.setAdapter(checkTimeControlAdapter);
        checkTimeControlSolar.setSelection(checkTimeControlAdapter.getPosition(String.valueOf(checkTimeSolar.getControl())));
        checkTimeNotificationSolar = (Spinner) findViewById(R.id.checkTimeNotificationSolar);
        checkTimeNotificationSolar.setAdapter(checkTimeNotificationAdapter);
        checkTimeNotificationSolar.setSelection(checkTimeNotificationAdapter.getPosition(String.valueOf(checkTimeSolar.getFirstNotif())));
        checkTimeIntervalSolar = (Spinner) findViewById(R.id.checkTimeIntervalSolar);
        checkTimeIntervalSolar.setAdapter(checkTimeNotificationAdapter);
        checkTimeIntervalSolar.setSelection(checkTimeNotificationAdapter.getPosition(String.valueOf(checkTimeSolar.getIntervalNotif())));

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_save, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @RequiresApi(api = Build.VERSION_CODES.M)
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.save:
                modifyCheckTime();
                AlarmManager alarmManager = (AlarmManager) this.getSystemService(ALARM_SERVICE);
                Calendar calendar = Calendar.getInstance();
                calendar.setTimeInMillis(System.currentTimeMillis());
                SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(this);
                Integer hour = Integer.valueOf(sharedPreferences.getString(ModifyCheckTime.SMS_HOUR, "16"));
                calendar.set(Calendar.HOUR_OF_DAY, hour);
                Intent intent = new Intent(this, DayliSmsService.class);
                PendingIntent pendingIntent = PendingIntent.getService(this, 10, intent, 0);
                alarmManager.setExactAndAllowWhileIdle(AlarmManager.RTC_WAKEUP, calendar.getTimeInMillis(), pendingIntent);
                finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    private void modifyCheckTime(){
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(this);
        sharedPreferences.edit().putString(SMS_HOUR, dayliSmsHour.getSelectedItem().toString()).apply();
        CheckTime checkTimeWifi = new CheckTime(Types.Wifi.getNum(), Integer.valueOf(checkTimeControlWifi.getSelectedItem().toString()), Integer.valueOf(checkTimeNotificationWifi.getSelectedItem().toString()), Integer.valueOf(checkTimeIntervalWifi.getSelectedItem().toString()) );
        checkTimeDAO.updateCheckTime(checkTimeWifi);
        CheckTime checkTimePing = new CheckTime(Types.Ping.getNum(), Integer.valueOf(checkTimeControlPing.getSelectedItem().toString()), Integer.valueOf(checkTimeNotificationPing.getSelectedItem().toString()), Integer.valueOf(checkTimeIntervalPing.getSelectedItem().toString()) );
        checkTimeDAO.updateCheckTime(checkTimePing);
        CheckTime checkTimeTcp = new CheckTime(Types.TCP.getNum(), Integer.valueOf(checkTimeControlTcp.getSelectedItem().toString()), Integer.valueOf(checkTimeNotificationTcp.getSelectedItem().toString()), Integer.valueOf(checkTimeIntervalTcp.getSelectedItem().toString()) );
        checkTimeDAO.updateCheckTime(checkTimeTcp);
        CheckTime checkTimeSnmp = new CheckTime(Types.SNMP.getNum(), Integer.valueOf(checkTimeControlSnmp.getSelectedItem().toString()), Integer.valueOf(checkTimeNotificationSnmp.getSelectedItem().toString()), Integer.valueOf(checkTimeIntervalSnmp.getSelectedItem().toString()) );
        checkTimeDAO.updateCheckTime(checkTimeSnmp);
        CheckTime checkTimeAndroid = new CheckTime(Types.Android.getNum(), Integer.valueOf(checkTimeControlAndroid.getSelectedItem().toString()), Integer.valueOf(checkTimeNotificationAndroid.getSelectedItem().toString()), Integer.valueOf(checkTimeIntervalAndroid.getSelectedItem().toString()) );
        checkTimeDAO.updateCheckTime(checkTimeAndroid);
        CheckTime checkTimeSolar = new CheckTime(Types.Solar.getNum(), Integer.valueOf(checkTimeControlSolar.getSelectedItem().toString()), Integer.valueOf(checkTimeNotificationSolar.getSelectedItem().toString()), Integer.valueOf(checkTimeIntervalSolar.getSelectedItem().toString()) );
        checkTimeDAO.updateCheckTime(checkTimeSolar);
        CheckTime checkTimeElectricity = new CheckTime(Types.Electricity.getNum(), Integer.valueOf(checkTimeControlElectricity.getSelectedItem().toString()), Integer.valueOf(checkTimeNotificationElectricity.getSelectedItem().toString()), Integer.valueOf(checkTimeIntervalElectricity.getSelectedItem().toString()) );
        checkTimeDAO.updateCheckTime(checkTimeElectricity);
        SharedMonitoringTools.sendSmsServer(SmsFormat.updateCheckTimeFormat(this), this);
    }
}
