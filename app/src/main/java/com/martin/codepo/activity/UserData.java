package com.martin.codepo.activity;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.Menu;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.Spinner;

import com.martin.codepo.R;
import com.martin.codepo.adapter.ChosableDevicesAdapter;
import com.martin.codepo.adapter.UsersAdapter;
import com.martin.codepo.database.CenterDAO;
import com.martin.codepo.database.Device;
import com.martin.codepo.database.DeviceDAO;
import com.martin.codepo.database.User;

import java.util.ArrayList;

public class UserData extends AppCompatActivity {

    protected User user;
    protected EditText name, firstname, email, password, phone;
    protected Spinner country, prefix;
    protected String[] countryNames = {"Afrique du Sud", "Algérie", "Angola", "BéninNovo", "Botswana", "Burkina Faso", "Burundi",
            "Cameroun", "Cap-Vert", "Centrafrique", "Congo", "Congo RDC", "Côte d'Ivoire", "Le Djibouti", "Egypte", "Erythrée", "EthiopieAbeba",
            "Gabon", "Gambie", "Ghana", "Guinée", "Guinée équatoriale", "Guinée-Bissau", "Kenya", "Lesotho", "Libéria", "Libye", "Madagascar",
            "Malawi", "Mali", "Maroc", "Mauritanie", "Mozambique", "Namibie", "Niger", "Nigeria", "Ouganda", "Rwanda", "Sao Tomé-et-PrincipeTomé",
            "Sénégal", "Sierra Leone", "Somalie", "Soudan", "Sud-Soudanou Djouba", "Swaziland", "Tanzanie", "Tchad", "Togo", "Tunisie", "Zambie",
            "Zimbabwe"};
    protected String[] prefixNumber = {"27", "213", "244", "229", "267", "226", "257", "237", "238", "236", "242", "243", "225", "253", "20",
            "291", "251", "241", "220", "233", "224", "240", "245", "254", "266", "231", "218", "261", "265", "223", "212", "222", "258", "264",
            "227", "234", "256", "250", "239", "221", "232", "252", "249", "211", "268", "255", "235", "228", "216", "260", "263"};
    protected CenterDAO centerDAO;
    protected ArrayAdapter<String> countryAdapter;
    protected ArrayAdapter<String> prefixAdapter;

    protected ArrayList<Device> deviceChoosen = new ArrayList<>();
    protected ChosableDevicesAdapter chosableDevicesAdapter;
    private DeviceDAO deviceDAO = new DeviceDAO(this);

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_encode_user);

        user = getIntent().getParcelableExtra(UsersAdapter.EXTRA_USER);

        centerDAO = new CenterDAO(this);

        name = (EditText) findViewById(R.id.name);
        firstname = (EditText) findViewById(R.id.firstname);
        email = (EditText) findViewById(R.id.email);
        password = (EditText) findViewById(R.id.password);
        phone = (EditText) findViewById(R.id.phone);
        country = (Spinner) findViewById(R.id.country);
        prefix = (Spinner) findViewById(R.id.prefix);

        countryAdapter = new ArrayAdapter<>(this, android.R.layout.simple_spinner_dropdown_item, countryNames);
        country.setAdapter(countryAdapter);
        country.setOnItemSelectedListener(onItemCountrySelected);
        prefixAdapter = new ArrayAdapter<>(this, android.R.layout.simple_spinner_dropdown_item, prefixNumber);
        prefix.setAdapter(prefixAdapter);
        prefix.setOnItemSelectedListener(onItemPrefixSelected);

        if(user != null){
            name.setText(user.getName());
            firstname.setText(user.getFirstname());
            email.setText(user.getEmail());
            password.setText(user.getPassword());
            phone.setText(user.getPhone());
            country.setSelection(countryAdapter.getPosition(user.getCountry()));
            prefix.setSelection(prefixAdapter.getPosition(user.getPrefix()));
            deviceChoosen = deviceDAO.selectAllDevicesByUser(user.getDeviceUsers(), true);
        }
        chosableDevicesAdapter = new ChosableDevicesAdapter(deviceDAO.selectAllDevices(true), deviceChoosen);
        RecyclerView recyclerViewPhone = (RecyclerView) findViewById(R.id.listDevices);
        recyclerViewPhone.setLayoutManager(new LinearLayoutManager(this));
        recyclerViewPhone.setAdapter(chosableDevicesAdapter);

    }

    private AdapterView.OnItemSelectedListener onItemCountrySelected = new AdapterView.OnItemSelectedListener() {

        @Override
        public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
            prefix.setSelection(position);
        }

        @Override
        public void onNothingSelected(AdapterView<?> parent) {

        }

    };

    private AdapterView.OnItemSelectedListener onItemPrefixSelected = new AdapterView.OnItemSelectedListener() {
        @Override
        public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
            country.setSelection(position);
        }

        @Override
        public void onNothingSelected(AdapterView<?> parent) {

        }
    };


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_save, menu);
        return super.onCreateOptionsMenu(menu);
    }


}
