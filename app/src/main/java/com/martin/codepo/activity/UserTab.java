package com.martin.codepo.activity;


import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.helper.ItemTouchHelper;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.martin.codepo.R;
import com.martin.codepo.adapter.UsersAdapter;
import com.martin.codepo.database.User;
import com.martin.codepo.database.UserDAO;

import java.util.ArrayList;

/**
 * A simple {@link Fragment} subclass.
 */
public class UserTab extends Fragment {

    private UserDAO userDAO;
    private ArrayList<User> users;
    private UsersAdapter usersAdapter;

    public UserTab() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.battery_server_container, container, false);


        userDAO = new UserDAO(getContext());
        users = userDAO.selectAllUser();
        usersAdapter = new UsersAdapter(users);

        RecyclerView recyclerViewPhone = (RecyclerView) view.findViewById(R.id.serversMonitored);
        recyclerViewPhone.setLayoutManager(new LinearLayoutManager(getContext()));
        recyclerViewPhone.setAdapter(usersAdapter);


        new ItemTouchHelper(new ItemTouchHelper.SimpleCallback(0, ItemTouchHelper.RIGHT){
            @Override
            public boolean onMove(RecyclerView recyclerView, RecyclerView.ViewHolder viewHolder, RecyclerView.ViewHolder target) {
                return false;
            }

            @Override
            public void onSwiped(RecyclerView.ViewHolder viewHolder, int direction) {
                final int pos = (int) viewHolder.itemView.getTag();
                users = userDAO.selectAllUser();

                AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
                builder.setTitle("Attention!");
                builder.setMessage("Êtes-vous sûr de vouloir supprimer cet utilisateur de la mémoire? Cela affectera les services qui le contactent.");
                builder.setCancelable(false);
                builder.setPositiveButton("Oui", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        usersAdapter.getUsersChoosen().remove(users.get(pos));
                        userDAO.delUser(users.get(pos).getId());
                        usersAdapter.setUsers(userDAO.selectAllUser());
                        usersAdapter.notifyDataSetChanged();
                        dialog.cancel();
                    }
                });
                builder.setNegativeButton("Non", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        usersAdapter.setUsers(users);
                        usersAdapter.notifyDataSetChanged();
                        dialog.cancel();
                    }
                });
                AlertDialog alertDialog = builder.create();
                alertDialog.show();

            }

        }).attachToRecyclerView(recyclerViewPhone);


        FloatingActionButton add = (FloatingActionButton) view.findViewById(R.id.add);
        add.setOnClickListener(addListener);

        return view;
    }

    private View.OnClickListener addListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            Intent intent = new Intent(getContext(), EncodeUser.class);
            startActivity(intent);
        }
    };

    @Override
    public void onResume() {
        super.onResume();

        userDAO = new UserDAO(getContext());
        users = userDAO.selectAllUser();
        usersAdapter.setUsers(users);
        usersAdapter.notifyDataSetChanged();

    }
}
