package com.martin.codepo.activity;

import android.os.Build;
import android.support.annotation.RequiresApi;
import android.view.MenuItem;
import android.widget.EditText;

import com.martin.codepo.R;
import com.martin.codepo.Types;
import com.martin.codepo.database.Center;
import com.martin.codepo.database.CenterDAO;
import com.martin.codepo.database.CheckTime;
import com.martin.codepo.database.CheckTimeDAO;
import com.martin.codepo.database.Device;
import com.martin.codepo.database.DeviceConfig;
import com.martin.codepo.database.DeviceDAO;
import com.martin.codepo.database.DeviceUser;
import com.martin.codepo.database.Threshold;
import com.martin.codepo.database.User;
import com.martin.codepo.monitoring.SharedMonitoringTools;
import com.martin.codepo.monitoring.sms.SmsFormat;

import java.util.ArrayList;
import java.util.UUID;

public class EncodeDevice extends Data {


    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        EditText threshold, thresholdCpu, thresholdRam, thresholdDisk;
        switch (item.getItemId()){
            case R.id.save:
                DeviceDAO deviceDAO = new DeviceDAO(this);
                Device device = addDevice();
                deviceDAO.addDevice(device);
                SharedMonitoringTools.sendSmsServer(SmsFormat.DeviceFormat(this, device.getId(), "create"), this);
                Data.networkDevice = null;
                finish();
                /*switch (getDeviceType()){
                    case ("Wifi"):
                        WifiDAO wifiDAO = new WifiDAO(this);
                        wifiDAO.addWifi(getEncodeWifi().addWifi());
                        Data.networkDevice = null;
                        finish();
                        return true;
                    case ("Ping"):
                        PingDAO pingDAO = new PingDAO(this);
                        pingDAO.addPing(getEncodePing().addPing());
                        Data.networkDevice = null;
                        finish();
                        return true;
                    case ("SNMP"):
                        thresholdCpu = getEncodeSNMP().getThresholdCpu();
                        thresholdRam = getEncodeSNMP().getThresholdRAM();
                        thresholdDisk = getEncodeSNMP().getThresholdDISK();
                        if(TextUtils.isEmpty(thresholdCpu.getText().toString())){
                            thresholdCpu.setError(getString(R.string.error_field_required));
                            return false;
                        }
                        else if (TextUtils.isEmpty(thresholdRam.getText().toString())) {
                            thresholdRam.setError(getString(R.string.error_field_required));
                            return false;
                        }
                        else if (TextUtils.isEmpty(thresholdDisk.getText().toString())) {
                            thresholdDisk.setError(getString(R.string.error_field_required));
                            return false;
                        }
                        else {
                            SnmpDAO snmpDAO = new SnmpDAO(this);
                            snmpDAO.addSnmp(getEncodeSNMP().addSnmp());
                            Data.networkDevice = null;
                            finish();
                            return true;
                        }
                    case ("TCP"):
                        threshold = getEncodeTCP().getThresholdBat();
                        if(TextUtils.isEmpty(threshold.getText().toString())){
                            threshold.setError(getString(R.string.error_field_required));
                            return false;
                        }
                        else {
                            TcpDao tcpDao = new TcpDao(this);
                            tcpDao.addTCP(getEncodeTCP().addTcp());
                            Data.networkDevice = null;
                            finish();
                            return true;
                        }
                    case ("Android"):
                        threshold = getEncodeAndroid().getThreshold();
                        if(TextUtils.isEmpty(threshold.getText().toString())){
                            threshold.setError(getString(R.string.error_field_required));
                            return false;
                        } else {
                            AndroidDAO androidDAO = new AndroidDAO(this);
                            androidDAO.addAndroid(getEncodeAndroid().addAndroid());
                            Data.networkDevice = null;
                            finish();
                            return true;
                        }
                    case ("Electricity"):
                        ElectricityDAO electricityDAO = new ElectricityDAO(this);
                        electricityDAO.addElectricity(getEncodeElectricity().addElectricity());
                        Data.networkDevice = null;
                        finish();
                        return true;
                    case ("Solar"):
                        threshold = getEncodeSolar().getThreshold();
                        if(TextUtils.isEmpty(threshold.getText().toString())){
                            threshold.setError(getString(R.string.error_field_required));
                            return false;
                        } else {
                            SolarDAO solarDAO = new SolarDAO(this);
                            solarDAO.addSolar(getEncodeSolar().addSolar());
                            Data.networkDevice = null;
                            finish();
                            return true;
                        }
                }*/
            default:
                return super.onContextItemSelected(item);
        }
    }

    public Device addDevice(){
        CenterDAO centerDAO = new CenterDAO(this);
        Center center = centerDAO.selectCenter();
        Device device = new Device(String.valueOf(UUID.randomUUID()), deviceIp.getText().toString(), deviceName.getText().toString(), type, center.getId());
        DeviceConfig deviceConfig;
        if(type == Types.SNMP.getNum()){
            deviceConfig = new DeviceConfig(String.valueOf(UUID.randomUUID()), configFirst.getText().toString(),
                    configSecond.getText().toString(), configThird.getSelectedItem().toString(), device.getId());

        } else {
            deviceConfig = new DeviceConfig(String.valueOf(UUID.randomUUID()), "", "", "", device.getId());
        }
        Threshold threshold;
        if(type == Types.Android.getNum() || type == Types.TCP.getNum() || type == Types.SNMP.getNum() || type == Types.Solar.getNum()){
            threshold = new Threshold(String.valueOf(UUID.randomUUID()), Float.valueOf(thresholdFirst.getText().toString()), Float.valueOf(thresholdFirst.getText().toString()), Float.valueOf(thresholdFirst.getText().toString()), device.getId());
            if(type == Types.SNMP.getNum()){
                threshold = new Threshold(String.valueOf(UUID.randomUUID()), Float.valueOf(thresholdFirst.getText().toString()), Float.valueOf(thresholdSecond.getText().toString()), Float.valueOf(thresholdThird.getText().toString()), device.getId());
            }
        }else {
            threshold = new Threshold(String.valueOf(UUID.randomUUID()), 1 , 1, 1, device.getId());
        }
        ArrayList<DeviceUser> deviceUsers = new ArrayList<>();
        usersChoosen = choosableUsersAdapter.getPhonesChoosen();
        for(User userChoosen : usersChoosen){
            DeviceUser deviceUser = new DeviceUser(String.valueOf(UUID.randomUUID()), device.getId(), userChoosen.getId());
            deviceUsers.add(deviceUser);
        }
        device.setThreshold(threshold);
        device.setDeviceConfig(deviceConfig);
        device.setDeviceUsers(deviceUsers);
        return device;
    }

}
