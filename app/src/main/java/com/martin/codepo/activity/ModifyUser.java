package com.martin.codepo.activity;

import android.content.Intent;
import android.view.MenuItem;

import com.martin.codepo.R;
import com.martin.codepo.database.Device;
import com.martin.codepo.database.DeviceUser;
import com.martin.codepo.database.User;
import com.martin.codepo.database.UserDAO;

import java.util.ArrayList;
import java.util.UUID;

/**
 * Created by martin on 13/03/18.
 */

public class ModifyUser extends UserData {

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()){
            case R.id.save:
                UserDAO userDAO = new UserDAO(this);
                User user = modifyUser();
                userDAO.updateUser(user);
                //Intent intent = new Intent(this, Monitoring.class);
                //intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                //startActivity(intent);
                finish();
                return true;
            default:
                return super.onContextItemSelected(item);
        }
    }

    public User modifyUser(){
        user.setName(name.getText().toString());
        user.setFirstname(firstname.getText().toString());
        user.setEmail(email.getText().toString());
        user.setPassword(password.getText().toString());
        user.setCountry(country.getSelectedItem().toString());
        user.setPrefix(prefix.getSelectedItem().toString());
        user.setPhone(phone.getText().toString());
        ArrayList<DeviceUser> deviceUsers = new ArrayList<>();
        //deviceChoosen = chosableDevicesAdapter.getDevicesChoosen();
        for(Device deviceChoosen : deviceChoosen){
            DeviceUser deviceUser = new DeviceUser(String.valueOf(UUID.randomUUID()), deviceChoosen.getId(), user.getId());
            deviceUsers.add(deviceUser);
        }
        user.setDeviceUsers(deviceUsers);
        return user;
    }
}
