package com.martin.codepo.activity;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.Switch;
import android.widget.TextView;

import com.martin.codepo.R;
import com.martin.codepo.database.Center;
import com.martin.codepo.database.CenterDAO;
import com.martin.codepo.database.User;
import com.martin.codepo.database.UserDAO;
import com.martin.codepo.monitoring.SharedMonitoringTools;
import com.martin.codepo.monitoring.sms.SmsFormat;

import java.util.ArrayList;
import java.util.Arrays;

public class CenterData extends AppCompatActivity {

    private Center center;
    private CenterDAO centerDAO;

    private AutoCompleteTextView centerView;
    private AutoCompleteTextView responsible;
    private AutoCompleteTextView tel;
    private AutoCompleteTextView address;
    private Switch online;
    private AutoCompleteTextView phoneServer;
    private Boolean previouslyChecked;

    public static String PHONE_SERVER = "PHONE_SERVER";


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_configuration);

        centerDAO = new CenterDAO(this);
        center = centerDAO.selectCenter();

        centerView = (AutoCompleteTextView) findViewById(R.id.center);
        responsible = (AutoCompleteTextView) findViewById(R.id.responsible);
        tel = (AutoCompleteTextView) findViewById(R.id.phone);
        address = (AutoCompleteTextView) findViewById(R.id.address);
        online = (Switch) findViewById(R.id.isOnline);
        phoneServer = (AutoCompleteTextView) findViewById(R.id.phoneServer);

        if (center != null){
            fillInfo();
        }

    }

    private void registerMonitoringInformations(){
        centerView.setError(null);
        boolean error = false;

        ArrayList<AutoCompleteTextView> infos = new ArrayList<>(Arrays.asList(centerView, responsible, tel, address));
        if(online.isChecked())
            infos.add(phoneServer);

        for (AutoCompleteTextView value : infos){
            if(TextUtils.isEmpty(value.getText().toString())){
                value.setError(getString(R.string.error_field_required));
                error = true;
            }
        }
        if(!error){
            center = new Center(centerView.getText().toString(), centerView.getText().toString(),
                    address.getText().toString(), responsible.getText().toString(), tel.getText().toString(), online.isChecked() ? 1 : 0);
            centerDAO.addCenter(center);
            SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(this);
            sharedPreferences.edit().putString(PHONE_SERVER, phoneServer.getText().toString()).apply();
            if(online.isChecked()){
                SharedMonitoringTools.sendSmsServer(SmsFormat.DatabaseFormat(getApplicationContext(), "create"), this);
            }
            UserDAO userDAO = new UserDAO(getApplicationContext());
            ArrayList<User> users = userDAO.selectAllUser();
            if(users.isEmpty()){
                redirectEncodeUser();
            }
        }

    }

    @SuppressLint("CommitPrefEdits")
    private void updateMonitoringInformations(){
        centerView.setError(null);
        boolean error = false;

        ArrayList<AutoCompleteTextView> infos = new ArrayList<>(Arrays.asList(centerView, responsible, tel, address));
        if(online.isChecked())
            infos.add(phoneServer);

        for (AutoCompleteTextView value : infos){
            if(TextUtils.isEmpty(value.getText().toString())){
                value.setError(getString(R.string.error_field_required));
                error = true;
            }
        }
        if(!error){
            center = new Center(centerView.getText().toString(), centerView.getText().toString(),
                    address.getText().toString(), responsible.getText().toString(), tel.getText().toString(), online.isChecked() ? 1 : 0);
            centerDAO.updateCenter(center);
            SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(this);
            sharedPreferences.edit().putString(PHONE_SERVER, phoneServer.getText().toString()).apply();
            if(online.isChecked()){
                SharedMonitoringTools.sendSmsServer(SmsFormat.DatabaseFormat(this, "update"), this);
            }
            finish();
        }
    }

    private void fillInfo(){
        centerView.setText(center.getName());
        responsible.setText(center.getResponsible());
        tel.setText(center.getPhone());
        address.setText(center.getAddress());
        online.setChecked(center.getIsOnline() == 1);
        if(online.isChecked()){
            online.setEnabled(false);
        }
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(this);
        phoneServer.setText(sharedPreferences.getString(PHONE_SERVER, ""));
    }

    private void redirectEncodeUser(){
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("Ajouter un utilisateur");
        View redirect = getLayoutInflater().inflate(R.layout.dialog_redirect_center, null);
        TextView textView = (TextView) redirect.findViewById(R.id.textView29);
        textView.setText(R.string.first_user_config);
        builder.setView(redirect);
        builder.setCancelable(false);
        final AlertDialog dialog = builder.create();
        dialog.show();

        Button redirectConfig = (Button) redirect.findViewById(R.id.redirect);
        redirectConfig.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(getApplicationContext(), EncodeUser.class));
                dialog.cancel();
                finish();
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_save, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()){
            case R.id.save:
                if(center != null){
                    updateMonitoringInformations();
                } else {
                    registerMonitoringInformations();
                }
                return true;
            default:
                return super.onContextItemSelected(item);
        }
    }
}
