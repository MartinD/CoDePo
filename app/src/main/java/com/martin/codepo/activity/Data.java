package com.martin.codepo.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.Menu;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;

import com.martin.codepo.NetworkDevice;
import com.martin.codepo.R;
import com.martin.codepo.Types;
import com.martin.codepo.adapter.ChoosableUsersAdapter;
import com.martin.codepo.database.Device;
import com.martin.codepo.database.User;
import com.martin.codepo.database.UserDAO;

import java.util.ArrayList;

/**
 * Created by martin on 26/03/17.
 */

public class Data extends AppCompatActivity {

    public static NetworkDevice networkDevice = null;

    protected Device device;
    protected int type;
    protected EditText deviceIp, deviceName, thresholdFirst, thresholdSecond, thresholdThird, configFirst, configSecond;
    protected Spinner configThird;
    protected String[] spinnerVersionText = {"1", "2c", "3"};

    protected ArrayList<User> usersChoosen = new ArrayList<>();
    protected ChoosableUsersAdapter choosableUsersAdapter;
    private UserDAO userDAO = new UserDAO(this);

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        device = getIntent().getParcelableExtra(DataDevice.EXTRA_DEVICE);
        type = device.getDeviceType();

        if(type == Types.Wifi.getNum()){
            setContentView(R.layout.fragment_encode_wifi);
        } else if(type == Types.Ping.getNum() || type == Types.Electricity.getNum()){
            setContentView(R.layout.fragment_encode_ping);
        } else if(type == Types.SNMP.getNum()){
            setContentView(R.layout.fragment_encode_snmp);
        } else if(type == Types.TCP.getNum()){
            setContentView(R.layout.fragment_encode_tcp);
        } else if(type == Types.Android.getNum()){
            setContentView(R.layout.fragment_encode_android);
        } else if(type == Types.Solar.getNum()){
            setContentView(R.layout.fragment_encode_solar);
        } else {
            setContentView(R.layout.activity_encode_data);
        }

        ArrayAdapter<String> configThirdArrayAdapter = null;
        if(type == Types.Android.getNum() || type == Types.TCP.getNum() || type == Types.SNMP.getNum() || type == Types.Solar.getNum()){
            thresholdFirst = (EditText) findViewById(R.id.thresholdFirst);
            if(type == Types.SNMP.getNum()){
                thresholdSecond = (EditText) findViewById(R.id.thresholdSecond);
                thresholdThird = (EditText) findViewById(R.id.thresholdThird);
            }
        }
        if(type == Types.SNMP.getNum()){
            configFirst = (EditText) findViewById(R.id.configFirst);
            configSecond = (EditText) findViewById(R.id.configSecond);
            configThird = (Spinner) findViewById(R.id.configThird);
            configThirdArrayAdapter = new ArrayAdapter<>(this, android.R.layout.simple_spinner_dropdown_item, spinnerVersionText);
            configThird.setAdapter(configThirdArrayAdapter);
        }

        deviceIp = (EditText) findViewById(R.id.deviceIp);
        deviceName = (EditText) findViewById(R.id.deviceName);

        if(type != Types.Wifi.getNum()){
            Button scanButton = (Button) findViewById(R.id.scanNetButton);
            scanButton.setOnClickListener(scanNetworkListener);
        }

        if(device.getId() != null){
            deviceIp.setText(device.getIp());
            deviceName.setText(device.getName());
            if(type == Types.Android.getNum() || type == Types.TCP.getNum() || type == Types.SNMP.getNum() || type == Types.Solar.getNum()){
                thresholdFirst.setText(String.valueOf(device.getThreshold().getFirst()));
                if(type == Types.SNMP.getNum()){
                    thresholdSecond.setText(String.valueOf(device.getThreshold().getSecond()));
                    thresholdThird.setText(String.valueOf(device.getThreshold().getThird()));
                }
            }
            if(type == Types.SNMP.getNum()){
                configFirst.setText(device.getDeviceConfig().getFirst());
                configSecond.setText(device.getDeviceConfig().getSecond());
                configThird.setSelection(configThirdArrayAdapter.getPosition(device.getDeviceConfig().getThird()));
            }
            Log.d("DEVICE USERS DEVICE ", String.valueOf(device.getDeviceUsers().size()));
            usersChoosen = userDAO.selectAllUserByDevice(device.getDeviceUsers());
        }

        choosableUsersAdapter = new ChoosableUsersAdapter(userDAO.selectAllUser(), usersChoosen);
        RecyclerView recyclerViewPhone = (RecyclerView) findViewById(R.id.listContact);
        recyclerViewPhone.setLayoutManager(new LinearLayoutManager(this));
        recyclerViewPhone.setAdapter(choosableUsersAdapter);
        Button contactButton = (Button) findViewById(R.id.contactButton);
        contactButton.setOnClickListener(contactButtonListener);


    }

    @Override
    public void onResume() {
        if(EncodeDevice.networkDevice != null)
            deviceIp.setText(EncodeDevice.networkDevice.getIpAddress());
        choosableUsersAdapter.setPhones(userDAO.selectAllUser());
        choosableUsersAdapter.notifyDataSetChanged();
        super.onResume();
    }

    private View.OnClickListener scanNetworkListener = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            startActivity(new Intent(getApplicationContext(), ScanNetwork.class));
        }
    };

    private View.OnClickListener contactButtonListener = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            startActivity(new Intent(getApplicationContext(), EncodeUser.class));
        }
    };

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_save, menu);
        return super.onCreateOptionsMenu(menu);
    }

}