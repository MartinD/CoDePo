package com.martin.codepo.activity;

import android.Manifest;
import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.os.SystemClock;
import android.preference.PreferenceManager;
import android.support.annotation.NonNull;
import android.support.annotation.RequiresApi;
import android.support.design.widget.BottomNavigationView;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.crashlytics.android.Crashlytics;
import com.martin.codepo.AlarmReceiver;
import com.martin.codepo.R;
import com.martin.codepo.Types;
import com.martin.codepo.database.Center;
import com.martin.codepo.database.CenterDAO;
import com.martin.codepo.database.CheckTimeDAO;
import com.martin.codepo.monitoring.SharedMonitoringTools;
import com.martin.codepo.monitoring.devices.AndroidService;
import com.martin.codepo.monitoring.devices.ApplicationService;
import com.martin.codepo.monitoring.devices.ElectricityService;
import com.martin.codepo.monitoring.devices.PingService;
import com.martin.codepo.monitoring.devices.SnmpService;
import com.martin.codepo.monitoring.devices.SolarService;
import com.martin.codepo.monitoring.devices.TcpService;
import com.martin.codepo.monitoring.devices.WifiService;
import com.martin.codepo.monitoring.sms.AlertSmsService;
import com.martin.codepo.monitoring.sms.DayliSmsService;
import com.martin.codepo.monitoring.sms.NoAlertSmsService;

import java.util.ArrayList;
import java.util.Calendar;

import io.fabric.sdk.android.Fabric;

public class Monitoring extends AppCompatActivity {

    private ServerMonitoring serverMonitoring = new ServerMonitoring();
    private SettingsMonitoring settingsMonitoring = new SettingsMonitoring();
    private UserTab userTab = new UserTab();

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        final Fabric fabric = new Fabric.Builder(this)
                .kits(new Crashlytics())
                .debuggable(true)
                .build();
        Fabric.with(this, new Crashlytics());
        setContentView(R.layout.activity_monitoring);
        getIntent().addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        getIntent().addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);

        init();


        BottomNavigationView navigation = (BottomNavigationView) findViewById(R.id.navigation);
        navigation.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener);

        android.support.v4.app.FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
        fragmentTransaction.replace(R.id.monitoringContainer, serverMonitoring);
        fragmentTransaction.commit();

        /*PowerManager pm = (PowerManager)getApplicationContext().getSystemService(this.POWER_SERVICE);
        PowerManager.WakeLock wl = pm.newWakeLock(PowerManager.PARTIAL_WAKE_LOCK, "MyWakeLock");
        wl.acquire();
        */

        if (ContextCompat.checkSelfPermission(this,Manifest.permission.SEND_SMS)!= PackageManager.PERMISSION_GRANTED){
            ActivityCompat.requestPermissions(this,new String[]{Manifest.permission.SEND_SMS}, 0);
        }
        CenterDAO centerDAO = new CenterDAO(this);
        Center center = centerDAO.selectCenter();

        if(center == null){
            System.out.println("Redirection");
            redirectCenterData();
        }

    }

    @Override
    protected void onRestart() {
        super.onRestart();
        CenterDAO centerDAO = new CenterDAO(this);
        Center center = centerDAO.selectCenter();

        if(center == null){
            redirectCenterData();
        }
    }

    @Override
    public void onBackPressed() {
        Toast.makeText(getApplicationContext(), "Vous ne pouvez pas quitter l'application de Monitoring!", Toast.LENGTH_LONG).show();
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if(keyCode == KeyEvent.KEYCODE_HOME){
            Toast.makeText(getApplicationContext(), "Vous ne pouvez pas quitter l'application de Monitoring!", Toast.LENGTH_LONG).show();
            return true;
        }
        return super.onKeyDown(keyCode, event);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_lock, menu);
        return super.onCreateOptionsMenu(menu);
    }

    private BottomNavigationView.OnNavigationItemSelectedListener mOnNavigationItemSelectedListener
            = new BottomNavigationView.OnNavigationItemSelectedListener() {

        @Override
        public boolean onNavigationItemSelected(@NonNull MenuItem item) {
            android.support.v4.app.FragmentTransaction fragmentTransaction;
            switch (item.getItemId()) {
                case R.id.navigation_server:
                    fragmentTransaction = getSupportFragmentManager().beginTransaction();
                    fragmentTransaction.replace(R.id.monitoringContainer, serverMonitoring);
                    fragmentTransaction.commit();
                    return true;
                case R.id.navigation_users:
                    fragmentTransaction = getSupportFragmentManager().beginTransaction();
                    fragmentTransaction.replace(R.id.monitoringContainer, userTab);
                    fragmentTransaction.commit();
                    return true;
                case R.id.navigation_notifications:
                    fragmentTransaction = getSupportFragmentManager().beginTransaction();
                    fragmentTransaction.replace(R.id.monitoringContainer, settingsMonitoring);
                    fragmentTransaction.commit();
                    return true;
            }

            return false;
        }

    };

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        showLock();
        return super.onOptionsItemSelected(item);
    }

    private void showLock(){
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("Application vérouillée");
        View serverInfo = getLayoutInflater().inflate(R.layout.dialog_locked, null);
        builder.setView(serverInfo);
        builder.setCancelable(false);
        //final AlertDialog dialog = builder.create();
        //dialog.show();

       /*unlockApp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (unlockPswd.getText().toString().equals(preferences.getString(Center.PASSWORD, null))){
                    dialog.cancel();
                } else {
                    unlockPswd.setText("");
                    unlockPswd.setError(getString(R.string.pswd_incorrect));
                }
            }
        });*/

    }

    private void redirectCenterData(){
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("Ajouter des informations sur le centre");
        View redirect = getLayoutInflater().inflate(R.layout.dialog_redirect_center, null);
        builder.setView(redirect);
        builder.setCancelable(false);
        final AlertDialog dialog = builder.create();
        dialog.show();

        Button redirectConfig = (Button) redirect.findViewById(R.id.redirect);
        redirectConfig.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.cancel();
                startActivity(new Intent(getApplicationContext(), CenterData.class));
            }
        });
    }

    /*@Override
    public boolean onOptionsItemSelected(MenuItem item) {
        final AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("Application vérouillée");
        builder.setPositiveButton("Déverouiller",new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
                EditText pswd = (EditText)
                if (pswd.getText().toString().equals(preferences.getString(PlaceAndPswd.PSWD, null))){
                    dialogInterface.cancel();
                    System.out.println("LA");
                } else {
                    pswd.setText("");
                    pswd.setError(getString(R.string.pswd_incorrect));
                }

            }
        });
        View serverInfo = getLayoutInflater().inflate(R.layout.dialog_locked, null);
        builder.setView(serverInfo);
        builder.setCancelable(false);
        return super.onOptionsItemSelected(item);
    }*/



    public void init(){


        runOnUiThread(new Runnable() {
            @RequiresApi(api = Build.VERSION_CODES.M)
            @Override
            public void run() {
                Context context = getApplicationContext();
                startService(new Intent(context, ApplicationService.class));
                //startService(new Intent(context, DayliSmsService.class));
                //startService(new Intent(context, AlertSmsService.class));
                //startService(new Intent(context, NoAlertSmsService.class));
                //startService(new Intent(context, WifiService.class));
                //startService(new Intent(context, PingService.class));
                //startService(new Intent(context, ElectricityService.class));
                //startService(new Intent(context, TcpService.class));
                //startService(new Intent(context, SnmpService.class));
                //startService(new Intent(context, SolarService.class));
                //startService(new Intent(context, AndroidService.class));

                AlarmManager alarmManager = (AlarmManager) context.getSystemService(ALARM_SERVICE);
                Intent intent;
                int control;
                PendingIntent pendingIntent;
                CheckTimeDAO checkTimeDAO = new CheckTimeDAO(context);

                for(int i=0; i<7; i++){
                    intent = new Intent(context, AlertSmsService.class);
                    intent.putExtra("DEVICE_TYPE", i);
                    control = checkTimeDAO.selectCheckTimeByDeviceType(i).getControl();
                    pendingIntent = PendingIntent.getService(context, i + 11, intent, 0);
                    alarmManager.setExactAndAllowWhileIdle(AlarmManager.ELAPSED_REALTIME_WAKEUP, SystemClock.elapsedRealtime() + control * SharedMonitoringTools.MINUTE, pendingIntent);
                }

                for(int i=0; i<7; i++){
                    intent = new Intent(context, NoAlertSmsService.class);
                    intent.putExtra("DEVICE_TYPE", i);
                    control = checkTimeDAO.selectCheckTimeByDeviceType(i).getControl();
                    pendingIntent = PendingIntent.getService(context, i + 21, intent, 0);
                    alarmManager.setExactAndAllowWhileIdle(AlarmManager.ELAPSED_REALTIME_WAKEUP, SystemClock.elapsedRealtime() + control * SharedMonitoringTools.MINUTE, pendingIntent);
                }

                Calendar calendar = Calendar.getInstance();
                calendar.setTimeInMillis(System.currentTimeMillis());
                SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);
                Integer hour = Integer.valueOf(sharedPreferences.getString(ModifyCheckTime.SMS_HOUR, "16"));
                calendar.set(Calendar.HOUR_OF_DAY, hour);
                intent = new Intent(context, DayliSmsService.class);
                pendingIntent = PendingIntent.getService(context, 10, intent, 0);
                alarmManager.setExactAndAllowWhileIdle(AlarmManager.RTC_WAKEUP, calendar.getTimeInMillis(), pendingIntent);

                intent = new Intent(context, WifiService.class);
                control = checkTimeDAO.selectCheckTimeByDeviceType(Types.Wifi.getNum()).getControl();
                pendingIntent = PendingIntent.getService(context, Types.Wifi.getNum(), intent, 0);
                alarmManager.setExactAndAllowWhileIdle(AlarmManager.ELAPSED_REALTIME_WAKEUP, SystemClock.elapsedRealtime() + control * SharedMonitoringTools.MINUTE, pendingIntent);

                intent = new Intent(context, PingService.class);
                control = checkTimeDAO.selectCheckTimeByDeviceType(Types.Ping.getNum()).getControl();
                pendingIntent = PendingIntent.getService(context, Types.Ping.getNum(), intent, 0);
                alarmManager.setExactAndAllowWhileIdle(AlarmManager.ELAPSED_REALTIME_WAKEUP, SystemClock.elapsedRealtime() + control * SharedMonitoringTools.MINUTE, pendingIntent);

                intent = new Intent(context, SnmpService.class);
                control = checkTimeDAO.selectCheckTimeByDeviceType(Types.SNMP.getNum()).getControl();
                pendingIntent = PendingIntent.getService(context, Types.SNMP.getNum(), intent, 0);
                alarmManager.setExactAndAllowWhileIdle(AlarmManager.ELAPSED_REALTIME_WAKEUP, SystemClock.elapsedRealtime() + control * SharedMonitoringTools.MINUTE, pendingIntent);

                intent = new Intent(context, TcpService.class);
                control = checkTimeDAO.selectCheckTimeByDeviceType(Types.TCP.getNum()).getControl();
                pendingIntent = PendingIntent.getService(context, Types.TCP.getNum(), intent, 0);
                alarmManager.setExactAndAllowWhileIdle(AlarmManager.ELAPSED_REALTIME_WAKEUP, SystemClock.elapsedRealtime() + control * SharedMonitoringTools.MINUTE, pendingIntent);

                intent = new Intent(context, AndroidService.class);
                control = checkTimeDAO.selectCheckTimeByDeviceType(Types.Android.getNum()).getControl();
                pendingIntent = PendingIntent.getService(context, Types.Android.getNum(), intent, 0);
                alarmManager.setExactAndAllowWhileIdle(AlarmManager.ELAPSED_REALTIME_WAKEUP, SystemClock.elapsedRealtime() + control * SharedMonitoringTools.MINUTE, pendingIntent);

                intent = new Intent(context, ElectricityService.class);
                control = checkTimeDAO.selectCheckTimeByDeviceType(Types.Electricity.getNum()).getControl();
                pendingIntent = PendingIntent.getService(context, Types.Electricity.getNum(), intent, 0);
                alarmManager.setExactAndAllowWhileIdle(AlarmManager.ELAPSED_REALTIME_WAKEUP, SystemClock.elapsedRealtime() + control * SharedMonitoringTools.MINUTE, pendingIntent);

                intent = new Intent(context, SolarService.class);
                control = checkTimeDAO.selectCheckTimeByDeviceType(Types.Solar.getNum()).getControl();
                pendingIntent = PendingIntent.getService(context, Types.Solar.getNum(), intent, 0);
                alarmManager.setExactAndAllowWhileIdle(AlarmManager.ELAPSED_REALTIME_WAKEUP, SystemClock.elapsedRealtime() + control * SharedMonitoringTools.MINUTE, pendingIntent);

            }
        });

    }

}
