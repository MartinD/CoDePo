package com.martin.codepo.activity;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.martin.codepo.R;
import com.martin.codepo.adapter.SettingsAdapter;
import com.martin.codepo.database.CheckTimeDAO;

import java.util.ArrayList;
import java.util.Arrays;

/**
 * A simple {@link Fragment} subclass.
 */
public class SettingsMonitoring extends Fragment {

    private ArrayList<String> names = new ArrayList<>(Arrays.asList("Tutoriel", "Centre", "Scan du réseau", "Informations", "Notifications"));

    public SettingsMonitoring() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_settings_monitoring, container, false);
        SettingsAdapter settingsAdapter = new SettingsAdapter(names);
        RecyclerView recyclerViewServers = (RecyclerView) view.findViewById(R.id.settings);
        recyclerViewServers.setLayoutManager(new LinearLayoutManager(getContext()));
        recyclerViewServers.setAdapter(settingsAdapter);

        return view;
    }


}
