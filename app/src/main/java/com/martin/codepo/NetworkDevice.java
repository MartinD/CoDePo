package com.martin.codepo;

/**
 * Created by user on 13/03/17.
 */

public class NetworkDevice {
    private String ipAddress;
    private String deviceName;
    private String macAddress;

    public NetworkDevice(String ipAddress, String macAddress, String deviceName) {
        this.ipAddress = ipAddress;
        this.macAddress = macAddress;
        this.deviceName = deviceName;
    }

    public NetworkDevice() {
        this.ipAddress = String.valueOf("");
        this.macAddress = String.valueOf("");
        this.deviceName = String.valueOf("");
    }
    public String getIpAddress() {
        return ipAddress;
    }

    public void setIpAddress(String ipAddress) {
        this.ipAddress = ipAddress;
    }

    public String getDeviceName() {
        return deviceName;
    }

    public void setDeviceName(String deviceName) {
        this.deviceName = deviceName;
    }

    public String getMacAddress() {
        return macAddress;
    }

    public void setMacAddress(String macAddress) {
        this.macAddress = macAddress;
    }
}
