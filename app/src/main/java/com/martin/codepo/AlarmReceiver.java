package com.martin.codepo;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.widget.Toast;

/**
 * Created by martin on 9/04/18.
 */

public class AlarmReceiver extends BroadcastReceiver {


    @Override
    public void onReceive(Context context, Intent intent) {
        System.out.println("ALARM TEST");
        Toast.makeText(context, "OnReceive alarm test", Toast.LENGTH_SHORT).show();
    }
}
