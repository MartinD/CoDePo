package com.martin.codepo;

import java.util.Objects;

/**
 * Created by user on 10/07/17.
 */
public enum Types {

    Wifi ("Wifi", 0, "Wifi") {

        @Override
        public String getName() {
            return Types.Wifi.value;
        }

        @Override
        public int getNum() {
            return Types.Wifi.num;
        }

        @Override
        public String getShortValue() {
            return Types.Wifi.shortValue;
        }
    },
    Ping ("Présence des éléments sur le réseau", 1, "Ping") {

        @Override
        public String getName() {
            return Types.Ping.value;
        }

        @Override
        public int getNum() {
            return Types.Ping.num;
        }

        @Override
        public String getShortValue() {
            return Types.Ping.shortValue;
        }
    },
    SNMP ("Etat du serveur", 2, "SNMP") {

        @Override
        public String getName() {
            return Types.SNMP.value;
        }

        @Override
        public int getNum() {
            return Types.SNMP.num;
        }

        @Override
        public String getShortValue() {
            return Types.SNMP.shortValue;
        }
    },
    TCP ("Charge de la batterie", 3, "TCP"){
        @Override
        public String getName() {
            return Types.TCP.value;
        }

        @Override
        public int getNum() {
            return Types.TCP.num;
        }

        @Override
        public String getShortValue() {
            return Types.TCP.shortValue;
        }
    },
    Android ("Charge du smartphone", 4, "Android"){
        @Override
        public String getName() {
            return Types.Android.value;
        }

        @Override
        public int getNum() {
            return Types.Android.num;
        }

        @Override
        public String getShortValue() {
            return Types.Android.shortValue;
        }
    },
    Electricity ("Réseau électrique", 5, "Electricity"){
        @Override
        public String getName() {
            return Types.Electricity.value;
        }

        @Override
        public int getNum() {
            return Types.Electricity.num;
        }

        @Override
        public String getShortValue() {
            return Types.Electricity.shortValue;
        }
    },
    Solar ("Panneaux solaires", 6, "Solar"){
        @Override
        public String getName() {
            return Types.Solar.value;
        }

        @Override
        public int getNum() {
            return Types.Solar.num;
        }

        @Override
        public String getShortValue() {
            return Types.Solar.shortValue;
        }
    };


    private String value;
    private int num;
    private String shortValue;

    Types(String value, int num, String shortValue) {
        this.value = value;
        this.num = num;
        this.shortValue = shortValue;
    }

    public abstract String getName();
    public abstract int getNum();
    public abstract String getShortValue();

    public static String typeFromString(String name){
        for(Types n : Types.values()){
            if(Objects.equals(n.value, name)){
                return n.toString();
            }
        }
        return null;
    }

    public static int getLength(){
        return Types.values().length;
    }

    public static int numFromString(String name){
        for(Types n : Types.values()){
            if(Objects.equals(n.value, name)){
                return n.getNum();
            }
        }
        return -1;
    }

}



